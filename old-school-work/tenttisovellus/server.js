const express = require('express');
const cors = require('cors');
const app = express();
const port = 3999;
const jwt = require('jsonwebtoken');

app.use(cors());
app.use(express.json());

const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: "PostJ17Pass",
  port: 5432
});

app.post('/user/login', function(req, res) {
	pool.query('SELECT käyttäjätunnus, salasana, käyttäjä_tyyppi FROM public."Käyttäjä" WHERE käyttäjätunnus = ($1) AND salasana = ($2)', [req.body.käyttäjätunnus, req.body.salasana], (error, row) => {
		if (error) {
			throw error;
		}
		else {
			if (row.rowCount > 0) {
				var käyttäjätyyppi = "opiskelija";
				if (req.body.käyttäjätunnus === "juuso.hauvala@gmail.com") {
					käyttäjätyyppi = "ylläpitäjä"; // ["juuso.hauvala@gmail.com"] && ["HauSql17"] === "ylläpitäjä"
				} // Tämä ratkaisu on käytössä, kunnes toiminto, joka reagoi haettuun käyttäjätyyppi_id:hen, saadaan tehdyksi.
				var token = jwt.sign(
					{
						käyttäjänimi: req.body.käyttäjätunnus,
						salasana: req.body.salasana,
						käyttäjä_tyyppi: käyttäjätyyppi
					}, 'supersalainen', {expiresIn: 3600}); // 3600 s = 120 min = 2 h
			  	res.send(token);
			}
			else {
				res.send(null);
			}
		}
	});
});

app.post('/user/register', function(req, res) {
	var ifexists = null;
	pool.query('SELECT käyttäjätunnus FROM public."Käyttäjä" WHERE käyttäjätunnus = ($1)', [req.body.käyttäjätunnus], (error, row) => {
		if (error) {
			throw error;
		}
		else {
			console.log(row);
			console.log(row.rowCount);
			if (row.rowCount > 0) {
				res.send("exists already");
			}
			else {
				var käyttäjätyyppi = "opiskelija";
				if (req.body.käyttäjätunnus === "juuso.hauvala@gmail.com") {
					käyttäjätyyppi = "ylläpitäjä"; // ["juuso.hauvala@gmail.com"] && ["HauSql17"] === "ylläpitäjä"
				}

				pool.query('INSERT INTO public."Käyttäjä"(käyttäjätunnus, salasana, käyttäjä_tyyppi) VALUES(($1), ($2), (SELECT käyttäjätyyppi_id FROM public."Käyttäjätyyppi" WHERE nimi = ($3)));', [req.body.käyttäjätunnus, req.body.salasana, käyttäjätyyppi], (error2, row) => {
					if (error2) {
						throw error2;
					}
					else {
						if (row.rowCount > 0) {
							var token = jwt.sign(
								{
									käyttäjänimi: req.body.käyttäjätunnus,
									salasana: req.body.salasana,
									käyttäjätyyppi_id: käyttäjätyyppi
								}, 'supersalainen', {expiresIn: 3600}); // 3600 s = 120 min = 2 h
							res.send(token);
						}
						else {
							res.send(null);
						}
					}
				});
			}
		}
	});
});

// salasanan bcrypt ja authentication ei ole vielä toteutettu.
// Token auttaa tenttisovellusta toistaiseksi vain kirjautumisessa.

app.listen(port, () => {
    console.log("Tenttipalvelin käynnistyi portissa: " + port);
});
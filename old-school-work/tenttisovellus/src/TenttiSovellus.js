import React, { useState, useEffect /* , useContext, */, useReducer } from 'react';
import './App.css';
import { AppBar, Box, Button, Container, CssBaseline, TextField, Toolbar } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import { alkutila, reducer /* , toimintoTyypit */ } from './Context.js';
import { DrawChart } from './DrawChart.js';
import { EditMode } from './EditMode.js';
import { Kysymys } from './Kysymys.js';

const axios = require('axios');

export const TenttiSovellus = () => {
  // const [tentit, dispatch] = useContext(QuizContext);

  const [kirjautunut, setKirjautunut] = useState(false);
  const [kirjautumisvirhe, setKirjautumisvirhe] = useState(false);
  const [kirjautumisilmoitus, setKirjautumisilmoitus]= useState('');
  const [kirjaudu, setKirjaudu] = useState(true);
  const [rekisteröidy, setRekisteröidy] = useState(false);
  const [käyttäjätunnus, setKäyttäjätunnus] = useState('');
  const [salasana, setSalasana] = useState('');
  const [tentit, dispatch] = useReducer(reducer, alkutila);
  const [näytäTentit, setNäytäTentit] = useState(false);
  const [valittuTentti, setValittuTentti] = useState(null);
  const [komponenttiAlustettu, setKomponenttiAlustettu] = useState(false);
  const [oikeatVastaukset, setOikeatVastaukset] = useState(false);
  const [muokkaus, setMuokkaus] = useState(false);
  const [luoTentti, setLuoTentti] = useState(false);
  const [uudenTentinNimi, setUudenTentinNimi] = useState('');

  useEffect(() => {
    if (!komponenttiAlustettu) {
      if (!localStorage.getItem("tentit")) {
        localStorage.setItem("tentit", JSON.stringify(tentit));
      }
      else {
        localStorage.setItem("tentit", JSON.stringify(tentit));
      }
      setKomponenttiAlustettu(true);
    }
    else {
      localStorage.setItem("tentit", JSON.stringify(tentit));
    }
  }, [tentit, komponenttiAlustettu]);

  return (
    <div>
      <React.Fragment>
        <CssBaseline />
        <AppBar>
          <Toolbar>
            {kirjautunut === true &&
              <Button color="inherit" onClick={() => {setLuoTentti(false); setNäytäTentit(true); setMuokkaus(false)}}>
                Tentit
              </Button>
            }
            {kirjautunut === true &&
              <Button color="inherit" onClick={() => {setKirjautunut(false); setKirjaudu(true); setSalasana(''); localStorage.removeItem('token');}}>
                Kirjaudu ulos
              </Button>
            }
            {kirjautunut === false &&
              <Button color="inherit" onClick={() => {setKirjaudu(true); setRekisteröidy(false); setKäyttäjätunnus(''); setSalasana('')}}>
                Kirjaudu
              </Button>
            }
            {kirjautunut === false &&
              <Button color="inherit" onClick={() => {setKirjaudu(false); setRekisteröidy(true); setKäyttäjätunnus(''); setSalasana('')}}>
                Rekisteröidy
              </Button>
            }
          </Toolbar>
        </AppBar>
        <Container>
          {kirjautunut === false && 
            <Box my={9}>
              {kirjaudu === true && 
                <div align="center">
                  <br /><br />
                  <h1>Kirjaudu</h1>
                  <TextField label="Käyttäjätunnus" variant="outlined"
                    value={käyttäjätunnus}
                    onChange={(event) => setKäyttäjätunnus(event.target.value)}
                  >
                  </TextField>
                  <br /><br />
                  <TextField label="Salasana" type="password" variant="outlined"
                    value={salasana}
                    onChange={(event) => setSalasana(event.target.value)}
                  >
                  </TextField>
                  <br /><br />
                  <Button variant="contained" color="primary"
                    onClick={() => {
                      axios.post("http://localhost:3999/user/login", {
                        käyttäjätunnus: käyttäjätunnus,
                        salasana: salasana
                      }).then(response => {
                        if (response.data === "") {
                          setKirjautumisvirhe(true);
                          setKirjautumisilmoitus('Annoit väärän käyttäjätunnuksen tai salasanan!');
                        }
                        else {
                          // Jos virheitä ei ilmene ja saadaan token aikaiseksi.
                          localStorage.setItem('token', JSON.stringify(response.data));
                          setKirjaudu(false);
                          setKirjautunut(true);
                          setKirjautumisvirhe(false);
                          setKirjautumisilmoitus('');
                        }
                        return response;
                      }).catch(function(error) {
                        console.error(error);
                        setKirjautumisvirhe(true);
                        setKirjautumisilmoitus('Kirjautumisessa tapahtui virhe!');
                        
                        return error.response;
                      });
                    }}
                  >
                    Kirjaudu
                  </Button>
                  <br /><br />
                  {kirjautumisvirhe === true && <div><font color="red">{kirjautumisilmoitus}</font></div>}
                </div>
              }
              {rekisteröidy === true && 
                <div align="center">
                  <br /><br />
                  <h1>Rekisteröidy</h1>
                  <TextField label="Käyttäjätunnus" variant="outlined"
                    value={käyttäjätunnus}
                    onChange={(event) => setKäyttäjätunnus(event.target.value)}
                  >
                  </TextField>
                  <br /><br />
                  <TextField label="Salasana" type="password" variant="outlined"
                    value={salasana}
                    onChange={(event) => setSalasana(event.target.value)}
                  >
                  </TextField>
                  <br /><br />
                  <Button variant="contained" color="primary"
                    onClick={() => {
                      axios.post("http://localhost:3999/user/register", {
                        käyttäjätunnus: käyttäjätunnus,
                        salasana: salasana
                      }).then(response => {
                        
                        if (response.data === "exists already") {
                          // Rekisteröitymistä ei tapahdu, jos kyseinen tunnus on jo olemassa.
                          setKirjautumisvirhe(true);
                          setKirjautumisilmoitus("Kyseinen käyttäjätunnus on jo olemassa!");
                        }
                        else if (response.data === "") {
                          // Jos ilmenee jokin muu virhe.
                          setKirjautumisvirhe(true);
                          setKirjautumisilmoitus("Rekisteröitymisessä tapahtui virhe!");
                        }
                        else {
                          // Jos virheitä ei ilmene ja saadaan token aikaiseksi.
                          localStorage.setItem('token', JSON.stringify(response.data));
                          setRekisteröidy(false);
                          setKirjautunut(true);
                          setKirjautumisvirhe(false);
                          setKirjautumisilmoitus('');
                        }

                        return response;
                      }).catch(function(error) {
                        console.error(error);
                        return error.response;
                      });
                    }}
                  >
                    Rekisteröidy
                  </Button>
                  <br /><br />
                  {kirjautumisvirhe === true && <div><font color="red">{kirjautumisilmoitus}</font></div>}
                </div>
              }
            </Box>
          }
          {kirjautunut === true && 
            <Box my={9}>
              {näytäTentit === true && muokkaus !== true && <div>{tentit.map((tentti, i) => <Button variant="contained"
                onClick={() => {setValittuTentti(i); setOikeatVastaukset(false)}}>{tentti.nimi}</Button>)}
                <Button color="primary" onClick={() => setLuoTentti(true)}>
                  <AddIcon /> Luo uusi tentti
                </Button>
              </div>}
              {valittuTentti !== null && muokkaus !== true && 
                <div>
                  <br />
                  <Button color="primary" onClick={() => {setMuokkaus(true)}}>
                    <EditIcon /> Muokkaa tenttiä
                  </Button>
                  {tentit[valittuTentti].kysymykset.map((kysymys, kysymysIndex) => 
                    <Kysymys
                      kysymysIndex={kysymysIndex} {...kysymys} tenttiIndex={valittuTentti} oikeatVastaukset={oikeatVastaukset} dispatch={dispatch}
                    />)
                  }
                  {oikeatVastaukset === true && <div><br /><DrawChart tentti={tentit[valittuTentti]}/></div>}
                  <br />
                  <Button variant="contained" color="primary" onClick={() => {setOikeatVastaukset(true)}}>Näytä vastaukset</Button></div>}
                  {muokkaus === true && <div><br />
                    <EditMode
                      tentit={tentit}
                      valittuTentti={valittuTentti}
                      dispatch={dispatch}
                    />
                    </div>
                  }
                  {luoTentti === true && <div>
                    <TextField label="Tentin nimi" /* onChange={(event) => { handleNewTestName(event)}} value={uudenTentinNimi} */ />
                    <br /><br />
                    <Button variant="contained" color="primary" onClick={() => {tentit.push({nimi: uudenTentinNimi, kysymykset: []}); setUudenTentinNimi(''); setLuoTentti(false); setValittuTentti((tentit.length + 1)); setMuokkaus(true)}}>
                      <SaveIcon/> Tallenna
                    </Button>
                    <Button variant="contained" onClick={() => {setLuoTentti(false); setUudenTentinNimi(''); setValittuTentti(null)}}>Peruuta</Button>
                  </div>
                  }
            </Box>
          }
        </Container>
      </React.Fragment>
    </div>
  );
}
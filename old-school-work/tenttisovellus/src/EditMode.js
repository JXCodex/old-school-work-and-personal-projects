import React from 'react';
import { Button, Checkbox, IconButton, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

export const EditMode = (props) => {
  return (
    <div>
      {props.tentit[props.valittuTentti].kysymykset.map((kysymys, i) => 
        <div>
          <TextField label="Kysymys" fullWidth={true} variant="outlined" value={kysymys.kysymys} onChange={(event) => props.dispatch({
            arvo: event.target.value,
            tyyppi: 'muokkaaKysymystä',
            tenttiIndex: props.valittuTentti,
            kysymysIndex: i
          })} />
          <br /><br />
          <TextField label="Aihepiiri" fullWidth={true} variant="outlined" value={kysymys.aihepiiri} onChange={(event) => props.dispatch({
            arvo: event.target.value,
            tyyppi: 'muokkaaAihepiiriä',
            tenttiIndex: props.valittuTentti,
            kysymysIndex: i
          })} />
          <br /><br />
          {kysymys.vastaukset.map((vastaus, j) =>
            <div>
              <Checkbox checked={vastaus.vastausOikein} onChange={(event) => props.dispatch({
                arvo: event.target.checked,
                tyyppi: 'muokkaaOikeaaVastausta',
                tenttiIndex: props.valittuTentti,
                kysymysIndex: i,
                vastausIndex: j
              })} />
              <TextField variant="outlined" value={vastaus.vastaus} onChange={(event) => props.dispatch({
                arvo: event.target.value,
                tyyppi: 'muokkaaVastausta',
                tenttiIndex: props.valittuTentti,
                kysymysIndex: i,
                vastausIndex: j
              })} />
              <IconButton onClick={() => props.dispatch({
                tyyppi: 'poistaVastaus',
                tenttiIndex: props.valittuTentti,
                kysymysIndex: i,
                vastausIndex: j
              })}>
                <DeleteIcon />
              </IconButton>
              <br /><br />
            </div>
          )}
          <Button variant="contained" onClick={(event) => props.dispatch({
            arvo: event.target.value,
            tyyppi: 'lisääVastaus',
            tenttiIndex: props.valittuTentti,
            kysymysIndex: i
          })}>
            <AddIcon /> Lisää vastaus
          </Button>
          <Button variant="contained" onClick={() => props.dispatch({
            tyyppi: 'poistaKysymys',
            tenttiIndex: props.valittuTentti,
            kysymysIndex: i
          })}>
            <DeleteIcon /> Poista kysymys
          </Button>
          <br /><br /><br />
        </div>
      )}
      <Button variant="contained" color="primary" onClick={() => props.dispatch({
        tyyppi: 'lisääKysymys',
        tenttiIndex: props.valittuTentti
      })}>
        <AddIcon /> Lisää kysymys
      </Button>
    </div>
  );
}
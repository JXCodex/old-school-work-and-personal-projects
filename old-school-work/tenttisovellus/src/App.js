import React from 'react';
import './App.css';
import { TenttiSovellus } from './TenttiSovellus.js';

// Tämä on keskeneräinen versio sovelluksesta,
// jota työstin Full Stack -kehittäjän työvoimakoulutuksessa.

const App = () => {
  return(
    <TenttiSovellus />
  );
}
export default App;
import React from 'react';
import './App.css';
import Chart from 'react-google-charts';

export const DrawChart = (props) => {
    let chartData = [];
    let aihepiirit = [];
    var aihevastaukset = [];
    var aihevastauksetOikein = [];
  
    for (var i = 0; i < props.tentti.kysymykset.length; i++) {
      let aihepiiriLöytyy = false;
      var aihepiiriIndex = 0;
      for (var j = 0; j < aihepiirit.length; j++) {
        if (props.tentti.kysymykset[i].aihepiiri === aihepiirit[j]) {
          aihepiiriLöytyy = true;
          aihepiiriIndex = j;
        }
      }
      if (aihepiiriLöytyy === false) {
        aihepiirit.push(props.tentti.kysymykset[i].aihepiiri);
      }
  
      for (var x = 0; x < props.tentti.kysymykset[i].vastaukset.length; x++) {
        if (props.tentti.kysymykset[i].vastaukset[x].valittu === props.tentti.kysymykset[i].vastaukset[x].vastausOikein && props.tentti.kysymykset[i].vastaukset[x].vastausOikein === true) {
          if (aihepiiriLöytyy === false) {
            aihevastauksetOikein.push(1);
            aihevastaukset.push(1);
          }
          else {
            aihevastauksetOikein[aihepiiriIndex]++;
            aihevastaukset[aihepiiriIndex]++;
          }
        }
        else if (props.tentti.kysymykset[i].vastaukset[x].valittu === false && props.tentti.kysymykset[i].vastaukset[x].vastausOikein === true) {
          if (aihepiiriLöytyy === false) {
            aihevastauksetOikein.push(0);
            aihevastaukset.push(1);
          }
          else {
            aihevastaukset[aihepiiriIndex]++;
          }
        }
        else if (props.tentti.kysymykset[i].vastaukset[x].valittu === true && props.tentti.kysymykset[i].vastaukset[x].vastausOikein === false) {
          if (aihepiiriLöytyy === false) {
            aihevastauksetOikein.push(0);
            aihevastaukset.push(1);
          }
          else {
            aihevastaukset[aihepiiriIndex]++;
          }
        }
      }
    }
  
    for (var y = 0; y < aihepiirit.length; y++) {
      chartData.push([aihepiirit[y], ((aihevastauksetOikein[y] / aihevastaukset[y]) * 100)]);
    }
  
    chartData.unshift(['Aihepiirit', 'Oikein %']);
  
    return(
      <Chart
        width={400}
        height={400}
        chartType='ColumnChart'
        loader={<div>Ladataan tilastoja</div>}
        data={chartData}
        options={{
          title: 'Tentin arvostelu',
          chartArea: { width: '90%', height: '90%' },
          hAxis: {
            title: 'Aihepiirit',
            minValue: 0
          },
          vAxis: {
            title: 'Oikein %',
            format: 'percent',
            minValue: 0,
            maxValue: 100
          }
        }}
      />
    );
}
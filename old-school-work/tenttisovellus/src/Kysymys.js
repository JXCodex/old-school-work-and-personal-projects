import React from 'react';
import { Box, Checkbox, FormLabel, withStyles } from '@material-ui/core';

export const Kysymys = (props) => {

    const checkboxStylesBlue = theme => ({
      root: {
        '&$checked': {
          color: 'blue'
        }
      }, checked: {}
    });
    const checkboxStylesGreen = theme => ({
      root: {
        '&$checked': {
          color: 'green'
        }
      }, checked: {}
    });
    const BlueCheckbox = withStyles(checkboxStylesBlue)(Checkbox);
    const GreenCheckbox = withStyles(checkboxStylesGreen)(Checkbox);
  
    return (
      <div>
        <h3>Aihepiiri: {props.aihepiiri}</h3>
        <h4>{props.kysymys}</h4>
        {props.vastaukset.map((vastaus, i) => ( 
        <Box>
          <FormLabel>
            <BlueCheckbox checked={vastaus.valittu} onChange={(event) => 
              props.dispatch({
                arvo: event.target.checked,
                tyyppi: 'päivitäVastausValinta',
                tenttiIndex: props.tenttiIndex,
                kysymysIndex: props.kysymysIndex,
                vastausIndex: i
              })} />
            {props.oikeatVastaukset === true && <GreenCheckbox checked={vastaus.vastausOikein} /> }
            {vastaus.vastaus}
          </FormLabel>
        </Box>))}
      </div>
    );
}
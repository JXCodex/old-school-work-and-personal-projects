// import React, { useContext, useReducer } from 'react';
import './App.css';

// const axios = require('axios');

// export const QuizContext = React.createContext(alkutila);

/*
const tietotesti = axios.get("http://localhost:3999/user/quiz").then(response => {
  return response;
}).catch(function(error) {
  console.error(error);
  return error.response;
});
*/

export const alkutila = [
    {nimi: 'Matematiikka', kysymykset: [
      {kysymys: 'Paljonko on 17 + 14?', aihepiiri: 'Peruslaskut', vastaukset: [
        {vastaus: '31', valittu: false, vastausOikein: true }, 
        {vastaus: '37', valittu: false, vastausOikein: false }, 
        {vastaus: '39', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Paljonko on 9 * 8?', aihepiiri: 'Peruslaskut', vastaukset: [
        {vastaus: '27', valittu: false, vastausOikein: false }, 
        {vastaus: '63', valittu: false, vastausOikein: false }, 
        {vastaus: '72', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Paljonko on 600 / 15?', aihepiiri: 'Peruslaskut', vastaukset: [
        {vastaus: '20', valittu: false, vastausOikein: false },
        {vastaus: '40', valittu: false, vastausOikein: true },
        {vastaus: '60', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Paljonko on 9 potenssiin 2?', aihepiiri: 'Potenssi ja neliöjuuri', vastaukset: [
        {vastaus: '77', valittu: false, vastausOikein: false },
        {vastaus: '79', valittu: false, vastausOikein: false },
        {vastaus: '81', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Paljonko on 3 potenssiin 3?', aihepiiri: 'Potenssi ja neliöjuuri', vastaukset: [
        {vastaus: '27', valittu: false, vastausOikein: true },
        {vastaus: '33', valittu: false, vastausOikein: false },
        {vastaus: '66', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Mikä on luvun 144 neliöjuuri?', aihepiiri: 'Potenssi ja neliöjuuri', vastaukset: [
        {vastaus: '11', valittu: false, vastausOikein: false },
        {vastaus: '12', valittu: false, vastausOikein: true },
        {vastaus: '14', valittu: false, vastausOikein: false }
      ]}
    ]},
    {nimi: 'Kielitaito', kysymykset: [
      {kysymys: 'Mikä näistä ei ole kysymyssana?', aihepiiri: 'Suomen kieli', vastaukset: [
        {vastaus: 'Mikä', valittu: false, vastausOikein: false },
        {vastaus: 'Minne', valittu: false, vastausOikein: false },
        {vastaus: 'Joka', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Minkä vokaalin edessä C lausutaan kuin S?', aihepiiri: 'Vieraat kielet', vastaukset: [
        {vastaus: 'A', valittu: false, vastausOikein: false },
        {vastaus: 'I', valittu: false, vastausOikein: true },
        {vastaus: 'O', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Mikä vokaali ei kuulu suomen kieleen?', aihepiiri: 'Suomen kieli', vastaukset: [
        {vastaus: 'Å', valittu: false, vastausOikein: true },
        {vastaus: 'U', valittu: false, vastausOikein: false },
        {vastaus: 'O', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Miten lausutaan kirjainyhdistelmä QU?', aihepiiri: 'Vieraat kielet', vastaukset: [
        {vastaus: 'KO', valittu: false, vastausOikein: false },
        {vastaus: 'KU', valittu: false, vastausOikein: false },
        {vastaus: 'KV', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Mikä näistä on pitkä vokaali suomen kielessä?', aihepiiri: 'Suomen kieli', vastaukset: [
        {vastaus: 'Ó', valittu: false, vastausOikein: false },
        {vastaus: 'OO', valittu: false, vastausOikein: true },
        {vastaus: 'Ö', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Mikä näistä ei vastaa kirjainyhdistelmää TH?', aihepiiri: 'Vieraat kielet', vastaukset: [
        {vastaus: 'ð', valittu: false, vastausOikein: true },
        {vastaus: 'Þ', valittu: false, vastausOikein: false },
        {vastaus: 'Ŧ', valittu: false, vastausOikein: false }
      ]}
    ]},
    {nimi: 'Ohjelmointi', kysymykset: [
      {kysymys: 'Mikä näistä kielistä on moniparadigmainen?', aihepiiri: 'Ohjelmointikielet', vastaukset: [
        {vastaus: 'C++', valittu: false, vastausOikein: true },
        {vastaus: 'Java', valittu: false, vastausOikein: false },
        {vastaus: 'C#', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Miten float-muuttuja eroaa int-muuttujasta?', aihepiiri: 'Muuttujat ja rakenteet', vastaukset: [
        {vastaus: 'sen arvo on desimaaliluku', valittu: false, vastausOikein: true },
        {vastaus: 'sen arvo on merkki', valittu: false, vastausOikein: false },
        {vastaus: 'se on luokka', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Minä vuonna C# julkaistiin?', aihepiiri: 'Ohjelmointikielet', vastaukset: [
        {vastaus: '1995', valittu: false, vastausOikein: false },
        {vastaus: '1997', valittu: false, vastausOikein: false },
        {vastaus: '2000', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Mikä näistä ei voi olla boolean-muuttujan arvo?', aihepiiri: 'Muuttujat ja rakenteet', vastaukset: [
        {vastaus: 'true', valittu: false, vastausOikein: false },
        {vastaus: 'false', valittu: false, vastausOikein: false },
        {vastaus: 'equal', valittu: false, vastausOikein: true }
      ]},
      {kysymys: 'Mikä näistä ei ole tyyppiturvallinen?', aihepiiri: 'Ohjelmointikielet', vastaukset: [
        {vastaus: 'Java', valittu: false, vastausOikein: false },
        {vastaus: 'JavaScript', valittu: false, vastausOikein: true },
        {vastaus: 'TypeScript', valittu: false, vastausOikein: false }
      ]},
      {kysymys: 'Miksi string ei ole muuttujatyyppi?', aihepiiri: 'Muuttujat ja rakenteet', vastaukset: [
        {vastaus: 'se on luokka', valittu: false, vastausOikein: true },
        {vastaus: 'se on tietotyyppi', valittu: false, vastausOikein: false },
        {vastaus: 'se on algoritmi', valittu: false, vastausOikein: false }
      ]}
    ]}
];

export const toimintoTyypit = {
    alustaTentit: 'alustaTentit',
    päivitäVastausValinta: 'päivitäVastausValinta',
    muokkaaKysymystä: 'muokkaaKysymystä',
    muokkaaAihepiiriä: 'muokkaaAihepiiriä',
    muokkaaVastausta: 'muokkaaVastausta',
    poistaKysymys: 'poistaKysymys',
    muokkaaOikeaaVastausta: 'muokkaaOikeaaVastausta',
    lisääVastaus: 'lisääVastaus',
    poistaVastaus: 'poistaVastaus',
    lisääKysymys: 'lisääKysymys',
    järjestäTentitAZ: 'järjestäTentitAZ',
    järjestäTentitZA: 'järjestäTentitZA'
}

export const reducer = (tila, toiminto) => {

  let nykyinenData = JSON.parse(localStorage.getItem("tentit"));
  // let nykyinenData = JSON.parse(localStorage.getItem(tila));

  switch (toiminto.tyyppi) {
    
    case toimintoTyypit.alustaTentit: {
      // nykyinenData = toiminto.arvo;
      return nykyinenData;
    }
    case toimintoTyypit.päivitäVastausValinta: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].vastaukset[toiminto.vastausIndex].valittu = toiminto.arvo; 
      return nykyinenData;
      // tentit[tenttiIndex].kysymykset[kysymysIndex].vastaukset[toiminto.vastausIndex].valittu = toiminto.arvo;
    }
    case toimintoTyypit.muokkaaKysymystä: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].kysymys = toiminto.arvo;
      break;
    }
    case toimintoTyypit.muokkaaAihepiiriä: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].aihepiiri = toiminto.arvo;
      break;
    }
    case toimintoTyypit.muokkaaVastausta: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].vastaukset[toiminto.vastausIndex].vastaus = toiminto.arvo;
      break;
    }
    case toimintoTyypit.poistaKysymys: {
      nykyinenData[toiminto.tenttiIndex].kysymykset.splice(toiminto.kysymysIndex, 1);
      break;
    }
    case toimintoTyypit.muokkaaOikeaaVastausta: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].vastaukset[toiminto.vastausIndex].vastausOikein = toiminto.arvo;
      break;
    }
    case toimintoTyypit.lisääVastaus: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].vastaukset.push({vastaus: '', valittu: false, vastausOikein: false});
      break;
    }
    case toimintoTyypit.poistaVastaus: {
      nykyinenData[toiminto.tenttiIndex].kysymykset[toiminto.kysymysIndex].vastaukset.splice(toiminto.vastausIndex, 1);   
      break;
    }
    case toimintoTyypit.lisääKysymys: {
      nykyinenData[toiminto.tenttiIndex].kysymykset.push({kysymys: '', vastaukset: []});   
      break;
    }
    case toimintoTyypit.järjestäTentitAZ: {
      nykyinenData = tila.sort((a, b) => (a.Nimi).localeCompare(b.Nimi, 'fi', { sensitivity: 'accent' }) <= 0 ? -1 : 1);
      break;
    }
    case toimintoTyypit.järjestäTentitZA: {
      nykyinenData = tila.sort((a, b) => (a.Nimi).localeCompare(b.Nimi, 'fi', { sensitivity: 'accent' }) >= 0 ? -1 : 1);
      break;
    }
    default: {
      throw new Error();
    }
  }

  return nykyinenData;
}

/*
export const QuizProvider = (props) => {
  const [tentit, dispatch] = useReducer(reducer, alkutila);

  return (
    <QuizContext.Provider value={{tila, dispatch}}>
      {props.children}
    </QuizContext.Provider>
  );
}
*/
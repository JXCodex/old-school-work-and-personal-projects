﻿// Simulaatioiden yhteiset muuttujat.
var vaihe = 0;
var toisto = new Array(); // Auttaa valintarakenteiden käytössä.
var max; // Esimerkkiohjelman vaiheiden määrä.
var alkuperSisalto; // Tarpeen silloin kun poistetaan värimerkintä vaiheesta, jota ei enää käsitellä.
var esto = false; // tarpeen kohdissa, joissa annetaan syötteitä.

function alkuun() {
	if (vaihe > 1) vaihe = 1;
	
	console.log('alkuun - vaihe: ' + vaihe);
	// Palaa esimerkkikoodin käsittelyn ensimmäiseen vaiheeseen.
	// Funktiota kutsutaan nappulan "Alkuun" kautta.
}
function edvaihe(edvaih) {
	if (vaihe > 1) vaihe = edvaih;
	
	console.log('edvaihe - vaihe: ' + vaihe);
	// Palauttaa normaalitilanteessa yhden vaiheen taaksepäin.
	// Joskus edellisen vaihenumeron suuruus riippuu valinta-
	// tai toistolauseen ehdoista. Funktiota kutsutaan nappulan
	// "Edellinen" kautta, jos sille ei ole estettä.
}
function seurvaihe(seurvaih) {
	if (vaihe < max) vaihe = seurvaih;
	
	console.log('seurvaihe - vaihe: ' + vaihe);
	// Siirtää normaalitilanteessa seuraavaan vaiheeseen.
	// Joskus edellisen vaihenumeron suuruus riippuu valinta-
	// tai toistolauseen ehdoista. Funktiota kutsutaan nappulan
	// "Seuraava" kautta, jos sille ei ole estettä.
}
function merkinta() {
	if (esto == false) {
		var luku = vaihe.toString();
		alkuperSisalto = document.getElementById(luku).innerHTML;
		document.getElementById(luku).innerHTML = "<span style='color: 	FF6600;'>" + document.getElementById(luku).innerHTML + "</span>";
		// Siirtää värimerkinnän esimerkkikoodin
		// vaiheesta toiseen vaiheeseen, jos
		// sitä ei ole estetty.
		}
}
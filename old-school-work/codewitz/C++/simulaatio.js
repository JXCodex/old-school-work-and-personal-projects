﻿// Esimerkkisimulaatioiden yhteiset muuttujat.
var vaihe = 0;
var toisto = new Array(); // Auttaa valintarakenteiden käytössä.
var max; // Esimerkkiohjelman vaiheiden määrä.
var alkuperSisalto; // Tarpeen silloin kun poistetaan värimerkintä vaiheesta, jota ei enää käsitellä.
var esto = false; // tarpeen kohdissa, joissa annetaan syötteitä.
// Harjoitussimulaatioiden yhteiset muuttujat.
var tehtava = new Array(); // Tämän taulukon arvo auttaa määrittämään tehtävän ehdot.
var tarkistus = false; // totuusarvo auttaa määrittämään miten palauteteksti tulostuu.

function alkuun() {
	if (vaihe > 1) vaihe = 1;
	
	console.log('alkuun - vaihe: ' + vaihe);
	// Palaa esimerkkikoodin käsittelyn ensimmäiseen vaiheeseen.
	// Funktiota kutsutaan painikkeen "Alkuun" kautta, jos funktiossa
	// ”paivita” muuttujan ”toiminto” arvo on 0.
}
function edvaihe(edvaih) {
	if (vaihe > 1) vaihe = edvaih;
	
	console.log('edvaihe - vaihe: ' + vaihe);
	// Palauttaa normaalitilanteessa yhden vaiheen taaksepäin.
	// Joskus edellisen vaihenumeron suuruus riippuu valinta-
	// tai toistolauseen ehdoista. Funktiota kutsutaan painikkeen
	// "Edellinen" kautta, jos funktiossa ”paivita” muuttujan
	// ”toiminto” arvo on 1, eikä esteitä ole.
}
function seurvaihe(seurvaih) {
	if (vaihe < max) vaihe = seurvaih;
	
	console.log('seurvaihe - vaihe: ' + vaihe);
	// Palauttaa normaalitilanteessa yhden vaiheen taaksepäin.
	// Joskus edellisen vaihenumeron suuruus riippuu valinta-
	// tai toistolauseen ehdoista. Funktiota kutsutaan painikkeen
	// "Seuraava" kautta, jos funktiossa ”paivita” muuttujan
	// ”toiminto” arvo on 2, eikä esteitä ole.
}
function merkinta() {
	if (esto == false) {
		var luku = vaihe.toString();
		alkuperSisalto = document.getElementById(luku).innerHTML;
		document.getElementById(luku).innerHTML = "<span style='color: FF6600;'>" + document.getElementById(luku).innerHTML;
		// Siirtää värimerkinnän esimerkkikoodin
		// vaiheesta toiseen vaiheeseen, jos
		// sitä ei ole estetty.
		}
}
function tarkista(arvo, vastaus) {
	tarkistus = arvo;
	if (arvo == true) {
		document.getElementById("palaute").innerHTML = document.getElementById("palaute").innerHTML + "<span style='color:green;'>" + vastaus + "</span>";
		}
	else document.getElementById("palaute").innerHTML = document.getElementById("palaute").innerHTML + "<span style='color:red;'>" + vastaus + "</span>";
	console.log('tarkista - tarkistus: ' + tarkistus);
	// Merkitsee HTML-dokumentin palaute-osioon ilmoituksen siitä miten tehtävä onnistui.
}
function uusiTeht(min, max) {
	tarkistus = false;
	document.getElementById("palaute").innerHTML = "";
	console.log('uusiTeht - tarkistus: ' + tarkistus);
	// Korvataan uusi tehtävä vanhalla arpomalla numero,
	// joka palauttaessaan arvon HTML-tiedostoon
	// määrittää tehtävän ehdon.
	
	return Math.floor(Math.random() * (max - min + 1) + min);
}
import React from 'react';
import { SvgIcon } from '@material-ui/core';

export const AlphabetSortIcon = (props) => {
    return (
        <SvgIcon {...props}>
            <path d="sort-icon-1" />
        </SvgIcon>
    );
}

export const AlphabetReverseSortIcon = (props) => {
    return (
        <SvgIcon {...props}>
            <path d="sort-icon-2" />
        </SvgIcon>
    );
}
import React from 'react';
import { Box, GridList, GridListTile, GridListTileBar } from '@material-ui/core';

export const ItemView = (props) => {
    return(
        <Box>
            {props.mode === 'list' && 
                <div>
                    {props.items.map((item, index) => <div>{item.title}</div>)}
                </div>
            }
            {props.mode === 'table' &&
                <GridList cols={3}>
                    {props.items.map((item, index) => 
                        <GridListTile>
                            {item.images.length > 0 && <img src={item.images[0].links.medium} alt={item.title} />}
                            <GridListTileBar
                                title={item.title}
                            />
                        </GridListTile>
                        )
                    }
                </GridList>
            }
        </Box>
    );
}
export default ItemView;
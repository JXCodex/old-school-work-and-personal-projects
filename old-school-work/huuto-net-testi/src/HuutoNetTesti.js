import React, { useEffect, useState } from 'react';
import { AppBar, Button, ButtonGroup, IconButton, TextField, Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import TreeItem from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import './App.css';
import ItemView from './ItemView';
import GridOnIcon from '@material-ui/icons/GridOn';
import ListIcon from '@material-ui/icons/List';
import SortIcon from '@material-ui/icons/Sort';
import SearchIcon from '@material-ui/icons/Search';
// import { AlphabetSortIcon, AlphabetReverseSortIcon } from './Components';

const useStyles = makeStyles({
    root: {
        height: 240,
        flexGrow: 1,
        maxWidth: 400
    }
});

export const HuutoNetTesti = () => {
    const classes = useStyles();

    const [tree, setTree] = useState({
        id: "0",
        label: "Kategoriat",
        categories: [],
    });

    const [items, setItems] = useState(null);
    const [mode, setMode] = useState('list');
    const [searchword, setSearchword] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function haeDataa() {
            let data = [];
            var i = 1;
            while (i <= 50) {
                let result = await axios('https://api.huuto.net/1.1/categories/' + i);
                data.push(JSON.parse(result.request.response));
                i++;

                // Tämä on osa keskeneräistä ominaisuutta hakea data yhteen taulukkoon usean yksittäisen datamuuttujan sijasta.
                /*
                await axios = ('https://api.huuto.net/1.1/categories/' + (i - 1) + '/subcategories');
                while (j <= 50) {
                    j++;
                }
                */
            }

            let result001 = await axios('https://api.huuto.net/1.1/categories/1');
            let r001Data = JSON.parse(result001.request.response);
            let result002 = await axios('https://api.huuto.net/1.1/categories/2');
            let r002Data = JSON.parse(result002.request.response);
            let result003 = await axios('https://api.huuto.net/1.1/categories/3');
            let r003Data = JSON.parse(result003.request.response);
            let result004 = await axios('https://api.huuto.net/1.1/categories/4');
            let r004Data = JSON.parse(result004.request.response);
            let result005 = await axios('https://api.huuto.net/1.1/categories/5');
            let r005Data = JSON.parse(result005.request.response);
            let result006 = await axios('https://api.huuto.net/1.1/categories/6');
            let r006Data = JSON.parse(result006.request.response);
            let result007 = await axios('https://api.huuto.net/1.1/categories/7');
            let r007Data = JSON.parse(result007.request.response);
            let result008 = await axios('https://api.huuto.net/1.1/categories/8');
            let r008Data = JSON.parse(result008.request.response);
            let result009 = await axios('https://api.huuto.net/1.1/categories/9');
            let r009Data = JSON.parse(result009.request.response);
            let result010 = await axios('https://api.huuto.net/1.1/categories/10');
            let r010Data = JSON.parse(result010.request.response);
            let result011 = await axios('https://api.huuto.net/1.1/categories/11');
            let r011Data = JSON.parse(result011.request.response);
            let result012 = await axios('https://api.huuto.net/1.1/categories/12');
            let r012Data = JSON.parse(result012.request.response);
            let result013 = await axios('https://api.huuto.net/1.1/categories/13');
            let r013Data = JSON.parse(result013.request.response);
            let result014 = await axios('https://api.huuto.net/1.1/categories/14');
            let r014Data = JSON.parse(result014.request.response);
            let result015 = await axios('https://api.huuto.net/1.1/categories/15');
            let r015Data = JSON.parse(result015.request.response);
            let result016 = await axios('https://api.huuto.net/1.1/categories/16');
            let r016Data = JSON.parse(result016.request.response);
            let result017 = await axios('https://api.huuto.net/1.1/categories/17');
            let r017Data = JSON.parse(result017.request.response);
            let result018 = await axios('https://api.huuto.net/1.1/categories/18');
            let r018Data = JSON.parse(result018.request.response);
            let result019 = await axios('https://api.huuto.net/1.1/categories/19');
            let r019Data = JSON.parse(result019.request.response);
            let result020 = await axios('https://api.huuto.net/1.1/categories/20');
            let r020Data = JSON.parse(result020.request.response);
            let result021 = await axios('https://api.huuto.net/1.1/categories/21');
            let r021Data = JSON.parse(result021.request.response);
            let result022 = await axios('https://api.huuto.net/1.1/categories/22');
            let r022Data = JSON.parse(result022.request.response);
            let result023 = await axios('https://api.huuto.net/1.1/categories/23');
            let r023Data = JSON.parse(result023.request.response);
            let result025 = await axios('https://api.huuto.net/1.1/categories/25');
            let r025Data = JSON.parse(result025.request.response);
            let result026 = await axios('https://api.huuto.net/1.1/categories/26');
            let r026Data = JSON.parse(result026.request.response);
            let result024 = await axios('https://api.huuto.net/1.1/categories/24');
            let r024Data = JSON.parse(result024.request.response);
            let result027 = await axios('https://api.huuto.net/1.1/categories/27');
            let r027Data = JSON.parse(result027.request.response);
            let result028 = await axios('https://api.huuto.net/1.1/categories/28');
            let r028Data = JSON.parse(result028.request.response);
            let result029 = await axios('https://api.huuto.net/1.1/categories/29');
            let r029Data = JSON.parse(result029.request.response);
            let result030 = await axios('https://api.huuto.net/1.1/categories/30');
            let r030Data = JSON.parse(result030.request.response);
            let result031 = await axios('https://api.huuto.net/1.1/categories/31');
            let r031Data = JSON.parse(result031.request.response);
            let result032 = await axios('https://api.huuto.net/1.1/categories/32');
            let r032Data = JSON.parse(result032.request.response);
            let result033 = await axios('https://api.huuto.net/1.1/categories/33');
            let r033Data = JSON.parse(result033.request.response);
            let result034 = await axios('https://api.huuto.net/1.1/categories/34');
            let r034Data = JSON.parse(result034.request.response);
            let result035 = await axios('https://api.huuto.net/1.1/categories/35');
            let r035Data = JSON.parse(result035.request.response);
            let result036 = await axios('https://api.huuto.net/1.1/categories/36');
            let r036Data = JSON.parse(result036.request.response);
            let result037 = await axios('https://api.huuto.net/1.1/categories/37');
            let r037Data = JSON.parse(result037.request.response);
            let result046 = await axios('https://api.huuto.net/1.1/categories/46');
            let r046Data = JSON.parse(result046.request.response);
            let result047 = await axios('https://api.huuto.net/1.1/categories/47');
            let r047Data = JSON.parse(result047.request.response);
            let result048 = await axios('https://api.huuto.net/1.1/categories/48');
            let r048Data = JSON.parse(result048.request.response);
            let result049 = await axios('https://api.huuto.net/1.1/categories/49');
            let r049Data = JSON.parse(result049.request.response);
            let result050 = await axios('https://api.huuto.net/1.1/categories/50');
            let r050Data = JSON.parse(result050.request.response);
            setTree({
                id: "0",
                label: "Kategoriat",
                categories: [
                    {
                        id: (r001Data.id).toString(), label: r001Data.title, categories: [
                            {
                                id: (r002Data.id).toString(), label: r002Data.title, categories: [
                                    {
                                        id: (r003Data.id).toString(), label: r003Data.title
                                    },
                                    {
                                        id: (r004Data.id).toString(), label: r004Data.title
                                    },
                                    {
                                        id: (r005Data.id).toString(), label: r005Data.title
                                    },
                                    {
                                        id: (r006Data.id).toString(), label: r006Data.title
                                    },
                                    {
                                        id: (r007Data.id).toString(), label: r007Data.title
                                    },
                                ],
                            },
                            {
                                id: (r008Data.id).toString(), label: r008Data.title, categories: [
                                    {
                                        id: (r009Data.id).toString(), label: r009Data.title
                                    },
                                    {
                                        id: (r010Data.id).toString(), label: r010Data.title
                                    },
                                    {
                                        id: (r011Data.id).toString(), label: r011Data.title
                                    },
                                    {
                                        id: (r012Data.id).toString(), label: r012Data.title
                                    },
                                    {
                                        id: (r013Data.id).toString(), label: r013Data.title
                                    },
                                    {
                                        id: (r014Data.id).toString(), label: r014Data.title
                                    },
                                ],
                            },
                            {
                                id: (r015Data.id).toString(), label: r015Data.title, categories: [
                                    {
                                        id: (r016Data.id).toString(), label: r016Data.title
                                    },
                                    {
                                        id: (r017Data.id).toString(), label: r017Data.title
                                    },
                                    {
                                        id: (r018Data.id).toString(), label: r018Data.title
                                    },
                                    {
                                        id: (r019Data.id).toString(), label: r019Data.title
                                    },
                                    {
                                        id: (r020Data.id).toString(), label: r020Data.title
                                    },
                                ],
                            },
                            {
                                id: (r021Data.id).toString(), label: r021Data.title, categories: [
                                    {
                                        id: (r022Data.id).toString(), label: r022Data.title
                                    },
                                    {
                                        id: (r023Data.id).toString(), label: r023Data.title
                                    },
                                    {
                                        id: (r025Data.id).toString(), label: r025Data.title
                                    },
                                    {
                                        id: (r026Data.id).toString(), label: r026Data.title
                                    },
                                    {
                                        id: (r024Data.id).toString(), label: r024Data.title
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        id: (r027Data.id).toString(), label: r027Data.title, categories: [
                            {
                                id: (r028Data.id).toString(), label: r028Data.title, categories: [
                                    {
                                        id: (r029Data.id).toString(), label: r029Data.title
                                    },
                                    {
                                        id: (r030Data.id).toString(), label: r030Data.title
                                    },
                                    {
                                        id: (r031Data.id).toString(), label: r031Data.title
                                    },
                                    {
                                        id: (r032Data.id).toString(), label: r032Data.title
                                    },
                                ],
                            },
                            {
                                id: (r046Data.id).toString(), label: r046Data.title, categories: [
                                    {
                                        id: (r047Data.id).toString(), label: r047Data.title
                                    },
                                    {
                                        id: (r048Data.id).toString(), label: r048Data.title
                                    },
                                    {
                                        id: (r049Data.id).toString(), label: r049Data.title
                                    },
                                    {
                                        id: (r050Data.id).toString(), label: r050Data.title
                                    },
                                ]
                            },
                            {
                                id: (r033Data.id).toString(), label: r033Data.title, categories: [
                                    {
                                        id: (r034Data.id).toString(), label: r034Data.title
                                    },
                                    {
                                        id: (r035Data.id).toString(), label: r035Data.title
                                    },
                                    {
                                        id: (r037Data.id).toString(), label: r037Data.title
                                    },
                                    {
                                        id: (r036Data.id).toString(), label: r036Data.title
                                    },
                                ]
                            },
                        ],
                    },
                ],
            });
        }
        haeDataa();
    }, [tree]);
    
    const renderTree = (nodes) => (
        <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.label} onClick={() => {getItems(nodes.id)}}>
            {Array.isArray(nodes.categories) ? nodes.categories.map((node) => renderTree(node)) : null}
        </TreeItem>
    );

    async function getItems(id) {
        setLoading(true);
        let result = await axios('https://api.huuto.net/1.1/categories/' + id + '/items');
        setLoading(false);
        let rData = JSON.parse(result.request.response);
        if (rData.items !== null) {
            setItems(rData.items);
        }
    }

    async function getSearchwordItems(word) {
        setLoading(true);
        let result = await axios('https://api.huuto.net/1.1/items/words/' + word);
        setLoading(false);
        let rData = JSON.parse(result.request.response);
        setItems(rData.items);
    }

    function Rotating() {
        return (<svg className="loading" viewBox="0 0 100 100"><g>
          <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" dur="5s" repeatCount="indefinite" />
          <circle cx="50" cy="50" r="35" />
          <polygon points="50,3 50,27 65,15" />
          <polygon points="50,97 50,72 35,85" />
        </g></svg>);
    }
    
    return (
        <React.Fragment>
            <AppBar>
                <Toolbar className="AppBar">
                    <ButtonGroup color="inherit" variant="text">
                        <Button startIcon={<SortIcon />} onClick={() => {setItems(items.sort((a, b) => parseInt(a.id) - parseInt(b.id)))}}>1, 2, 3...</Button>
                        <Button startIcon={<SortIcon />} onClick={() => {setItems(items.sort((a, b) => parseInt(b.id) - parseInt(a.id)))}}>...3, 2, 1</Button>
                        <Button startIcon={<SortIcon />} onClick={() => {setItems(items.sort(function(a, b) { 
                            return ('' + a.title).localeCompare(b.title);
                        }))}}>A, B, C...</Button>
                        <Button startIcon={<SortIcon />} onClick={() => {setItems(items.sort(function(a, b) { 
                            return ('' + b.title).localeCompare(a.title);
                        }))}}>...C, B, A</Button>
                        <Button startIcon={<GridOnIcon />} onClick={() => {setMode('table')}}>Taulukko</Button>
                        <Button startIcon={<ListIcon />} onClick={() => {setMode('list')}}>Lista</Button>
                    </ButtonGroup>
                    <TextField id="searchwordField" defaultValue={searchword} label="Hae kohteita" variant="outlined" onChange={(event) => setSearchword(event.target.value)}>
                    </TextField>
                    <IconButton color="inherit" onClick={() => {getSearchwordItems(searchword)}}>
                        <SearchIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <br /><br /><br />
            <div className="row">
                <div className="column" style={{ width: '50vh', height: '100vh' }}>
                    <TreeView
                        className={classes.root}
                        defaultCollapseIcon={<ExpandMoreIcon />}
                        defaultExpanded={["0"]}
                        defaultExpandIcon={<ChevronRightIcon />}
                    >
                        {renderTree(tree)}
                    </TreeView>
                </div>
                <div className="column" style={{ width: '10vh', height: '100vh' }} />
                <div className="column" style={{ width: '50vh', height: '100vh' }}>
                    {loading && <Rotating key="loading" size={30} />}
                    {items !== null && <ItemView items={items} mode={mode} />}
                </div>
            </div>
        </React.Fragment>
    );
}
export default HuutoNetTesti;
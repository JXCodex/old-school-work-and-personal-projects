import './CityAndTower.css';

import { BadgeList, SpecialBadgeList, SpecialSpecialBadgeList } from './BadgeList';
import { Container, Link, Typography } from '@mui/material';
import React, { useState } from 'react';

import FieldData from './data/FieldData.json';
import { FieldRules } from './FieldRules';
import { Fields } from './Fields';
import PlayersData from './data/PlayersData.json';
import SeasonsData from './data/SeasonsData.json';
import { TypeDetails } from './TypeDetails';

export const CitySeasons = (props) => {
    // CitySeasons sisältää koodin, joka määrittää miltä seutukunnan oman League Cityn nykyisen kauden aikataulua, sääntöjä ja yksityiskohtia sisältävä sivu näyttää.
    const [subtab, setSubtab] = useState('mainSubtab');
    const [subtabType, setSubtabType] = useState('mainSubtab');

    let currentDate = new Date();
    const [year, setYear] = useState(currentDate.getFullYear());
    let badgeStartDate;
    let badgeEndDate;
    let cupStartDate;
    let cupEndDate;
    let cupField = null;
    let playerCounter = 0;
    let fullPlayerCounter = 0;

    let yearList = [];
    let fieldList = [];

    let seasonExists = false;

    const checkIfFileExists = (path) => {
        try {
         return require(`${path}`);
        } catch (err) {
         return null;
        }
    };

    if (year > 2022) setYear(2022);

    for (var i = 0; i < PlayersData.users.length; i++) {
        for (var j = 0; j < PlayersData.users[i].seasons.length; j++) {
            if (PlayersData.users[i].seasons[j].year === year) {
                if (PlayersData.users[i].seasons[j].group !== 'ei jatkoon') {
                    fullPlayerCounter++;
                }
            }
        }    
    }
    
    if (props.city === 'tampere') {
        for (i = 0; i < SeasonsData.seasons.tampere.length; i++) {
            yearList.push(SeasonsData.seasons.tampere[i].season);

            if (SeasonsData.seasons.tampere[i].season === year) {
                seasonExists = true;
                badgeStartDate = new Date(SeasonsData.seasons.tampere[i].badgeStartDate);
                badgeEndDate = new Date(SeasonsData.seasons.tampere[i].badgeEndDate);
                cupStartDate = new Date(SeasonsData.seasons.tampere[i].cupStartDate);
                cupEndDate = new Date(SeasonsData.seasons.tampere[i].cupEndDate);
                cupField = SeasonsData.seasons.tampere[i].cupField;
            }
        }

        for (i = 0; i < FieldData.fields.length; i++) {
            for (j = 0; j < FieldData.fields[i].year.length; j++) {
                if (FieldData.fields[i].year[j] === year) {
                    fieldList.push(FieldData.fields[i].engName);
                }
            }
        }
    }

    const renderPlayer = (player) => {

        let textColor = '#000000';

        if (player.team === 'Instinct') textColor = '#D5D500';
        else if (player.team === 'Mystic') textColor = '#4040FF';
        else if (player.team === 'Valor') textColor = '#FF0000';

        for (var i = 0; i < player.seasons.length; i++) {
            if (player.seasons[i].year === year) {
                if (player.seasons[i].group !== 'ei jatkoon') {
                    playerCounter++;

                    if (playerCounter > 1 && playerCounter === fullPlayerCounter) return(<font color={textColor}> <font color={props.theme.palette.primary.main}>ja</font> {player.username}</font>);
                    else if (playerCounter > 1) return(<font color={textColor}><font color={props.theme.palette.primary.main}>,</font> {player.username}</font>);
                    
                    return(<font color={textColor}> {player.username}</font>);
                }
            }

        }
        return(<font />);
    }

    const renderChosenField = (field) => {

        for (var i = 0; i < field.year.length; i++) {
            if (field.year[i] === year && field.code === cupField) {
                return(<font className="Site-body-text" face="Segoe UI Semibold" color={field.textColor}> {field.name.toUpperCase()}</font>);
            }
        }
        return(<font />);
    }

    const revealHighPlayer = (seasonTitle) => {

        let titleCarrier = null;
        let team = null;
        for (var i = 0; i < PlayersData.users.length; i++) {
            for (var j = 0; j < PlayersData.users[i].seasons.length; j++) {
                if (PlayersData.users[i].seasons[j].year === year && PlayersData.users[i].seasons[j].finalPlace === seasonTitle) {
                    titleCarrier = PlayersData.users[i].username;
                    team = PlayersData.users[i].team;
                }
            }
        }

        if (titleCarrier !== null && team !== null) {
            
            let teamColor = null;

            if (team === 'Valor') teamColor = '#FF0000';
            else if (team === 'Mystic') teamColor = '#4040FF';
            else if (team === 'Instinct') teamColor = '#FFFF00';
            
            return(
                <font className="Site-body-mid-text" color={teamColor}>
                    {titleCarrier}
                </font>
            );
        }

        return (<font />);
    }

    return(
        <Container>
            {subtab === 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF', color: props.theme.palette.primary.main }}>
                    <font className="Site-body-header">
                        TURNAUS
                    </font>
                    <br /><br />
                    {props.city === 'tampere' && <font className="Site-body-mid-header">
                        KAUSI {year}
                    </font>}
                    {props.city === 'tampere' && <font color={props.theme.palette.primary.dark}>
                        <br /><br />
                        {year > yearList[1] && <font>
                            <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); setYear(year - 1)}}>
                                <font color={props.theme.palette.primary.dark}>KAUSI {year - 1}</font>
                            </Link> 
                            <font> {'≪'} </font>
                        </font>}
                        <Link className="Site-body-link" component="button">
                            <font face="Segoe UI Black" color={props.theme.palette.primary.dark}> KAUSI {year} </font>
                        </Link>
                        {year < yearList[yearList.length - 1] && <font>
                            <font> {'≫'} </font>
                            <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); setYear(year + 1)}}>
                                <font color={props.theme.palette.primary.dark}>KAUSI {year + 1}</font>
                            </Link>
                        </font>}
                        <br /><br />
                    </font>}
                    {props.city === 'helsinki' && <font className="Site-body-text">Helsingin League Metropoliksen turnauskausi</font>}   
                    {props.city === 'lahti' && <font className="Site-body-text">Lahden League Cityn turnauskausi</font>}
                    {props.city === 'tampere' && <font className="Site-body-text">Tampereen League Cityn turnauskausi</font>}
                    {props.city === 'turku' && <font className="Site-body-text">Turun League Cityn turnauskausi</font>}
                    {props.city !== 'helsinki' && props.city !== 'lahti' && props.city !== 'tampere' && props.city !== 'turku' && <font className="Site-body-text">{props.fixedCity}n League Cityn turnauskausi</font>}

                    {props.city === 'tampere' && <font className="Site-body-text">
                        {' '}{year}{' '}
                    </font>}
                    <font className="Site-body-text">
                        koostuu kahdesta vaiheesta: haasteesta nimeltä The Badge Challenge sekä lopputurnauksesta nimeltä The Champion Cup, johon saavat kutsun The Badge Challengen suorittaneet pelaajat. Kun olet suorittanut The Badge Challengen, voit valita yhden seutukunnan League Cityn (tai Helsingin tapauksessa League Metropoliksen), jonka The Champion Cupiin osallistut tällä kaudella. Tällä hetkellä Tampere on kuitenkin ainut kaupunki, jolla on oma League City, joten et voi vielä osallistua missään muualla.
                    </font>
                    <br /><br />
                    {props.city === 'helsinki' && 
                        <font className="Site-body-text">
                            Helsingin seutukuntaan kuuluvat Helsingin lisäksi myös Espoo, Hyvinkää, Järvenpää, Karkkila, Kauniainen, Kerava, Kirkkonummi, Lohja, Mäntsälä, Nurmijärvi, Pornainen, Sipoo, Siuntio, Tuusula, Vantaa ja Vihti, joten jos voitat Helsingin paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden 17 paikkakunnan
                        </font>
                    }
                    {props.city === 'joensuu' && 
                        <font className="Site-body-text">
                            Joensuun seutukuntaan kuuluvat Joensuun lisäksi myös Ilomantsi, Juuka, Kontiolahti, Liperi, Outokumpu ja Polvijärvi, joten jos voitat Joensuun paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden seitsemän paikkakunnan
                        </font>
                    }
                    {props.city === 'jyvaskyla' && 
                        <font className="Site-body-text">
                            Jyväskylän seutukuntaan kuuluvat Jyväskylän lisäksi myös Hankasalmi, Laukaa, Muurame, Petäjävesi, Toivakka ja Uurainen, joten jos voitat Jyväskylän paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden seitsemän paikkakunnan
                        </font>
                    }
                    {props.city === 'kuopio' && 
                        <font className="Site-body-text">
                            Kuopion seutukuntaan kuuluu Kuopion lisäksi myös Siilinjärvi, joten jos voitat Kuopion paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden molempien paikkakuntien
                        </font>
                    }
                    {props.city === 'lahti' && 
                        <font className="Site-body-text">
                            Lahden seutukuntaan kuuluvat Lahden lisäksi myös Asikkala, Hartola, Heinola, Hollola, Kärkölä, Orimattila, Padasjoki ja Sysmä, joten jos voitat Lahden paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden yhdeksän paikkakunnan
                        </font>
                    }
                    {props.city === 'pori' && 
                        <font className="Site-body-text">
                            Porin seutukuntaan kuuluvat Porin lisäksi myös Harjavalta, Huittinen, Kokemäki, Merikarvia, Nakkila, Pomarkku ja Ulvila, joten jos voitat Porin paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden kahdeksan paikkakunnan
                        </font>
                    }
                    {props.city === 'tampere' && 
                        <font className="Site-body-text">
                            Tampereen seutukuntaan kuuluvat Tampereen lisäksi myös Hämeenkyrö, Kangasala, Lempäälä, Nokia, Orivesi, Pirkkala, Pälkäne, Vesilahti ja Ylöjärvi, joten jos voitat Tampereen paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden 10 paikkakunnan
                        </font>
                    }
                    {props.city === 'turku' && 
                        <font className="Site-body-text">
                            Turun seutukuntaan kuuluvat Turun lisäksi myös Kaarina, Lieto, Masku, Mynämäki, Naantali, Nousiainen, Paimio, Raisio, Rusko ja Sauvo, joten jos voitat Turun paikallisen The Champion Cupin, sinusta tulee seuraavaan kauteen asti näiden 11 paikkakunnan
                        </font>
                    }
                    <font className="Site-body-text">
                        {' '}liigamestari (eng. League Champion) ja toiseksi, kolmanneksi ja neljänneksi tulleista pelaajista näiden paikkakuntien eliittikolmoset (eng. Elite Three).
                    </font>
                    <br /><br />
                    <font className="Site-body-mid-header">
                        THE BADGE CHALLENGE
                    </font>
                    <br />
                    <br />
                    {props.city === 'tampere' && <font className="Site-body-text">Tampereen League City -kauden {currentDate.getFullYear()} The Badge Challenge </font>}

                    {/* Jos The Badge Challenge ei ole vielä alkanut. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() < badgeStartDate.getTime() && <font className="Site-body-text">alkaa </font>}
                    {/* Jos The Badge Challenge taas on jo alkanut. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() >= badgeStartDate.getTime() && <font className="Site-body-text">alkoi </font>}
                    {/* Jos The Badge Challengellä ei ole alkamispäivämäärää. */ seasonExists === false && <font className="Site-body-text">on tulossa pian!</font>}

                    {/* Alla oleva koodi määrittää tämän vuoden The Badge Challengen alkamispäivämäärän näkymän. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text">{badgeStartDate.getDate()}.{badgeStartDate.getMonth() + 1}.{badgeStartDate.getFullYear()} klo </font>}
                    {props.city === 'tampere' && seasonExists === true && badgeStartDate.getHours() < 10 && <font className="Site-body-text">0</font> /* Jos tuntiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text">{badgeStartDate.getHours()}:</font> /* Asetetaan samalla ":" tuntien ja minuuttien väliin. */}
                    {props.city === 'tampere' && seasonExists === true && badgeStartDate.getMinutes() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos minuuttiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text">{badgeStartDate.getMinutes()} ja </font>}

                    {/* Jos The Badge Challenge ei ole vielä päättynyt. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() < badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>tulee päättymään </font>}
                    {/* Jos The Badge Challenge taas on jo päättynyt. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() >= badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>päättyi </font>}

                    {/* Alla oleva koodi määrittää tämän vuoden The Badge Challengen päättymispäivämäärän näkymän. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{badgeEndDate.getDate()}.{badgeEndDate.getMonth() + 1}.{badgeEndDate.getFullYear()} klo </font>}
                    {props.city === 'tampere' && seasonExists === true && badgeEndDate.getHours() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos tuntiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{badgeEndDate.getHours()}:</font> /* Asetetaan samalla ":" tuntien ja minuuttien väliin. */}
                    {props.city === 'tampere' && seasonExists === true && badgeEndDate.getMinutes() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos minuuttiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{badgeEndDate.getMinutes()}.</font>}

                    {/* Jos The Badge Challenge taas on jo päättynyt. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() >= badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}> Luettelo jatkoon päässeistä pelaajista löytyy The Badge Challengen sääntöjen lopusta.</font>}
                    
                    {props.city === 'tampere' && seasonExists === true && <br />}
                    {props.city === 'tampere' && seasonExists === true && <br />}
                    {checkIfFileExists('./resources/posters/' + year.toString() + '/the-badge-challenge-' + props.city + '.png') !== null && seasonExists === true && <img src={require('./resources/posters/' + year.toString() + '/the-badge-challenge-' + props.city + '.png')} className="seasonRules" alt="badge-challenge" />}

                    {checkIfFileExists('./resources/posters/' + year.toString() + '/the-badge-challenge-' + props.city + '.png') === null && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>EI JULISTETTA JULKAISTU</font>}
                    
                    <br /><br />
                    <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>SÄÄNNÖT</font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        The Badge Challenge on jokaisen{' '}
                        {props.city !== 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>League City</font>}
                        {props.city === 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>League Metropolis</font>}
                        {' '}-kauden ensimmäinen vaihe. Säännöt ovat seuraavat:
                        <ul>
                            <li>Kerää virtuaalimerkkejä haastamalla ihmisiä Great League (jossa CP enintään 1500) -säännöillä PVP-otteluihin ja voittamalla näitä otteluita.</li>
                            <li>Vastustajasi on käytettävä vain merkin vaatimuksen mukaisia pokémoneja:</li>
                            <ul>
                                <li>18 perusmerkin (tyyppikohtaisia merkkejä kuten ruoho, tuli, vesi jne.) tapauksessa vain merkin nimessä mainitun tyypin pokémoneja (kaksityyppisetkin käy, kunhan toinen tyypeistä on se nimikkotyyppi).</li>
                                <li>4 erikoismerkin (joista jokainen on 1,5 kertaa yhden perusmerkin arvoinen) tapauksessa vain niiden määrittelemiä pokémonryhmiä (vauvapokémonit, aluerajoitetut pokémonit, Eeveen kehitysmuodot tai fossiilipokémonit).</li>
                                {props.city !== 'helsinki' && year === 2020 && <li>Kiiltomerkistä (joka on 2 kertaa yhden perusmerkin arvoinen) saa otella vain The Badge Challengen aikana järjestetyissä League Cityn sivutapahtumissa. Kiiltomerkkihaasteessa vastustajasi voi käyttää vain "kiiltäviä" (eng. Shiny) eli harvinaisia erivärisiä yksilöitään.</li>}
                                {props.city === 'helsinki' && year === 2020 && <li>Kiiltomerkistä (joka on 2 kertaa yhden perusmerkin arvoinen) saa otella vain The Badge Challengen aikana järjestetyissä League Metropoliksen sivutapahtumissa. Kiiltomerkkihaasteessa vastustajasi voi käyttää vain "kiiltäviä" (eng. Shiny) eli harvinaisia erivärisiä yksilöitään.</li>}
                                {props.city !== 'helsinki' && year > 2020 && <li>Kiiltomerkistä, Varjomerkistä ja Megamerkistä (jotka ovat 2 kertaa yhden perusmerkin arvoisia) saa otella vain The Badge Challengen aikana järjestetyissä League Cityn sivutapahtumissa. Kiiltomerkkihaasteessa vastustajasi voi käyttää vain "kiiltäviä" (eng. Shiny) eli harvinaisia erivärisiä yksilöitään ja Varjomerkkihaasteessa varjopokémoneja (eng. Shadow Pokémon) eli GO-Rakettiryhmän (eng. Team GO Rocket) jäseniltä pyydystettyjä aggressiivisia punasilmäisiä ja purppuran auran peitossa olevia yksilöitä. Megamerkkihaasteessa vastustajasi on käytettävä yhtä megakehitettyä pokémonia.</li>}
                                {props.city === 'helsinki' && year > 2020 && <li>Kiiltomerkistä, Varjomerkistä ja Megamerkistä (jotka ovat 2 kertaa yhden perusmerkin arvoisia) saa otella vain The Badge Challengen aikana järjestetyissä League Metropoliksen sivutapahtumissa. Kiiltomerkkihaasteessa vastustajasi voi käyttää vain "kiiltäviä" (eng. Shiny) eli harvinaisia erivärisiä yksilöitään ja Varjomerkkihaasteessa varjopokémoneja (eng. Shadow Pokémon) eli GO-Rakettiryhmän (eng. Team GO Rocket) jäseniltä pyydystettyjä aggressiivisia punasilmäisiä ja purppuran auran peitossa olevia yksilöitä. Megamerkkihaasteessa vastustajasi on käytettävä yhtä megakehitettyä pokémonia.</li>}
                            </ul>
                            <li>Perusmerkkihaasteissa tai Kiiltomerkkihaasteessa voit itse käyttää aivan mitä pokémoneja tahansa, mutta erikoismerkeillä on omat ehtonsa.</li>
                            <ul>
                                {year === 2020 && <li>Perusmerkkihaasteissa tai Kiiltomerkkihaasteessa et saa kuitenkaan käyttää kahta saman lajin pokémonia.</li>}
                                {year > 2020 && <li>Perusmerkkihaasteissa tai Kiilto-, Varjo- ja Megamerkkihaasteissa et saa kuitenkaan käyttää kahta saman lajin pokémonia.</li>}
                                <li>Alolan ja Galarin variaatiot lasketaan eri lajeiksi, mutta esim. Castformin, Deoxysin, Wormadamin, Cherrimin ja muiden muotoja ei lasketa eri lajeiksi.</li>
                            </ul>
                            <li>Voit itse valita mitä merkkejä tavoittelet ja missä järjestyksessä, mutta voit tavoitella merkkejä enintään kolmessa ottelussa per päivä.</li>
                            <li>Sekä sinun että vastustajasi on ottelun jälkeen lähetettävä ottelun jälkeen tiedot pokémoneista, joita käytitte, sekä ilmoitettava samalla ottelun lopputulos, jotta tulos voidaan rekisteröidä ja merkki myöntää.</li>
                            <li>Jos voitat tietyn määrän vaadittuja merkkejä (tulostaulukosta näet, että kuinka monta vielä tarvitset, jos olet jo osallistunut), saat kutsun The Champion Cupiin, lopputurnaukseen, jossa pääset ottelemaan toisia kilpailijoita vastaan.</li>
                            <li>Vaadittujen merkkien määrä on seuraavanlainen:</li>
                            <ul>
                                <li>Uusien kilpailijoiden tai niiden, jotka eivät ole aiemmin suoriutuneet The Badge Challengestä aiemmilla kausilla, on kerättävä tällä kaudella kuusi merkkiä.</li>
                                <li>Niiden, jotka ovat suorittaneet The Badge Challengen vähintään yhdellä edellisistä kausista, on kerättävä tällä kaudella neljä merkkiä.</li>
                                <li>Edellisen kauden liigamestarin tai eliittikolmosten kyseiseltä seutukunnalta ei tarvitse kerätä uusia merkkejä. He saavat ilman niitäkin kutsun tämän kauden The Champion Cupiin samalle seutukunnalle.</li>
                            </ul>
                        </ul>
                    </font>
                    <br />
                    <div /* ref={badgeRef} */ className="Site-body-text" align="center">
                        <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>MERKIT</font>
                        <br /><br />
                        <font face="Segoe UI Semibold" color={props.theme.palette.primary.main} text-align="center">
                            TÄMÄN KAUDEN PERUSMERKIT
                            <br />
                            Klikkaa merkkiä katsoaksesi siitä tarkempia tietoja.
                            <br />
                            {year >= 2020 && <BadgeList subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} />}
                            <br /><br />
                            TÄMÄN KAUDEN ERIKOISMERKIT
                            <br />
                            Klikkaa merkkiä katsoaksesi siitä tarkempia tietoja.
                            <br />
                            {year >= 2020 && <SpecialBadgeList subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} />}
                            <br /><br />
                            TÄLLÄ KAUDELLA SAATAVILLA VAIN LEAGUE CITYN OHEISTAPAHTUMISSA
                            <br />
                            Klikkaa merkkiä katsoaksesi siitä tarkempia tietoja.
                            <br />
                            {year >= 2020 && <SpecialSpecialBadgeList subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} year={year} />}
                            <br />
                        </font>
                    </div>
                    {props.city === 'tampere' && seasonExists === true && currentDate.getTime() > badgeEndDate.getTime() && <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        <br />
                        THE BADGE CHALLENGEN LOPPUTULOS
                        <br /><br />
                        <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>Jatkoon pääsivät
                            {PlayersData.users.map((player) => renderPlayer(player))}.
                            <br /><br />
                        </font>
                    </font>}
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        THE CHAMPION CUP
                    </font>
                    <br /><br />
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}> Tampereen League City -kauden {year} The Champion Cup </font>}
                    {/* Jos The Champion Cup ei ole alkanut. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() < cupStartDate.getTime() && currentDate.getTime() < cupEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>pidetään </font>}
                    {/* Jos The Champion Cup on käynnissä. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() >= cupStartDate.getTime() && currentDate.getTime() < cupEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>on parhaillaan käynnissä ja kestää </font>}
                    {/* Jos The Champion Cup on päättynyt. */ props.city === 'tampere' && seasonExists === true && currentDate.getTime() >= cupStartDate.getTime() && currentDate.getTime() >= cupEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>pidettiin </font>}
                    {/* Alla oleva koodi määrittää tämän vuoden The Champion Cupin alkamispäivämäärän näkymän. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupStartDate.getDate()}.{cupStartDate.getMonth() + 1}.{cupStartDate.getFullYear()} klo </font>}
                    {props.city === 'tampere' && seasonExists === true && cupStartDate.getHours() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos tuntiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupStartDate.getHours()}:</font>/* Asetetaan samalla ":" tuntien ja minuuttien väliin. */}
                    {props.city === 'tampere' && seasonExists === true && cupStartDate.getMinutes() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos minuuttiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupStartDate.getMinutes()}</font>}

                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}> – </font>}
                    
                    {/* Alla oleva koodi määrittää tämän vuoden The Champion Cupin päättymispäivämäärän näkymän. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupEndDate.getDate()}.{cupEndDate.getMonth() + 1}.{cupEndDate.getFullYear()} klo </font>}
                    {props.city === 'tampere' && seasonExists === true && cupEndDate.getHours() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos tuntiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupEndDate.getHours()}:</font> /* Asetetaan samalla ":" tuntien ja minuuttien väliin. */}
                    {props.city === 'tampere' && seasonExists === true && cupEndDate.getMinutes() < 10 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>0</font> /* Jos minuuttiluku on pienempi kuin 10. */}
                    {props.city === 'tampere' && seasonExists === true && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>{cupEndDate.getMinutes()}.</font>}
                    {props.city === 'tampere' && seasonExists === true && <br />}
                    {props.city === 'tampere' && seasonExists === true && <br />}

                    {props.city === 'tampere' && seasonExists === true && year < 2023 && <img src={require('./resources/posters/' + year.toString() + '/the-champion-cup-' + props.city + '.png')} className="seasonRules" alt="champion-cup" />}
                    {props.city === 'tampere' && seasonExists === true && year >= 2023 && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>EI JULISTETTA JULKAISTU</font>}
                    {props.city === 'tampere' && seasonExists === true && <br />}
                    {props.city === 'tampere' && seasonExists === true && <br />}
                    
                    <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>SÄÄNNÖT</font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        The Champion Cup on jokaisen{' '}
                        {props.city !== 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>League City</font>}
                        {props.city === 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>League Metropolis</font>}
                        {' '}-kauden viimeinen vaihe. Säännöt ovat seuraavat:
                        <ul>
                            <li>Rakenna kuuden pokémonin "teematiimi" Great League (joissa CP enintään 1500) -säännöillä, mutta "kentän" mukaisilla pokémoneilla, joista legendaariset ja myyttiset lajit sekä varjopokémonit ja megakehitetyt pokémonit ovat kiellettyjä.</li>
                            <li>Kilpaile ensin alkukarsinnoissa pisteistä muiden samassa lohkossa kanssasi olevien pelaajien kanssa. Saat kustakin ottelusta pisteitä seuraavanlaisesti:</li>
                            <ul>
                                <li>4 pistettä, jos voitat vastustajasi kaikissa kolmessa erässä.</li>
                                <li>3 pistettä, jos voitat vastustajasi kahdessa erässä, mutta hän voittaa yhdessä erässä sinut.</li>
                                <li>2 pistettä, jos molemmat voittavat yhden erän, häviävät yhden erän ja kokevat yhden erän tasapelinä.</li>
                                <li>1 piste, jos häviät vastustajallesi kahdessa erässä, mutta voitat yhdessä erässä.</li>
                                <li>0 pistettä, jos häviät vastustajallesi kaikissa kolmessa erässä.</li>
                            </ul>
                            <li>Jokaisesta lohkosta yksi tai kaksi eniten pisteitä pelaajaa etenee neljännesfinaaleihin, lohkojen jakauman ja koon riippuessa osallistujamäärästä.</li>
                            <li>Neljännesfinaaleista alkaen otteluita käydään pudotuspeleinä single-elimination -muodossa, jossa voittaja etenee seuraavalle kierrokselle ja häviäjä putoaa turnauksesta.</li>
                            <ul>
                                <li>Neljännesfinaalit ja semifinaalit käydään kolmieräisinä, mutta pronssiottelu sekä kultaottelu (tai finaaliottelu) viisieräisinä.</li>
                                <li>Semifinaaleihin etenevät vain neljännesfinaaleissa voittaneet pelaajat.</li>
                                <li>Pronssiottelu käydään semifinaaleissa hävinneiden kesken.</li>
                                <li>Kultaottelu käydään semifinaaleissa voittaneiden kesken.</li>
                            </ul>
                            <li>Kultaottelun voittajasta tulee seutukunnan liigamestari ja kolmesta hänen jälkeensä parhaiten pelanneesta seutukunnan eliittikolmoset.</li>
                        </ul>
                    </font>
                    <br />
                    {props.city === 'tampere' && fieldList.length > 0 && <center>
                        <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>KENTÄT</font>
                        <br /><br />
                        {currentDate.getTime() < badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main} text-align="center">
                            TÄLLÄ KAUDELLA THE CHAMPION CUPIIN OVAT EHDOLLA NÄMÄ TEEMAKENTÄT
                        </font>}
                        {currentDate.getTime() >= badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main} text-align="center">
                            TÄLLÄ KAUDELLA THE CHAMPION CUPIIN OLIVAT EHDOLLA NÄMÄ TEEMAKENTÄT
                        </font>}
                        <br /><br />
                        <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main} text-align="center">
                            Klikkaa kenttää katsoaksesi siitä tarkempia tietoja.
                        </font>
                        <Fields color={props.theme.palette.primary.main} subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} year={year} />
                        {currentDate.getTime() >= badgeEndDate.getTime() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>Cupin ottelukentäksi valittiin:
                            <br />
                            {FieldData.fields.map((field) => renderChosenField(field))}
                            <br />
                        </font>}
                        {currentDate.getTime() >= cupEndDate.getTime() && <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            <br />
                            TURNAUKSEN LOPPUTULOS
                            <br />
                            <font className="Site-body-mid-text" face="Segoe UI Semibold" color='#DCAB01'>Kauden liigamestari: {revealHighPlayer('LIIGAMESTARI')}</font>
                            <br />
                            <font className="Site-body-mid-text" face="Segoe UI Semibold" color='#A1A1A1'>Kauden eliittikolmonen #1: {revealHighPlayer('ELIITTIKOLMONEN #1')}</font>
                            <br />
                            <font className="Site-body-mid-text" face="Segoe UI Semibold" color='#8D573A'>Kauden eliittikolmonen #2: {revealHighPlayer('ELIITTIKOLMONEN #2')}</font>
                            <br />
                            <font className="Site-body-mid-text" face="Segoe UI Semibold" color='#00AEF9'>Kauden eliittikolmonen #3: {revealHighPlayer('ELIITTIKOLMONEN #3')}</font>

                            <br /><br />
                            <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>Onnittelut voittajalle ja kolmelle muulle parhaalle! Ja kiitokset osallistumisesta myös neljännesfinaaleissa ja alkukarsinnoissa pudonneille!</font>
                            
                            {year === currentDate.getFullYear() && <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}><br /><br />KAUSI ON NYT SIIS PÄÄTTYNYT! MILLOIN ALKAA SEURAAVA?<br /></font>}
                            {year === currentDate.getFullYear() && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>Seuraavan kauden aikataulu paljastetaan tammikuussa {year + 1}.</font>}
                        </font>}
                    </center>}
                </Typography>
            }
            {subtab !== 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <Link component="button" onClick={() => {window.scrollTo(0, 0); setSubtab('mainSubtab'); setSubtabType('mainSubtab')}}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                                {'≪'} Palaa League Cityn sääntöihin
                            </center>
                        </font>
                    </Link>
                    <br /><br />
                    {subtabType === 'type' && <TypeDetails subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} type={subtab} city={props.city} cityTheme={props.theme} />}
                    {subtabType === 'field' && <FieldRules year={year} subtab={subtab} setSubtab={setSubtab} setSubtabType={setSubtabType} type={subtab} city={props.city} cityTheme={props.theme} />}
                </Typography>
            }
        </Container>
    );
}
export default CitySeasons;
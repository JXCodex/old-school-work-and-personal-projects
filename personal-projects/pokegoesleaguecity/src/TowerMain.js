import './CityAndTower.css';

import { Container, Link, Typography } from '@mui/material';

import React from 'react';

export const TowerMain = (props) => {
    // TowerMain sisältää koodin, joka määrittää miltä Battle Chateau Tower -etusivu näyttää.
    return(
        <Container> 
            <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF',  color: props.theme.palette.primary.main }}>
                <font className="Site-body-header">
                    Tervetuloa Battle Chateau Towerin nettisivuille
                </font>
                {props.login === true && 
                    <font className="Site-body-header">
                        , {props.username}
                    </font>
                }
                <font className="Site-body-header">
                    !
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Jos haluaa otella erilaisemmissa haasteissa ja joskus jopa voimakkaammilla pokémoneilla kuin League Cityssä, tämä tarjoaa mahdollisuuden otella Great Leaguen lisäksi myös Ultra League (jossa CP enintään 2500) -säännöillä. Vaikka Ultra League poistettiin League Cityn formaatista kaudella 2020, se tuodaan loppuvuodesta 2021 pelattavaksi Great Leaguen rinnalle toisessa formaatissa. Siinä missä League Cityssä kilpaillaan turnausmuodossa liigan mestarin (eng. Champion) tittelistä, Battle Chateau Towerissa käytävillä otteluilla vain nostamaan omaa arvoaan ja sijoitustaan saavuttamaan tornisuurherttuan (eng. Tower Grand Duke) tai tornisuurherttuattaren (eng. Tower Grand Duchess) titteli.
                </font>
                <br /><br />
                <font className="Site-body-mid-header">
                    SÄÄNNÖT
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Battle Chateau Tower on aatelisarvojen mukaan nimettyihin arvoihin perustuva ja järjestyvä ottelujärjestelmä. Ja sinulla on jokaisen League City -kauden loppuun asti aikaa nostaa arvoasi mahdollisimman korkealle. Jos saavutat tornisuurherttuan (eng. Tower Grand Duke) tai tornisuurherttuattaren (eng. Tower Grand Duchess) tittelin League Cityn ennen senaikaisen kauden päättymistä, saat etuja, jotka hyödyttävät sinua League Cityn seuraavalla kaudella, jos päätät Battle Chateau Towerin lisäksi osallistua sinnekin.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('rules')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Lue nykyisen ottelukauden aikataulu ja säännöt täältä {'≫'}
                    </font>
                </Link>
                <br /><br />
                <font className="Site-body-mid-header">
                    PVP-OPAS
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Opas Battle Chateau Towerissa tai PVP-otteluissa pärjäämiseen.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('PVPguide')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Lue täältä {'≫'}
                    </font>
                </Link>
                <br /><br />
                <font className="Site-body-mid-header">
                    PELAAJAT
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Luettelo Battle Chateau Towerissa otelleista pelaajista ja heidän profiilinsa.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('playerStats')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Katso täältä {'≫'}
                    </font>
                </Link>
            </Typography>
        </Container>
    );
}
export default TowerMain;
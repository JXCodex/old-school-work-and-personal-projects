// Tämä tiedosto on käytössä siihen asti, kunnes saan tietokannan tehdyksi.

import PokemonAlolaData from './data/pokemon/pokemonAlolaData.json';
import PokemonGalarData from './data/pokemon/pokemonGalarData.json';
import PokemonHisuiData from './data/pokemon/pokemonHisuiData.json';
import PokemonHoennData from './data/pokemon/pokemonHoennData.json';
import PokemonJohtoData from './data/pokemon/pokemonJohtoData.json';
import PokemonKalosData from './data/pokemon/pokemonKalosData.json';
import PokemonKantoData from './data/pokemon/pokemonKantoData.json';
import PokemonPaldeaData from './data/pokemon/pokemonPaldeaData.json';
import PokemonSinnohData from './data/pokemon/pokemonSinnohData.json';
import PokemonUnovaData from './data/pokemon/pokemonUnovaData.json';

let arrayData = [];

for (var i = 0; i < PokemonKantoData.pokemon.length; i++) {
    arrayData.push(PokemonKantoData.pokemon[i]);
}
for (i = 0; i < PokemonJohtoData.pokemon.length; i++) {
    arrayData.push(PokemonJohtoData.pokemon[i]);
}
for (i = 0; i < PokemonHoennData.pokemon.length; i++) {
    arrayData.push(PokemonHoennData.pokemon[i]);
}
for (i = 0; i < PokemonSinnohData.pokemon.length; i++) {
    arrayData.push(PokemonSinnohData.pokemon[i]);
}
for (i = 0; i < PokemonUnovaData.pokemon.length; i++) {
    arrayData.push(PokemonUnovaData.pokemon[i]);
}
for (i = 0; i < PokemonKalosData.pokemon.length; i++) {
    arrayData.push(PokemonKalosData.pokemon[i]);
}
for (i = 0; i < PokemonAlolaData.pokemon.length; i++) {
    arrayData.push(PokemonAlolaData.pokemon[i]);
}
for (i = 0; i < PokemonGalarData.pokemon.length; i++) {
    arrayData.push(PokemonGalarData.pokemon[i]);
}
for (i = 0; i < PokemonHisuiData.pokemon.length; i++) {
    arrayData.push(PokemonHisuiData.pokemon[i]);
}
for (i = 0; i < PokemonPaldeaData.pokemon.length; i++) {
    arrayData.push(PokemonPaldeaData.pokemon[i]);
}

export const FullPokemonList = arrayData;
import { Button } from '@mui/material';
import FieldData from './data/FieldData.json';
import React from 'react';

export const Fields = (props) => {

    let rowsOfFour = [];
    let row = [];
    
    for (var i = 0; i < FieldData.fields.length; i++) {
        for (var j = 0; j < FieldData.fields[i].year.length; j++) {
            if (FieldData.fields[i].year[j] === props.year) {
                if (FieldData.fields[i].code === "semiopen" && props.year === 2021) {
                }
                else {
                    if (row.length < 6) {
                        row.push(FieldData.fields[i].code);
                        row.push('space');
                    }
                    else {
                        row.push(FieldData.fields[i].code);
                        rowsOfFour.push(row);
                        row = [];
                    }
                }
            }
        }
    }
    if (row.length > 0) {
        rowsOfFour.push(row);
        row = [];
    }

    const renderField = (field) => {

        if (field === 'space') {
            return(
                <td align="center" width="15px" min-width="15px" max-width="15px" />
            );
        }

        let fieldCodeName = null;
        let fieldEngName = null;
        let fieldName = null;
        let textColor = null;
        
        for (var i = 0; i < FieldData.fields.length; i++) {
            if (FieldData.fields[i].code === field) {
                fieldCodeName = FieldData.fields[i].code;
                fieldEngName = FieldData.fields[i].engName;
                fieldName = FieldData.fields[i].name
                textColor = FieldData.fields[i].textColor;
            }
        }

        return(
            <td align="center" width="170px" min-width="170px" max-width="170px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(fieldCodeName); props.setSubtabType('field')}}>
                    <img src={require('./resources/fields/' + fieldCodeName + '.png')} className="Field" alt="field" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {fieldName.toUpperCase()}<br />{fieldEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    const renderFieldTableRow = (fieldTableRow) => {

        return(
            <tr height="140px">
                {fieldTableRow.map((field) => renderField(field))}
            </tr>
        );
    }

    const renderSemiopen = () => {

        let fieldCodeName = null;
        let fieldEngName = null;
        let fieldName = null;
        let textColor = null;

        for (var i = 0; i < FieldData.fields.length; i++) {
            if (FieldData.fields[i].code === "semiopen") {
                fieldCodeName = FieldData.fields[i].code;
                fieldName = FieldData.fields[i].name;
                fieldEngName = FieldData.fields[i].engName;
                textColor = FieldData.fields[i].textColor;
            }
        }


        return(
            <td align="center" colspan="7" width="170px" min-width="170px" max-width="170px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(fieldCodeName); props.setSubtabType('field')}}>
                    <img src={require('./resources/fields/' + fieldCodeName + '.png')} className="Field" alt="field" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {fieldName.toUpperCase()}<br />{fieldEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFour.length === 0 && row.length === 0) {
        return(<font />);
    }
    
    return(
        <table align="center">
            <tbody>
                {props.year === 2021 && <tr height="140px">
                    {renderSemiopen()}
                </tr>}
                {rowsOfFour.map((fieldTableRow) => renderFieldTableRow(fieldTableRow))}
            </tbody>
        </table>
    );
}
import { Box, Container, Typography } from '@mui/material';
import { CertainDualTypesCheck, CertainTypesCheck, TypeImmunityCheck, TypeResistanceCheck, TypeStrengthCheck, TypeWeaknessCheck } from './TypeCheck';

import PokemonAlolaData from './data/pokemon/pokemonAlolaData.json';
import PokemonGalarData from './data/pokemon/pokemonGalarData.json';
import PokemonHisuiData from './data/pokemon/pokemonHisuiData.json';
import PokemonHoennData from './data/pokemon/pokemonHoennData.json';
import PokemonJohtoData from './data/pokemon/pokemonJohtoData.json';
import PokemonKalosData from './data/pokemon/pokemonKalosData.json';
import PokemonKantoData from './data/pokemon/pokemonKantoData.json';
import PokemonPaldeaData from './data/pokemon/pokemonPaldeaData.json';
import PokemonSinnohData from './data/pokemon/pokemonSinnohData.json';
import PokemonUnovaData from './data/pokemon/pokemonUnovaData.json';
import React from 'react';
import SpecialSpecialTypeData from './data/SpecialSpecialTypeData.json';
import SpecialTypeData from './data/SpecialTypeData.json';
import TypeData from './data/TypeData.json';
import alola from './resources/types/alola.png';
import galar from './resources/types/galar.png';
import hisui from './resources/types/hisui.png';
import hoenn from './resources/types/hoenn.png';
import johto from './resources/types/johto.png';
import kalos from './resources/types/kalos.png';
import kanto from './resources/types/kanto.png';
import paldea from './resources/types/paldea.png';
import sinnoh from './resources/types/sinnoh.png';
import unova from './resources/types/unova.png';

export const TypeDetails = (props) => {

    let color = null;
    let contrastColor = null;
    let typeName = '';
    let typeEngName = '';
    let typeDescription = '';
    let typeExtraDescription = '';
    let typeListTitle = '';
    let typeNotification = '';

    let categoryIndex = null;
    let allow = null;

    for (var i = 0; i < TypeData.types.length; i++) {
        if (props.type === TypeData.types[i].engName) {
            color = TypeData.types[i].color;
            contrastColor = TypeData.types[i].contrastText;
            typeName = TypeData.types[i].name;
            typeEngName = TypeData.types[i].engName;
            typeDescription = TypeData.types[i].description;
            typeListTitle = TypeData.types[i].listTitle;
            typeNotification = TypeData.types[i].notification;
        }
    }

    if (color === null) {
        for (var j = 0; j < SpecialTypeData.types.length; j++) {
            if (props.type === SpecialTypeData.types[j].engName) {
                color = SpecialTypeData.types[j].color;
                contrastColor = SpecialTypeData.types[j].contrastText;
                typeName = SpecialTypeData.types[j].name;
                typeEngName = SpecialTypeData.types[j].engName;
                typeDescription = SpecialTypeData.types[j].description;
                typeExtraDescription = SpecialTypeData.types[j].extraDescription;
                typeListTitle = SpecialTypeData.types[j].listTitle;
                typeNotification = SpecialTypeData.types[j].notification;
                if (SpecialTypeData.types[j].allowedTypes.length > 0) allow = true;
                else if (SpecialTypeData.types[j].forbiddenTypes.length > 0) allow = false;
                else if (SpecialTypeData.types[j].forbiddenDualTypes.length > 0) allow = false;
            }
        }
    }

    if (color === null) {
        for (var k = 0; k < SpecialSpecialTypeData.types.length; k++) {
            if (props.type === SpecialSpecialTypeData.types[k].engName) {
                color = SpecialSpecialTypeData.types[k].color;
                contrastColor = SpecialSpecialTypeData.types[k].contrastText;
                typeName = SpecialSpecialTypeData.types[k].name;
                typeEngName = SpecialSpecialTypeData.types[k].engName;
                typeDescription = SpecialSpecialTypeData.types[k].description;
                typeListTitle = SpecialSpecialTypeData.types[k].listTitle;
                typeNotification = SpecialSpecialTypeData.types[k].notification;
            }
        }
    }

    const renderPokemon = (pokemon) => {

        let pokemonName = pokemon.name;
        let typeTrue = false;
        
        for (var i = 0; i < pokemon.categories.length; i++) {
            if (pokemon.categories[i] === 'muoto' && pokemon.engName !== undefined) pokemonName = pokemon.engName;
        }

        if (props.type !== 'baby' && props.type !== 'safari' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'shine' && props.type !== 'shadow' && props.type !== 'mega' && props.type !== 'shine' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax') {
            for (i = 0; i < pokemon.types.length; i++) {
                if (pokemon.engTypes[i] === props.type) typeTrue = true;
            }
        }
        else if (props.type === 'baby' || props.type === 'fossil') {
            for (i = 0; i < pokemon.engCategories.length; i++) {
                if (pokemon.engCategories[i] === props.type) {
                    typeTrue = true;
                    categoryIndex = i;
                }
            }
        }
        else if (props.type === 'safari') {
            for (i = 0; i < pokemon.engCategories.length; i++) {
                if (pokemon.engCategories[i] === 'regional') {
                    typeTrue = true;
                    categoryIndex = i;
                }
            }
        }
        else if (props.type === 'eevee') {
            for (i = 0; i < pokemon.engCategories.length; i++) {
                if (pokemon.engCategories[i] === 'eeveelution') {
                    typeTrue = true;
                    categoryIndex = i;
                }
            }
        }
        else if (props.type === 'shine') {
            if (pokemon.shinyExists === true) typeTrue = true;
        }
        else if (props.type === 'shadow') {
            if (pokemon.shadowExists === true) typeTrue = true;
        }
        else if (props.type === 'dynamax') {
            if (pokemon.dynamaxExists === true) typeTrue = true;
        }
        else if (props.type === 'gigantamax') {
            if (pokemon.gigantamaxExists === true) typeTrue = true;
        }
        else if (props.type === 'legendary' || props.type === 'mythical' || props.type === 'mega' || props.type === 'ultra beast') {
            for (i = 0; i < pokemon.engCategories.length; i++) {
                if (pokemon.engCategories[i] === props.type) typeTrue = true;
            }
        }

        if (pokemon.available === false || typeTrue === false) {
            return(<font />);
        }

        const pokemonNumber = (id) => {
            let newId = id.slice(0, 4);

            return (<font>#{newId}</font>);
        }

        return(
            <abbr title={pokemonName}>
                <Box borderRadius="50%" style={{ width: '70px', height: '70px', backgroundColor: color, display: 'inline-block' }}>
                    <center>
                        <font className="PokemonInfo" face="Segoe UI Semibold" color={contrastColor}>
                            {pokemonNumber(pokemon.id)}
                        </font>
                        <br />
                        {props.type !== 'gigantamax' && <img src={require('./data/pokemon/' + pokemon.region + '/' + pokemon.id + '.png')} alt={'./data/pokemon/' + pokemon.region + '/' + pokemon.id + '.png'} style={{ width: '40px', height: '40px' }} />}
                        {props.type === 'gigantamax' && <img src={require('./data/pokemon/' + pokemon.region + '/' + pokemon.id + '-gigantamax.png')} alt={'./data/pokemon/' + pokemon.region + '/' + pokemon.id + '.png'} style={{ width: '40px', height: '40px' }} />}
                    </center>
                </Box>
            </abbr>
        );
    }

    return(
        <div>
            <font className="Site-body-header" face="Segoe UI Semibold" color={color}>
                {props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'mega' && props.type !== 'dynamax' && props.type !== 'gigantamax' && 
                    <center>
                        <img src={require('./resources/types/' + typeEngName + '.png')} alt="icon" />
                        <br />
                        {typeName.toUpperCase() + ' / ' + typeEngName.toUpperCase()} 
                    </center>
                }
                {(props.type === 'eevee' || props.type === 'fossil') && 
                    <center>
                        {typeName.toUpperCase() + ' / ' + typeEngName.toUpperCase()} 
                    </center>
                }
                {(props.type === 'mega' || props.type === 'dynamax' || props.type === 'gigantamax') && 
                    <center>
                        <img src={require('./resources/types/' + typeEngName + '.png')} alt="icon" />
                        <br />
                        {typeName.toUpperCase()} 
                    </center>
                }
            </font>
            {props.city !== 'kingdom' && props.city !== 'tower' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax' && <br className="TutorialBr" />}
            {props.city !== 'kingdom' && props.city !== 'tower' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax' && 
                <table align="center">
                    <tbody>
                        <tr height="120px">
                                <td align="center" width="150px" min-width="150px" max-width="150px">
                                    <img src={require('./resources/badges/' + typeEngName + '.png')} className="Badge" alt="badge2019" />
                                </td>
                            </tr>
                    </tbody>
                </table>
            }
            {props.type !== 'baby' && props.type !== 'safari' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'shine' && props.type !== 'shadow' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'ultra beast' && props.type !== 'mega' && props.type !== 'dynamax' && props.type !== 'gigantamax' &&
                <center>
                    {props.city !== 'kingdom' && props.city !== 'tower' && typeDescription !== '' && 
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            <br />
                            {typeDescription}
                        </font>
                    }
                    <br /><br />
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={color}>
                        VAHVUUDET, HEIKKOUDET, VASTUSTUSKYVYT JA IMMUNITEETIT
                    </font>
                    <br /><br />
                    {props.type !== 'dragon' && props.type !== 'normal' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Vahva näitä tyyppejä tyyppiä vastaan:
                        </font>
                    }
                    {props.type === 'dragon' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Vahva tätä tyyppiä vastaan:
                        </font>
                    }
                    {props.type === 'normal' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Ei vahvuuksia muita tyyppejä vastaan!
                        </font>
                    }
                    <TypeStrengthCheck subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />
                    <br />
                    {props.type !== 'electric' && props.type !== 'normal' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Heikko näille tyypeille:
                        </font>
                    }
                    {(props.type === 'electric' || props.type === 'normal') &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Heikko tälle tyypille:
                        </font>
                    }
                    <TypeWeaknessCheck subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />
                    <br />
                    {props.type !== 'ice' && props.type !== 'normal' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Vastustuskykyinen näille tyypeille:
                        </font>
                    }
                    {props.type === 'ice' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Vastustuskykyinen tälle tyypille:
                        </font>
                    }
                    {props.type === 'normal' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Ei vastustuskykyä muille tyypeille!
                        </font>
                    }
                    <TypeResistanceCheck subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />
                    <br />
                    {props.type === 'ghost' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Immuniteetti (eli siis tavallista vahvempi vastustuskyky) näille tyypeille:
                        </font>
                    }
                    {(props.type === 'dark' || props.type === 'fairy' || props.type === 'flying' || props.type === 'ground' || props.type === 'normal' || props.type === 'steel') &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Immuniteetti (eli siis tavallista vahvempi vastustuskyky) tälle tyypeille:
                        </font>
                    }
                    {props.type !== 'ghost' && props.type !== 'dark' && props.type !== 'fairy' && props.type !== 'flying' && props.type !== 'ground' && props.type !== 'normal' && props.type !== 'steel' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Ei immuniteettiä (eli siis tavallista vahvempaa vastustuskykyä) muille tyypeille!
                        </font>
                    }
                    <TypeImmunityCheck subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />
                </center>
            }
            {(props.type === 'baby' || props.type === 'safari' || props.type === 'eevee' || props.type === 'fossil') && 
                <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                    <br />
                    <center>
                        {typeDescription}
                        {allow !== null && <CertainTypesCheck allow={allow} categoryIndex={categoryIndex} subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />}
                    </center>
                    {typeExtraDescription !== '' && <center>
                        <br />
                        {typeExtraDescription}
                        {allow !== null && <CertainDualTypesCheck allow={allow} categoryIndex={categoryIndex} color={color} subtab={props.subtab} setSubtab={props.setSubtab} type={props.type} />}
                    </center>}
                </font>
            }
            {(props.type === 'shine' || props.type === 'shadow' || props.type === 'mega' || props.type === 'legendary' || props.type === 'mythical' || props.type === 'dynamax' || props.type === 'gigantamax') && 
                <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                    <br />
                    <center>
                        {typeDescription}
                    </center>
                </font>
            }
            {props.city !== 'kingdom' && props.city !== 'tower' && typeListTitle !== '' &&
                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={color}>
                    <br />
                    <center>
                        {typeListTitle}
                    </center>
                </font>
            }
            {(props.city === 'kingdom' || props.city === 'tower') && 
                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={color}>
                    <br />
                    <center>
                        TÄMÄN TYYPIN POKÉMONIT
                    </center>
                </font>
            }
            {props.city !== 'kingdom' &&
                <Container>
                    <br />
                    <Typography style={{ backgroundColor: '#FFFFFF' }}>
                    <center>
                        {typeNotification !== '' && <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            {typeNotification}
                            <br /><br />
                        </font>}
                        <font className="Site-body-text" face="Segoe UI Semibold" color={color}>
                            Pidä hiirtä pokémonin kuvan kohdalla nähdäksesi pokémonin nimi.
                            <br />
                        </font>
                        {props.type !== 'dark' && props.type !== 'baby' && props.type !== 'ultra beast' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            KANTO <img src={kanto} className="TypeIcon" alt="kanto-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonKantoData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'fossil' && props.type !== 'ultra beast' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            JOHTO <img src={johto} className="TypeIcon" alt="johto-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonJohtoData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'eevee' && props.type !== 'ultra beast' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            HOENN <img src={hoenn} className="TypeIcon" alt="hoenn-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonHoennData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            SINNOH <img src={sinnoh} className="TypeIcon" alt="sinnoh-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonSinnohData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'baby' && props.type !== 'eevee' && props.type !== 'shadow' && props.type !== 'mega' && props.type !== 'ultra beast' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            UNOVA <img src={unova} className="TypeIcon" alt="unova-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonUnovaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'baby' && props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            KALOS <img src={kalos} className="TypeIcon" alt="kalos-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonKalosData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}     
                        {props.type !== 'baby' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'shadow' && props.type !== 'mega' && props.type !== 'dynamax' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            ALOLA <img src={alola} className="TypeIcon" alt="alola-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonAlolaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'bug' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'shadow' && props.type !== 'mega' && props.type !== 'ultra beast' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            GALAR <img src={galar} className="TypeIcon" alt="galar-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonGalarData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'baby' && props.type !== 'safari' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'mega' && props.type !== 'ultra beast' && props.type !== 'dynamax' && props.type !== 'gigantamax' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            HISUI <img src={hisui} className="TypeIcon" alt="hisui-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonHisuiData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                        {props.type !== 'rock' && props.type !== 'baby' && props.type !== 'safari' && props.type !== 'eevee' && props.type !== 'fossil' && props.type !== 'shadow' && props.type !== 'legendary' && props.type !== 'mythical' && props.type !== 'mega' && props.type !== 'dynamax' && props.type !== 'gigantamax' && props.type !== 'ultra beast' && <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            PALDEA <img src={paldea} className="TypeIcon" alt="paldea-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonPaldeaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>}
                    </center>
                    </Typography>
                </Container>
            }
        </div>
    );
}
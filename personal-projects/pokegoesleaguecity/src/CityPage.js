import './CityAndTower.css';

import { AppBar, Box, Button, ButtonGroup, Container, Paper, Toolbar, Typography } from '@mui/material';
import React, { useState } from 'react';

import CityMain from './CityMain';
import CityPlayerStats from './CityPlayerStats';
import { CitySeasons } from './CitySeasons';
import PVPguide from './PVPguide';
import SeasonsData from './data/SeasonsData.json';
import { ThemeProvider } from "@mui/material/styles";
import { mainTheme } from './CityThemes';

// Tämä sivu on suunniteltu aukeamaan vain Helsingin, Kuopion, Tampereen ja Turun omille sivuille, koska vain niiden taustakuvat ja logot ovat saatu suunniteltua.

export const CityPage = (props) => {
    const [tab, setTab] = useState('mainTab');

    const styles = {
        paperContainer: {
            backgroundImage: `url(${require('./resources/backgrounds/' + props.city + '.png')})`,
        }
    };

    let currentDate = new Date();
    let badgeStartDate;
    let badgeEndDate;
    let cupStartDate;
    let cupEndDate;
    const [year] = useState(currentDate.getFullYear());
    let scrollText = '';
    let scrollTextColor = props.theme.palette.primary.main;

    if (props.city === 'tampere') {
        let seasonExists = false;
        let nextSeasonExists = false;
        for (var i = 0; i < SeasonsData.seasons.tampere.length; i++) {
            if (SeasonsData.seasons.tampere[i].season === year) {
                seasonExists = true;
                badgeStartDate = new Date(SeasonsData.seasons.tampere[i].badgeStartDate);
                badgeEndDate = new Date(SeasonsData.seasons.tampere[i].badgeEndDate);
                cupStartDate = new Date(SeasonsData.seasons.tampere[i].cupStartDate);
                cupEndDate = new Date(SeasonsData.seasons.tampere[i].cupEndDate);
            }
            else if (SeasonsData.seasons.tampere[i].season === (year + 1)) {
                nextSeasonExists = true;
            }
        }

        if (seasonExists) {
            if (currentDate.getTime() < badgeStartDate.getTime()) {
                scrollText = 'KAUDEN ' + year + ' THE BADGE CHALLENGE ALKAA ' + badgeStartDate.getDate() + '.' + (badgeStartDate.getMonth() + 1) + '.' + badgeStartDate.getFullYear() + '!';
            }
            else if (currentDate.getTime() >= badgeStartDate.getTime() && currentDate.getTime() < badgeEndDate.getTime()) {
                scrollText = 'KAUDEN ' + year + ' THE BADGE CHALLENGE ON NYT KÄYNNISSÄ JA KESTÄÄ ' + badgeEndDate.getDate() + '.' + (badgeEndDate.getMonth() + 1) + '.' + badgeEndDate.getFullYear() + ' ASTI!';
            }
            else if (currentDate.getTime() >= badgeEndDate.getTime() && currentDate.getTime() < cupStartDate.getTime()) {
                scrollText = 'KAUDEN ' + year + ' THE CHAMPION CUP ALKAA ' + cupStartDate.getDate() + '.' + (cupStartDate.getMonth() + 1) + '.' + cupStartDate.getFullYear() + '!';
            }
            else if (currentDate.getTime() >= badgeEndDate.getTime() && currentDate.getTime() >= cupStartDate.getTime()) {
                scrollText = 'KAUDEN ' + year + ' THE CHAMPION CUP ON NYT KÄYNNISSÄ JA KESTÄÄ ' + cupEndDate.getDate() + '.' + (cupEndDate.getMonth() + 1) + '.' + cupEndDate.getFullYear() + ' ASTI!';
            }
            else if (currentDate.getTime() >= cupEndDate.getTime()) {
                scrollText = 'KAUSI ' + year + ' ON NYT PÄÄTTYNYT! ONNITTELUT SEUTUKUNTAMME UUDELLE LIIGAMESTARILLE!';
            }
        }
        else {
            if (nextSeasonExists) {
                scrollText = 'KAUSI ' + parseInt(year + 1) + ' TULOSSA PIAN!';
            }
            else {
                scrollText = 'EI TURNAUKSIA KÄYNNISSÄ TÄLLÄ HETKELLÄ!';
            }
        }
    }
    else {
        scrollText = 'PAHOITTELEMME, MUTTA TÄLLÄ SEUTUKUNNALLA EI OLE VIELÄ OMAA TURNAUSTA!';
    }

    if (props.city !== 'lahti') scrollTextColor = props.theme.palette.primary.contrastText;
    else scrollTextColor = props.theme.palette.secondary.contrastText;

    return(
        <div style={{ backgroundColor: props.theme.palette.primary.main, color: props.theme.palette.primary.contrastText, }}>
            <Paper style={styles.paperContainer} square>
                <ThemeProvider theme={{...props.theme}}>
                    <AppBar>
                        <Toolbar className="AppBar">
                            <ButtonGroup color="inherit" variant="text">
                                <Button className="NavButton" color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('mainTab')}}>
                                    ETUSIVU
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('season')}}>
                                    TURNAUS
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('PVPguide')}}>
                                    PVP-OPAS
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('playerStats')}}>
                                    PELAAJAT
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); props.setCity('main'); props.setFixedCity('Main'); props.setPrevCity('main'); props.setTheme(mainTheme)}}>
                                    SEUTUVALIKKO
                                </Button>
                            </ButtonGroup>
                        </Toolbar>
                    </AppBar>
                </ThemeProvider>
                <Container>
                    <header className="Site-header whiteHeaderContainer">
                        <br className="LogoUpBr" />
                        <img src={require('./resources/logos/' + props.city + '.png')} className="Site-logo" alt="Site-logo" />
                    </header>
                </Container>
                {year > 2020 &&
                    <Container>
                        <Typography className="whiteMidContainer" style={{ backgroundColor: '#FFFFFF' }}>
                            <br />
                            <Box className="ScrollFrame">
                                <Box className="ScrollContainer">
                                    <Box className="ScrollText">
                                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color={scrollTextColor}>
                                            {scrollText}
                                        </font>
                                    </Box>
                                </Box>
                            </Box>
                        </Typography>
                    </Container>
                }
                {tab === 'mainTab' && <CityMain city={props.city} fixedCity={props.fixedCity} tab={tab} setTab={setTab} theme={props.theme} />}
                {tab === 'season' && <CitySeasons city={props.city} fixedCity={props.fixedCity} theme={props.theme} />}
                {tab === 'PVPguide' && <PVPguide city={props.city} theme={props.theme} />}
                {tab === 'playerStats' && <CityPlayerStats city={props.city} fixedCity={props.fixedCity} theme={props.theme} />}
            </Paper>
        </div>
    );
}
export default CityPage;
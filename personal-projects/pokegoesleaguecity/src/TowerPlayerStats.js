import './CityAndTower.css';

import { Box, Container, Link, Typography } from '@mui/material';
import React, { useState } from 'react';
import { baronTheme, commonerTheme, dukeTheme, earlTheme, grandDukeTheme, marquisTheme, viscountTheme } from './TowerRankThemes';

import PlayerProfile from './PlayerProfile';
import PlayersData from './data/PlayersData.json';
import baronUniform from './resources/tower-ranks/1_Baron.png';
import battleKey from './resources/others/battle-key-light.png';
import commonerUniform from './resources/tower-ranks/0_Commoner.png';
import dukeUniform from './resources/tower-ranks/5_Duke.png';
import earlUniform from './resources/tower-ranks/3_Earl.png';
import grandDukeUniform from './resources/tower-ranks/6_GrandDuke.png';
import instinctEmblem from './resources/teams/instinct.png';
import marquisUniform from './resources/tower-ranks/4_Marquis.png';
import mysticEmblem from './resources/teams/mystic.png';
import valorEmblem from './resources/teams/valor.png';
import viscountUniform from './resources/tower-ranks/2_Viscount.png';

export const TowerPlayerStats = (props) => {
    const [subtab, setSubtab] = useState('mainSubtab');
    const [player, setPlayer] = useState(null);
    const [gender, setGender] = useState(null);
    const [playerTeam, setPlayerTeam] = useState(null);
    const [playerLVL, setPlayerLVL] = useState(null);
    const [playerDescription, setPlayerDescription] = useState(null);
    const [seasons] = useState(null);
    const [tower, setTower] = useState(null);

    const CheckRank = (props) => {
        if (props.tower.current.rank === 6) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={grandDukeTheme.palette.primary.main}>
                        ARVO: <abbr title='Tornisuurherttuatar'><img src={grandDukeUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={grandDukeTheme.palette.primary.main}>
                    ARVO: <abbr title='Tornisuurherttua'><img src={grandDukeUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        else if (props.tower.current.rank === 5) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={dukeTheme.palette.primary.main}>
                        ARVO: <abbr title='Torniherttuatar'><img src={dukeUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={dukeTheme.palette.primary.main}>
                    ARVO: <abbr title='Torniherttua'><img src={dukeUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        else if (props.tower.current.rank === 4) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={marquisTheme.palette.primary.main}>
                        ARVO: <abbr title='Tornimarkiisitar'><img src={marquisUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={marquisTheme.palette.primary.main}>
                    ARVO: <abbr title='Tornimarkiisi'><img src={marquisUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        else if (props.tower.current.rank === 3) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={earlTheme.palette.primary.main}>
                        ARVO: <abbr title='Tornikreivitär'><img src={earlUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={earlTheme.palette.primary.main}>
                    ARVO: <abbr title='Tornijaarli'><img src={earlUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        else if (props.tower.current.rank === 2) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={viscountTheme.palette.primary.main}>
                        ARVO: <abbr title='Tornivarakreivitär'><img src={viscountUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={viscountTheme.palette.primary.main}>
                    ARVO: <abbr title='Tornivarakreivi'><img src={viscountUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        else if (props.tower.current.rank === 1) {
            if (props.gender === 'Nainen') {
                return (
                    <font color={baronTheme.palette.primary.main}>
                        ARVO: <abbr title='Torniparonitar'><img src={baronUniform} className="TowerRank" alt="tower-rank" /></abbr>
                    </font>
                );
            }
            return (
                <font color={baronTheme.palette.primary.main}>
                    ARVO: <abbr title='Torniparoni'><img src={baronUniform} className="TowerRank" alt="tower-rank" /></abbr>
                </font>
            );
        }
        return (
            <font color={commonerTheme.palette.primary.main}>
                ARVO: <abbr title='Torniaateliton'><img src={commonerUniform} className="TowerRank" alt="tower-rank" /></abbr>
            </font>
        );
    }

    const renderPlayer = (player) => {

        let textColor;
        let emblem;

        if (player.team === 'Instinct') {
            textColor = '#FFFF00';
            emblem = instinctEmblem;
        }
        else if (player.team === 'Mystic') {
                textColor = '#4040FF';
                emblem = mysticEmblem;
        }
        else if (player.team === 'Valor') {
                textColor = '#FF0000';
                emblem = valorEmblem;
        }
        else {
            textColor = '#A9A9A9';
            emblem = null;
        }

        return(
            <div>
                <Box borderRadius={4} p={0.5} style={{ backgroundColor: '#9F9F9F' }}>
                    <Box borderRadius={3.25} p={1} style={{ backgroundColor: '#000000', color: textColor }}>
                        <font className="PlayerInfo" face="Segoe UI Semibold">
                            {player.username}: <img src={emblem} className="TeamEmblem" alt="team-emblem" /> ({player.team}), LVL {player.lvl},{' '}
                        </font>
                        <font className="PlayerInfo" face="Segoe UI Semibold" color={textColor}>
                            <CheckRank gender={player.gender} tower={player.tower} />,{' '}
                        </font>
                        <font className="PlayerInfo" face="Segoe UI Semibold" color='#B5E61D'>
                            {player.tower.current.points} BP
                        </font>
                        <font className="PlayerInfo" face="Segoe UI Semibold" color={textColor}>
                            ,{' '}
                        </font>
                        <font className="PlayerInfo" face="Segoe UI Semibold" color='#FFC90E'>
                            {player.tower.current.battleKeys} x <img src={battleKey} className="KeyIcon" alt="battle-key" />
                        </font>
                        <br />
                        <Link component="button" onClick={() => {window.scrollTo(0, 0); setPlayer(player.username); setGender(player.gender); setPlayerTeam(player.team); setPlayerLVL(player.lvl); setPlayerDescription(player.description); setSubtab('player'); setTower(player.tower)}}>
                            <font color={textColor} className="PlayerProfileLink" face="Segoe UI Semibold">{'≫'} näytä profiili {'≫'}</font>
                        </Link>
                    </Box>
                </Box>
                <br className="PlayerStatBr" />
            </div>
        );
    }

    return(
        <Container>
            {subtab === 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <font className="Site-body-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        PELAAJAT
                    </font>
                    <br /><br />
                    {PlayersData.users.map((player) => renderPlayer(player))}
                </Typography>
            }
            {subtab !== 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <Link className="LinkLeft" component="button" onClick={() => {window.scrollTo(0, 0); setPlayer(null); setPlayerTeam(null); setPlayerLVL(null); setPlayerDescription(null); setSubtab('mainSubtab')}}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                                {'≪'} Palaa pelaajaluetteloon
                            </center>
                        </font>
                    </Link>
                    {props.yourUsername === player && <Link className="LinkRight" component="button" onClick={() => {window.scrollTo(0, 0)}}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                            Muokkaa profiilia {'≫'}
                            </center>
                        </font>
                    </Link>}
                    <br /><br />
                    <PlayerProfile player={player} yourUsername={props.username} city={props.city} seasons={seasons} gender={gender} team={playerTeam} theme={props.theme} tower={tower} lvl={playerLVL} description={playerDescription} />
                </Typography>
            }
        </Container>
    );
}
export default TowerPlayerStats;
/*
Tästä on tarkoitus tulla keksimäni Pokémon GO:n pelaamiseen suunnatun
turnausformaatin viralliset nettisivut, kun saan ne valmiiksi.
Sitä odotellessa tätä turnausformaattia on käyty ainoastaan Tampereella
Facebook-ryhmän ja Telegram-ryhmien avustuksella.

Huom. Useimmat kuvatiedostot, joita käytän eivät ole mukana repositoryssa.
*/

import './index.css';

import * as serviceWorker from './serviceWorker';

import App from './App';
import React from 'react';
import { createRoot } from 'react-dom/client';

const root = createRoot(document.getElementById('root'));

root.render(<App />);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

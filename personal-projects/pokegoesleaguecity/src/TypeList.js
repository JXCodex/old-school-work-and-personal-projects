import { Button } from '@mui/material';
import React from 'react';
import TypeData from './data/TypeData.json';

export const TypeList = (props) => {

    let rowsOfSix = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        row.push(TypeData.types[i]);
        if (row.length === 11) {
            rowsOfSix.push(row);
            row = [];
        }
        else {
            if (i < (TypeData.types.length - 1)) row.push('space');
            else row = [];
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        return(
            <tr height="90px">
                {iconTableRow.map((icon) => renderIcon(icon))}
            </tr>
        );
    }

    const renderIcon = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(icon.engName)}}>
                    <img src={require('./resources/types/' + icon.engName + '.png')} className="TypeIcon" alt="typeicon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={icon.color}>
                    {icon.name.toUpperCase()}<br />{icon.engName.toUpperCase()}
                </font>
            </td>
        );
    }

    return(
        <table align="center">
            <tbody>
                {rowsOfSix.map((badgeTableRow) => renderIconTableRow(badgeTableRow))}
            </tbody>
        </table>
    );
}
import './CityAndTower.css';

import { Container, Link, Typography } from '@mui/material';

import React from 'react';

export const CityMain = (props) => {

    return(
        <Container>
            <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF', color: props.theme.palette.primary.main, fontFamily: "Segoe UI Semibold" }}>
                {props.city === 'helsinki' && <font className="Site-body-header">
                    Tervetuloa Helsingin League Metropoliksen nettisivuille
                </font>}
                {(props.city === 'joensuu' || props.city === 'jyvaskyla' || props.city === 'kuopio' || props.city === 'pori') && <font className="Site-body-header">
                Tervetuloa {props.fixedCity}n League Cityn nettisivuille!
                </font>}
                {props.city === 'lahti' && <font className="Site-body-header">
                    Tervetuloa Lahden League Cityn nettisivuille!
                </font>}
                {props.city === 'tampere' && <font className="Site-body-header">
                    Tervetuloa Tampereen League Cityn nettisivuille!
                </font>}
                {props.city === 'turku' && <font className="Site-body-header">
                    Tervetuloa Turun League Cityn nettisivuille!
                </font>}
                <br /><br />
                {props.city === 'tampere' && 
                    <font className="Site-body-text">
                        Tampere on kaupunki, jossa League City -formaatti perustettiin. Koko League City sai alkunsa toukokuussa 2019, kun Juuso Hauvala päätti suunnitella Tampereelle Pokémon GO:n oman PVP-pelaamiselle (player versus player eli pelaaja vastaan pelaaja) suunnatun turnauksen. Turnaus aloitti alun perin Facebook-ryhmän ja Telegram-ryhmien sekä Google Drive -ohjedokumenttien avulla ylläpidettynä ja Google Sheets -taulukoiden avulla tilastoseurattuna yhteisöpohjaisena kilpailuna, kunnes näitä nettisivuja alettiin toteuttaa sille maaliskuussa 2020.
                    </font>
                }
                {props.city !== 'tampere' && 
                    <font className="Site-body-text">
                        Koko League City sai alkunsa toukokuussa 2019, kun Juuso Hauvala päätti suunnitella oman turnauksen Pokémon GO:n PVP-pelaamiseen (player versus player eli pelaaja vastaan pelaaja). Turnaus aloitti alun perin Facebook-ryhmän ja Telegram-ryhmien sekä Google Drive -ohjedokumenttien avulla ylläpidettynä ja Google Sheets -taulukoiden avulla tilastoseurattuna yhteisöpohjaisena kilpailuna, kunnes näitä nettisivuja alettiin toteuttaa sille maaliskuussa 2020. Toistaiseksi tässä kaupungissa tai muuallakaan Tampereen seutukunnan ulkopuolella ei vielä järjestetä League City -turnauksia.
                    </font>
                }
                <br /><br />
                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    TURNAUS
                </font>
                <br /><br />
                {props.city === 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    Jokainen League Metropolis -turnauskausi
                </font>}
                {props.city !== 'helsinki' && <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    Jokainen League City -turnauskausi
                </font>}
                <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    {' '}koostuu kahdesta vaiheesta: haasteesta nimeltä The Badge Challenge sekä lopputurnauksesta nimeltä The Champion Cup, johon saavat kutsun The Badge Challengen suorittaneet pelaajat.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('season')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Lue turnauksen säännöt ja aikataulut täältä {'≫'}
                    </font>
                </Link>
                <br /><br />
                <font className="Site-body-mid-header">
                    PVP-OPAS
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Opas tässä turnausformaattissa tai PVP-otteluissa yleensä pärjäämiseen.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('PVPguide')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Lue täältä {'≫'}
                    </font>
                </Link>
                <br /><br />
                <font className="Site-body-mid-header">
                    KILPAILIJAT
                </font>
                <br /><br />
                <font className="Site-body-text">
                    Luettelo turnausformaattiin osallistuneista kilpailijoista ja heidän profiilinsa.
                </font>
                <br /><br />
                <Link className="Site-body-link" component="button" onClick={() => {window.scrollTo(0, 0); props.setTab('playerStats')}}>
                    <font color={props.theme.palette.primary.dark}>
                        Katso täältä {'≫'}
                    </font>
                </Link>
            </Typography>
        </Container>
    );
}
export default CityMain;
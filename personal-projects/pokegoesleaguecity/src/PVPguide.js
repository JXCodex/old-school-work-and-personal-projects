import './CityAndTower.css';

import { Container, Link, Typography } from '@mui/material';
import React, { useState } from 'react';

import { TypeDetails } from './TypeDetails';
import { TypeList } from './TypeList';

export const PVPguide = (props) => {
    const [subtab, setSubtab] = useState('mainSubtab');
    
    return(
        <Container>
            {subtab === 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <font className="Site-body-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        PVP-OPAS
                    </font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        Tässä on kokeneiden pelaajien kommentteihin ja komiteamme henkilökohtaisiin kokemuksiin perustuvia ohjeita niille, jotka tarvitsevat hieman neuvoja Pokémon GO:n pelaamiseen PVP-muodossa (player versus player eli pelaaja vastaan pelaaja).
                    </font>
                    <br /><br />
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        TYYPIT
                    </font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        Ensisijainen asia, mikä pokémoneista on hyvä tietää, on niiden tyypit. Pelissä on 18 eri tyyppiä. Klikkaa jotain näistä kuvakkeista saadaksesi lisätietoa:
                        <br /><br />
                        <TypeList subtab={subtab} setSubtab={setSubtab} />
                    </font>
                    <br /><br />
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        TYYPPIEN OMINAISUUDET
                    </font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        Pokémonin oma tyyppi/tyypit määrittää minkä tyypin liikkeille se on heikko ja mitä vastaan vastustuskykyinen. Joillakin pokémoneilla yhden tyypin sijasta kaksi tyyppiä ja jotkut pokémonit voivat oppia liikkeitä, jotka eroavat tyypiltään huomattavasti itse niitä käyttävästä pokémonista. Pokémonien ja liikkeiden tyypillä on seuraava vaikutus tyyppitehoon:
                        <ul>
                            <li>Jos pokémon <font face="Segoe UI Black">käyttää oman tyyppinsä liikettä</font>, se tekee vastustajan pokémonille <font face="Segoe UI Black">120%</font> siitä vahingosta kuin mitä se toisen tyypin pokémonin käyttämänä tekisi. Tästä ominaisuudesta käytetään termiä <font face="Segoe UI Black">STAB (Same-Type Attack Bonus)</font>. Esimerkkitilanteita:</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#2E9D37'} face="Segoe UI Semibold">ruohotyypin pokémon <font face="Segoe UI Black">Venusaur</font> käyttää ruohotyypin liikettä <font face="Segoe UI Black">Frenzy Plant</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Semibold">tulityypin pokémon <font face="Segoe UI Black">Charizard</font> käyttää tulityypin liikettä <font face="Segoe UI Black">Blast Burn</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#5BAFDD'} face="Segoe UI Semibold">vesityypin pokémon <font face="Segoe UI Black">Blastoise</font> käyttää vesityypin liikettä <font face="Segoe UI Black">Hydro Cannon</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#EDD60A'} face="Segoe UI Semibold">sähkötyypin pokémon <font face="Segoe UI Black">Raichu</font> käyttää sähkötyypin liikettä <font face="Segoe UI Black">Wild Charge</font>.</font></li>
                            </ul>
                            <br />
                            
                            <li>Jos pokémon on <font color={'#228B22'} face="Segoe UI Black">heikko</font> jonkin tietyn tyypin hyökkäykselle, se kärsii <font color={'#228B22'} face="Segoe UI Black">160% (super effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä. Esimerkkitilanteita:</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#5BAFDD'} face="Segoe UI Black">vesityypin liike </font> tekee vahinkoa <font color={'#CEC067'} face="Segoe UI Black">pelkkää kivityyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                                <li>Jos esimerkiksi <font color={'#EDD60A'} face="Segoe UI Black">sähkötyypin liike </font> tekee vahinkoa <font color={'#5BAFDD'} face="Segoe UI Black">pelkkää vesityyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Black">tulityypin liike </font> tekee vahinkoa <font color={'#2E9D37'} face="Segoe UI Black">pelkkää ruohotyyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                            </ul>
                            <br />
                            
                            <li>Jos <font color={'#228B22'} face="Segoe UI Black">kaksityyppisen pokémonin tyypeistä molemmat ovat heikkoja</font> tietyn tyypin hyökkäykselle, se kärsii <font color={'#228B22'} face="Segoe UI Black">256% (super effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#2E9D37'} face="Segoe UI Black">ruohotyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#5BAFDD'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">vesityyppiä</font></font> <font color={'#CC652A'} face="Segoe UI Semibold"> että <font face="Segoe UI Black"> maatyyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Black">tulityypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#85C900'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">ötökkätyyppiä</font></font> <font color={'#2C989A'} face="Segoe UI Semibold"> että <font face="Segoe UI Black"> terästyyppiä</font>.</font></li>
                            </ul>
                            <br />
                            
                            <li>Jos pokémonilla on <font color={'#B22222'} face="Segoe UI Black">vastustuskyky</font> jonkin tietyn tyypin hyökkäykselle, se kärsii <font color={'#B22222'} face="Segoe UI Black">62,5% (not very effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#5BAFDD'} face="Segoe UI Black">vesityypin liike </font> tekee vahinkoa <font color={'#2E9D37'} face="Segoe UI Black">pelkkää ruohotyyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                                <li>Jos esimerkiksi <font color={'#2E9D37'} face="Segoe UI Black">ruohotyypin liike </font> tekee vahinkoa <font color={'#6C85D5'} face="Segoe UI Black">pelkkää lentotyyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Black">tulityypin liike </font> tekee vahinkoa <font color={'#CEC067'} face="Segoe UI Black">pelkkää kivityyppiä<font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                            </ul>
                            <br />
                            
                            <li>Jos <font color={'#B22222'} face="Segoe UI Black">kaksityyppisen pokémonin tyypeistä molemmilla on vastustuskyky</font> tietyn tyypin hyökkäykselle tai jos <font color={'#B22222'} face="Segoe UI Black">yksityyppisellä pokémonilla on immunitetti (tässä tapauksessa tavallista vahvempi vastustuskyky)</font> jonkin tietyn tyypin hyökkäykselle, se kärsii <font color={'#B22222'} face="Segoe UI Black">39,1% (not very effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Black">tulityypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#5BAFDD'} face="Segoe UI Semibold"> sekä <font face="Segoe UI Black">vesityyppiä</font></font><font color={'#CEC067'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">kivityyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#2E9D37'} face="Segoe UI Black">ruohotyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#85C900'} face="Segoe UI Semibold"> sekä <font face="Segoe UI Black">ötökkätyyppiä</font></font><font color={'#D852C5'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">myrkkytyyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#EDD60A'} face="Segoe UI Black">sähkötyypin liike </font> tekee vahinkoa <font color={'#CC652A'} face="Segoe UI Black">pelkkää maatyyppiä <font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                                <li>Jos esimerkiksi <font color={'#A38DDF'} face="Segoe UI Black">kummitustyypin liike </font> tekee vahinkoa <font color={'#92938E'} face="Segoe UI Black">pelkkää normaalityyppiä <font face="Segoe UI Semibold"> olevaan pokémoniin.</font></font></li>
                            </ul>
                            <br />
                            
                            <li>Jos <font color={'#B22222'} face="Segoe UI Black">kaksityyppisen pokémonin tyypeistä toisella on vastustuskyky ja toisella immuniteetti</font> jonkin tietyn tyypin hyökkäykselle, se kärsii <font color={'#B22222'} face="Segoe UI Black">24,4% (not very effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#A38DDF'} face="Segoe UI Black">kummitustyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#92938E'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">normaalityyppiä</font></font><font color={'#3F3F3F'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">pimeystyyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#EDD60A'} face="Segoe UI Black">sähkötyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#2E9D37'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">ruohotyyppiä</font></font><font color={'#CC652A'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">maatyyppiä</font>.</font></li>
                            </ul>
                            <br />

                            <li>Jos <font color={'#228B22'} face="Segoe UI Black">kaksityyppisellä pokémonilla toinen sen tyypeistä on heikko</font> jollekin tyypille <font color={'#B22222'} face="Segoe UI Black">ja toinen tyyppi puolestaan vastustuskykyinen</font>. Pokémon kärsii sen tyypin liikkeistä <font face="Segoe UI Black">neutraalia vahinkoa eli 100%</font>.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#FE9301'} face="Segoe UI Black">tulityypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#5BAFDD'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">vesityyppiä</font></font><font color={'#2E9D37'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">ruohotyyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#5BAFDD'} face="Segoe UI Black">vesityypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#2E9D37'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">ruohotyyppiä</font></font><font color={'#CC652A'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">maatyyppiä</font>.</font></li>
                            </ul>
                            <br />
                            
                            <li>Jos <font color={'#228B22'} face="Segoe UI Black">kaksityyppisellä pokémonilla toinen sen tyypeistä on heikko</font> jollekin tyypille <font color={'#B22222'} face="Segoe UI Black">ja toinen tyyppi puolestaan immuuni</font>. Pokémon kärsii <font color={'#B22222'} face="Segoe UI Black">62,5% (not very effective)</font> siitä vahingosta kuin mitä neutraalia vahinkoa tekevästä liikkeestä.</li>
                            <ul>
                                <li>Jos esimerkiksi <font color={'#EDD60A'} face="Segoe UI Black">sähkötyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#5BAFDD'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">vesityyppiä</font></font><font color={'#CC652A'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">maatyyppiä</font>.</font></li>
                                <li>Jos esimerkiksi <font color={'#A38DDF'} face="Segoe UI Black">kummitustyypin liike </font> tekee vahinkoa pokémoniin, joka on <font color={'#92938E'} face="Segoe UI Semibold">sekä <font face="Segoe UI Black">normaalityyppiä</font></font><font color={'#FF564E'} face="Segoe UI Semibold"> että <font face="Segoe UI Black">psyykkistyyppiä</font>.</font></li>
                            </ul>
                        </ul>
                    </font>
                    <br />
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        TAISTELUVOIMA (CP) JA YKSILÖLLINEN ARVO (IV)
                    </font>
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        Jokaisella pokémonilla on taisteluvoima (CP eli Combat Power) sekä oma yksilöllinen arvo (IV eli individual value). CP-arvosta riippuu mihin kolmesta League-vaihtoehdosta pokémon PVP:ssä soveltuu. Great League sallii pokémonit, joiden CP on enintään 1500. Ultra League sallii pokémonit, joiden CP on enintään 2500. Master League sallii kaikenlaiset pokémonit.{' '}
                        {props.city !== 'helsinki' && props.city !== 'kingdom' && props.city !== 'tower' && props.city !== 'doublestar' &&
                            <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                                League Cityssä kaikki ottelut käydään Great League -muodossa, joten muilla ottelumuodoilla ei ole siellä väliä.{' '}
                            </font>
                        }
                        {props.city === 'helsinki' && 
                            <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                                League Metropoliksessa kaikki ottelut käydään Great League -muodossa, joten muilla ottelumuodoilla ei ole siellä väliä.{' '}
                            </font>
                        }
                        {props.city === 'kingdom' &&
                            <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                                League Kingdomissa kaikki ottelut käydään Great League -muodossa, joten muilla ottelumuodoilla ei ole siellä väliä.{' '}
                            </font>
                        }
                        {props.city === 'tower' &&
                            <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                                Battle Chateau Towerissa otteluita voi käydä joko Great League- tai Ultra League -muodossa, joten Master Leaguella ei ole siellä väliä.{' '}
                            </font>
                        }
                        {props.city === 'doublestar' &&
                            <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                                Double Star Tournamentissa kaikki ottelut käydään Great League -muodossa, joten muilla ottelumuodoilla ei ole siellä väliä.{' '}
                            </font>
                        }
                        IV-arvo määräytyy pokémonin hyökkäyksen, puolustuksen ja kestävyyden mukaan. Nämä kolme ominaisuutta voi tarkistaa pokémonilta Appraise-toiminnon kautta. Pokémonin yksilöllinen arvo on 100%, jos sekä hyökkäys, puolustus että kestävyys ovat maksimissa eli 15.
                        <br /><br />
                        PVE-otteluissa (player versus environment eli pelaaja vastaan ympäristö), joihin kuuluvat ottelut toisten pelaajien saleille jättämiä pokémoneja vastaan, raidiottelut, tai ottelut pokéstopeilla ja ilmapalloissa olevia GO-Rakettiryhmän jäseniä vastaan, riittää suurimman mahdollisen CP:n tai IV:n tavoittelu, mutta PVP-tarkoitukseen jokaisella pokémonilla on toiset erilliset IV-arvot. PVP-otteluissa pokémon, jonka IV on 100%, sopii kyseiseen Leagueen parhaiten vain, jos sen CP-maksimiarvo jää alle Leaguen CP-rajan. Mutta useimmissa Great Leagueen ja Ultra Leagueen soveltuvissa pokémoneissa puolustuksen ja kestävyyden suositellaan olevan korkeampia kuin hyökkäyksen, mutta ei liian matalia maksimiin verrattuna. Omien pokémonien PVP-kohtaiset IV:t voi katsoa erilaisilla sivustoilla tai sovelluksissa olevista laskureista, kuten <a href="https://www.stadiumgaming.gg/rank-checker">GO Stadiumin Rank Checkeristä</a>.
                    </font>
                    <br /><br />
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        TIIMINRAKENNUS
                    </font>
                    <br /><br />
                    {props.city !== 'helsinki' && props.city !== 'kingdom' && props.city !== 'tower' && props.city !== 'doublestar' &&
                        <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            Useimmissa PVP-turnausformaateissa, mukaan lukien League Cityssä,{' '}
                        </font>
                    }
                    {props.city === 'helsinki' && 
                        <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            Useimmissa PVP-turnausformaateissa, mukaan lukien League Metropoliksessa,{' '}
                        </font>
                    }
                    {props.city !== 'tower' && 
                        <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            jokainen osallistuja varaa yhden turnauksen tai turnausvaiheen ajaksi kuuden pokémonin tiimin.
                        </font>
                    }
                    <br /><br />
                    <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        <i>Tämä osio on vielä työn alla.</i>
                    </font>
                </Typography>
            }
            {subtab !== 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <Link component="button" onClick={() => {window.scrollTo(0, 0); setSubtab('mainSubtab')}}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                                {'≪'} Palaa PVP-vinkkeihin
                            </center>
                        </font>
                    </Link>
                    <br /><br />
                    <TypeDetails subtab={subtab} setSubtab={setSubtab} type={subtab} city={props.city} cityTheme={props.theme} />
                </Typography>
            }
        </Container>
    );
}
export default PVPguide;
import { Button } from '@mui/material';
import React from 'react';
import SpecialSpecialTypeData from './data/SpecialSpecialTypeData.json';
import SpecialTypeData from './data/SpecialTypeData.json';
import TypeData from './data/TypeData.json';

export const BadgeList = (props) => {

    let rowsOfSix = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        row.push(TypeData.types[i]);
        if (row.length === 11) {
            rowsOfSix.push(row);
            row = [];
        }
        else {
            if (i < (TypeData.types.length - 1)) row.push('space');
            else {
                rowsOfSix.push(row);
                row = [];
            }
        }
    }

    const renderBadgeTableRow = (badgeTableRow) => {
        return(
            <tr height="120px">
                {badgeTableRow.map((badge) => renderBadge(badge))}
            </tr>
        );
    }

    const renderBadge = (badge) => {
        if (badge === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        return(
            <td align="center" width="100px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(badge.engName); props.setSubtabType('type')}}>
                    <img src={require('./resources/badges/' + badge.engName + '.png')} className="Badge" alt="badge" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={badge.color}>
                    {badge.name.toUpperCase()}<br />{badge.engName.toUpperCase()}
                </font>
            </td>
        );
    }

    return(
        <table align="center">
            <tbody>
                {rowsOfSix.map((badgeTableRow) => renderBadgeTableRow(badgeTableRow))}
            </tbody>
        </table>
    );
}

export const SpecialBadgeList = (props) => {

    let row = [];

    for (var i = 0; i < SpecialTypeData.types.length; i++) {
        row.push(SpecialTypeData.types[i]);
        
        if (i < (SpecialTypeData.types.length - 1)) row.push('space');
    }

    const renderBadge = (badge) => {
        if (badge === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        return(
            <td align="center" width="100px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(badge.engName); props.setSubtabType('type')}}>
                    <img src={require('./resources/badges/' + badge.engName + '.png')} className="Badge" alt="badge" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={badge.color}>
                    {badge.name.toUpperCase()}<br />{badge.engName.toUpperCase()}
                </font>
            </td>
        );
    }

    return(
        <table align="center">
            <tbody>
                {row.map((badge) => renderBadge(badge))}
            </tbody>
        </table>
    );
}

export const SpecialSpecialBadgeList = (props) => {

    let row = [];
    let entryAmount = 0;

    for (var i = 0; i < SpecialSpecialTypeData.types.length; i++) {
        if (SpecialSpecialTypeData.types[i].year <= props.year) {
            if (entryAmount > 0) row.push('space');
            row.push(SpecialSpecialTypeData.types[i]);
            entryAmount++;
        }
    }

    const renderBadge = (badge) => {
        if (badge === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        return(
            <td align="center" width="100px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(badge.engName); props.setSubtabType('type')}}>
                    <img src={require('./resources/badges/' + badge.engName + '.png')} className="Badge" alt="badge" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={badge.color}>
                    {badge.name.toUpperCase()}<br />{badge.engName.toUpperCase()}
                </font>
            </td>
        );
    }

    return(
        <table align="center">
            <tbody>
                {row.map((badge) => renderBadge(badge))}
            </tbody>
        </table>
    );
}
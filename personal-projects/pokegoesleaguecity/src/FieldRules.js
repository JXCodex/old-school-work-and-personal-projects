import { Box, Container, Link, Typography } from '@mui/material';

import FieldData from './data/FieldData.json';
import { FullPokemonList } from './FullPokemonList';
import PokemonAlolaData from './data/pokemon/pokemonAlolaData.json';
import PokemonGalarData from './data/pokemon/pokemonGalarData.json';
import PokemonHisuiData from './data/pokemon/pokemonHisuiData.json';
import PokemonHoennData from './data/pokemon/pokemonHoennData.json';
import PokemonJohtoData from './data/pokemon/pokemonJohtoData.json';
import PokemonKalosData from './data/pokemon/pokemonKalosData.json';
import PokemonKantoData from './data/pokemon/pokemonKantoData.json';
import PokemonPaldeaData from './data/pokemon/pokemonPaldeaData.json';
import PokemonSinnohData from './data/pokemon/pokemonSinnohData.json';
import PokemonUnovaData from './data/pokemon/pokemonUnovaData.json';
import React from 'react';
import SpecialSpecialTypeData from './data/SpecialSpecialTypeData.json';
import TypeData from './data/TypeData.json';
import alola from './resources/types/alola.png';
import galar from './resources/types/galar.png';
import hisui from './resources/types/hisui.png';
import hoenn from './resources/types/hoenn.png';
import johto from './resources/types/johto.png';
import kalos from './resources/types/kalos.png';
import kanto from './resources/types/kanto.png';
import paldea from './resources/types/paldea.png';
import sinnoh from './resources/types/sinnoh.png';
import unova from './resources/types/unova.png';

export const FieldRules = (props) => {

    var i; var j;
    let fieldIndex = -1;
    let fieldThemeColor = null;
    let fieldThemeLightColor = null;
    let contrastText = null;

    for (i = 0; i < FieldData.fields.length; i++) {
        if (FieldData.fields[i].code === props.subtab) {
            fieldIndex = i;
            fieldThemeColor = FieldData.fields[i].textColor;
            fieldThemeLightColor = FieldData.fields[i].lightColor;
            contrastText = FieldData.fields[i].contrastText;
        }
    }

    const renderPokeType = (type) => {

        let engName = '';
        let themeColor = '';
        let typeText = '???';

        for (var i = 0; i < TypeData.types.length; i++) {
            if (type === TypeData.types[i].name) {
                typeText = TypeData.types[i].name.toUpperCase() + ' / ' + TypeData.types[i].engName.toUpperCase();
                themeColor = TypeData.types[i].color;
                engName = TypeData.types[i].engName;
            }
        }

        return(
            <div>
                <Link component="button" className="Site-body-link" onClick={() => {window.scrollTo(0, 0); props.setSubtab(engName); props.setSubtabType('type')}}>
                    <font face="Segoe UI Semibold" color={themeColor}>
                        {typeText} <img src={require('./resources/types/' + engName + '.png')} className="TypeIconText" alt="icon" />
                    </font>
                </Link>
            </div>
        );
    };

    const renderCategoryType = (category) => {

        let engName = '';
        let themeColor = '';
        let categoryText = '???';

        for (var i = 0; i < SpecialSpecialTypeData.types.length; i++) {
            if (category === SpecialSpecialTypeData.types[i].name) {
                if (SpecialSpecialTypeData.types[i].name !== 'mega' && SpecialSpecialTypeData.types[i].name !== 'dynamax' && SpecialSpecialTypeData.types[i].name !== 'gigantamax') categoryText = SpecialSpecialTypeData.types[i].name.toUpperCase() + ' / ' + SpecialSpecialTypeData.types[i].engName.toUpperCase();
                else categoryText = SpecialSpecialTypeData.types[i].name.toUpperCase();
                themeColor = SpecialSpecialTypeData.types[i].color;
                engName = SpecialSpecialTypeData.types[i].engName;
            }
        }

        return(
            <div>
                <Link component="button" className="Site-body-link" onClick={() => {window.scrollTo(0, 0); props.setSubtab(engName); props.setSubtabType('type')}}>
                    <font face="Segoe UI Semibold" color={themeColor}>
                        {categoryText} <img src={require('./resources/types/' + engName + '.png')} className="TypeIconText" alt="icon" />
                    </font>
                </Link>
            </div>
        );
    };

    const renderDualPokeType = (types) => {

        let icon1 = '', icon2 = '';
        let themeColor = '';
        let text = '???';

        if ((types.type1 === 'ötökkä' && types.type2 === 'teräs') || (types.type1 === 'teräs' && types.type2 === 'ötökkä')) {
            icon1 = 'bug'; icon2 = 'steel'; themeColor = '#59B04D'; text = 'TERÄSÖTÖKÄT / STEEL BUGS';
        }
        else if ((types.type1 === 'maa' && types.type2 === 'vesi') || (types.type1 === 'vesi' && types.type2 === 'maa')) {
            icon1 = 'ground'; icon2 = 'water'; themeColor = '#8781A0'; text = 'MUTAPOKÉMONIT / MUD POKÉMON';
        }
        else {
            for (var k = 0; k < TypeData.types.length; k++) {
                if (types.type1 === TypeData.types[k].name) {
                    icon1 = TypeData.types[k].engName;
                }
                else if (types.type2 === TypeData.types[k].name) {
                    icon2 = TypeData.types[k].engName;
                }
            }
            themeColor = fieldThemeColor;
            text = types.type1.toUpperCase() + '-' + types.type2.toUpperCase() + ' -HYBRIDIT';
        }

        for (i = 0; i < FieldData.fields.length; i++) {
            for (j = 0; j < FieldData.fields[i].forbiddenDualTypes.length; j++) {
                if (types.type1 === FieldData.fields[i].forbiddenDualTypes[j].type1 && types.type2 === FieldData.fields[i].forbiddenDualTypes[j].type2) {
                    text = text + ' / ' + FieldData.fields[i].forbiddenEngDualTypes[j].type1.toUpperCase() + '-' + FieldData.fields[i].forbiddenEngDualTypes[j].type2.toUpperCase() + ' HYBRIDS';
                }
            }
        }

        return(
            <font className="Site-body-text" face="Segoe UI Semibold" color={themeColor}>
                {text} <img src={require('./resources/types/' + icon1 + '.png')} className="TypeIconText" alt="icon1" />+<img src={require('./resources/types/' + icon2 + '.png')} className="TypeIconText" alt="icon2" /> 
                <br />
            </font>
        );
    };

    const pokemonNumber = (id) => {
        let newId = id.slice(0, 4);

        return (<font>#{newId}</font>);
    }

    const renderForbiddenPokemon = (pokemon) => {

        let pokemonName = pokemon.name;
        let region = '';
        let idTrue = false;

        for (i = 0; i < FullPokemonList.length; i++) {
            if (FullPokemonList[i].id === pokemon) {
                idTrue = true;
                pokemonName = FullPokemonList[i].name;
                region = FullPokemonList[i].region;
                for (j = 0; j < FullPokemonList[i].categories.length; j++) {
                    if (FullPokemonList[i].categories[j] === 'muoto') {
                        pokemonName = pokemon.engName;
                        
                    }
                } 
            }
        }

        if (idTrue === false) {
            return(<font />);
        }

        return(
            <abbr title={pokemonName}>
                <Box borderRadius="50%" style={{ width: '70px', height: '70px', backgroundColor: fieldThemeLightColor, display: 'inline-block' }}>
                    <center>
                        <font className="PokemonInfo" face="Segoe UI Semibold" color={contrastText}>
                            {pokemonNumber(pokemon)}
                        </font>
                        <br />
                        <img src={require('./data/pokemon/' + region + '/' + pokemon + '.png')} alt={'./data/pokemon/' + region + '/' + pokemon + '.png'} style={{ width: '40px', height: '40px' }} />
                    </center>
                </Box>
            </abbr>
        );
    }

    const renderPokemon = (pokemon) => {

        let pokemonName = pokemon.name;
        let typeTrue = false;
        let categoriesTrue = true;
        let idTrue = true;
        let doubleType1True = false;
        let doubleType2True = false;

        for (i = 0; i < pokemon.categories.length; i++) {
            if (pokemon.categories[i] === 'muoto') pokemonName = pokemon.engName;
            if (pokemon.categories[i] === 'legendaarinen' && props.subtab !== 'semiopen') categoriesTrue = false;
            if (pokemon.categories[i] === 'myyttinen' && props.subtab !== 'semiopen') categoriesTrue = false;
            if (pokemon.categories[i] === 'mega') categoriesTrue = false;
            if (pokemon.categories[i] === 'ultrapeto' && props.subtab !== 'semiopen') categoriesTrue = false;
        }

        for (i = 0; i < pokemon.types.length; i++) {
            for (j = 0; j < FieldData.fields[fieldIndex].allowedTypes.length; j++) {
                if (pokemon.types[i] === FieldData.fields[fieldIndex].allowedTypes[j]) {
                    typeTrue = true;
                }
            }
            for (j = 0; j < FieldData.fields[fieldIndex].forbiddenTypes.length; j++) {
                if (pokemon.types[i] === FieldData.fields[fieldIndex].forbiddenTypes[j]) { 
                    return(<font />);
                }
            }
        }

        for (i = 0; i < FieldData.fields[fieldIndex].forbiddenIds.length; i++) {
            if (pokemon.id === FieldData.fields[fieldIndex].forbiddenIds[i]) {
                idTrue = false;
            }
        }

        for (i = 0; i < FieldData.fields[fieldIndex].forbiddenDualTypes.length; i++) {
            for (j = 0; j < pokemon.types.length; j++) {
                if (pokemon.types[j] === FieldData.fields[fieldIndex].forbiddenDualTypes[i].type1) {
                    doubleType1True = true;
                }
                else if (pokemon.types[j] === FieldData.fields[fieldIndex].forbiddenDualTypes[i].type2) {
                    doubleType2True = true;
                }
            }
        }

        if (pokemon.available === false || typeTrue === false || categoriesTrue === false || idTrue === false) {
            return(<font />);
        }

        if (doubleType1True === true && doubleType2True === true) {
            return(<font />);
        }

        return(
            <abbr title={pokemonName}>
                <Box borderRadius="50%" style={{ width: '70px', height: '70px', backgroundColor: fieldThemeLightColor, display: 'inline-block' }}>
                    <center>
                        <font className="PokemonInfo" face="Segoe UI Semibold" color={contrastText}>
                            {pokemonNumber(pokemon.id)}
                        </font>
                        <br />
                        <img src={require('./data/pokemon/' + pokemon.region + '/' + pokemon.id + '.png')} alt={'./data/pokemon/' + pokemon.region + '/' + pokemon.id + '.png'} style={{ width: '40px', height: '40px' }} />
                    </center>
                </Box>
            </abbr>
        );
    }

    let fieldName = null;
    let engFieldName = null;

    for (var k = 0; k < FieldData.fields.length; k++) {
        if (props.subtab === FieldData.fields[k].code) {
            fieldName = FieldData.fields[k].name;
            engFieldName = FieldData.fields[k].engName;
        }
    }

    return(
        <div>
            <center>
                <img src={require('./resources/fields/' + props.subtab + '.png')} className="Field" alt="field" />
                <br />
                <font className="Site-body-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                    {fieldName.toUpperCase() + ' / ' + engFieldName.toUpperCase()}
                </font>
                <br /><br />
                <table align="center">
                    <tbody>
                        <tr>
                            {props.subtab !== 'semiopen' && <td align="center" valign="top" width="32%" min-width="32%" max-width="32%">
                                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                                    VAADITUT TYYPIT
                                </font>
                                <br />
                                {FieldData.fields[fieldIndex].allowedTypes.map((type) => renderPokeType(type))}
                            </td>}
                            {props.subtab !== 'semiopen' && <td align="center" valign="top" width="2%" min-width="2%" max-width="2%" />}
                            {props.subtab !== 'semiopen' && <td align="center" valign="top" width="32%" min-width="32%" max-width="32%">
                                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                                    KIELLETYT TYYPIT
                                </font>
                                <br />
                                {FieldData.fields[fieldIndex].forbiddenTypes.map((type) => renderPokeType(type))}
                            </td>}
                            {props.subtab !== 'semiopen' && <td align="center" valign="top" width="2%" min-width="2%" max-width="2%" />}
                            <td align="center" valign="top" width="32%" min-width="32%" max-width="32%">
                                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                                    KIELLETYT RYHMÄT
                                </font>
                                <br />
                                {FieldData.fields[fieldIndex].forbiddenCategories.map((category) => renderCategoryType(category))}
                            </td>
                        </tr>
                    </tbody>
                </table>
                {FieldData.fields[fieldIndex].forbiddenDualTypes.length > 0 && <table align="center">
                    <br />
                    <tbody>
                        <tr>
                            <td align="center" valign="top" width="32%" min-width="32%" max-width="32%">
                                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                                    KIELLETYT KAKSOISTYYPIT
                                </font>
                                <br />
                                {FieldData.fields[fieldIndex].forbiddenDualTypes.map((types) => renderDualPokeType(types))}
                            </td>
                        </tr>
                    </tbody>
                </table>}
                {props.subtab === 'open' && <table align="center">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">
                                <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                                    KIELLETYT RYHMÄT
                                </font>
                                <br />
                                {FieldData.fields[fieldIndex].forbiddenCategories.map((category) => renderCategoryType(category))}
                            </td>
                        </tr>
                    </tbody>
                </table>}
                <br />
                {props.subtab !== 'semiopen' && <div>
                    <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                        KIELLETYT POKÉMON-LAJIT
                    </font>
                    <br />
                    {FieldData.fields[fieldIndex].forbiddenIds.map((pokemon) => renderForbiddenPokemon(pokemon))}
                    <br /><br />
                </div>}
            </center>
            <Container>
                <Typography style={{ backgroundColor: '#FFFFFF' }}>
                    <center>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color={fieldThemeColor}>
                            <center>
                                LUETTELO KENTÄN SALLITUISTA POKÉMONEISTA
                            </center>
                        </font>
                        {props.subtab !== 'open' && <font className="Site-body-text" face="Segoe UI Semibold" color={fieldThemeColor}>
                            Kaksityyppisten pokémonien kohdalla riittää, että vähintään toinen sen tyypeistä on yksi kentän vaadituista tyypeistä.{' '}
                        </font>}
                        {props.subtab !== 'open' && <font className="Site-body-text" face="Segoe UI Semibold" color={fieldThemeColor}>
                            Alolassa ja Galarissa on toistaiseksi vain uudenlaisia variaatioita edellisten alueiden pokémoneista.
                        </font>}
                        {props.subtab === 'open' && <font className="Site-body-text" face="Segoe UI Semibold" color={fieldThemeColor}>
                            Alolassa ja Galarissa ovat toistaiseksi vain Meltan, Melmetal sekä uudenlaisia variaatioita edellisten alueiden pokémoneista.
                        </font>}
                        <font className="Site-body-text" face="Segoe UI Semibold" color={fieldThemeColor}>
                            <br /><br />
                            Pidä hiirtä pokémonin kuvan kohdalla nähdäksesi pokémonin nimi.
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            KANTO <img src={kanto} className="TypeIcon" alt="kanto-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonKantoData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            JOHTO <img src={johto} className="TypeIcon" alt="johto-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonJohtoData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            HOENN <img src={hoenn} className="TypeIcon" alt="hoenn-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonHoennData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            SINNOH <img src={sinnoh} className="TypeIcon" alt="sinnoh-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonSinnohData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            UNOVA <img src={unova} className="TypeIcon" alt="unova-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonUnovaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            KALOS <img src={kalos} className="TypeIcon" alt="kalos-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonKalosData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font> 
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            ALOLA <img src={alola} className="TypeIcon" alt="alola-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonAlolaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            GALAR <img src={galar} className="TypeIcon" alt="galar-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonGalarData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            HISUI <img src={hisui} className="TypeIcon" alt="hisui-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonHisuiData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color='#FDA08F'>
                            <br />
                            PALDEA <img src={paldea} className="TypeIcon" alt="hisui-icon" />
                            <br />
                            <font className="Site-body-text" face="Segoe UI Semibold" color='#FDA08F'>
                                {PokemonPaldeaData.pokemon.map((pokemon) => renderPokemon(pokemon))}
                            </font>
                            <br />
                        </font>
                    </center>
                </Typography>
            </Container>
        </div>
    );
}
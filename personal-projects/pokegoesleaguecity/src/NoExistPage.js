import './Main.css';

import { Button, Container, Typography } from '@mui/material';

import KeyboardReturnIcon from '@mui/icons-material/KeyboardReturn';
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { mainTheme } from './CityThemes';

export const NoExistPage = (props) => {
    return (
        <div>
            <br /><br />
            <Container>
                <Typography className="Main-body-text" style={{ backgroundColor: '#FFFFFF', height: '750px' }}>
                    <ThemeProvider theme={{...props.theme}}>
                        {props.city !== 'kingdom' && props.city !== 'doublestar' && <font face="Segoe UI Semibold" color={props.theme.palette.primary.main} className="Notifi-caption">
                            Pahoittelemme, mutta tällä seutukunnalla ei ole vielä omia sivuja asetettuna.
                        </font>}
                        {props.city === 'kingdom' && <font face="Segoe UI Semibold" color={props.theme.palette.primary.main} className="Notifi-caption">
                            Pahoittelemme, mutta League Kingdomia ei ole vielä olemassa.
                        </font>}
                        {props.city === 'doublestar' && <font face="Segoe UI Semibold" color={props.theme.palette.primary.main} className="Notifi-caption">
                            Pahoittelemme, mutta Double Star Tournamentia ei ole vielä olemassa.
                        </font>}
                        <br /><br />
                        <Button href="/" /* color={props.theme.palette.primary.main} */ variant="contained" startIcon={<KeyboardReturnIcon /* style={{ color: props.theme.palette.primary.main }} */ />} onClick={() => {window.scrollTo(0, 0); props.setTheme(mainTheme)}}>
                            <font face="Segoe UI Semibold">
                                PALAA ETUSIVULLE
                            </font>
                        </Button>
                    </ThemeProvider>
                </Typography>
            </Container>
        </div>
    );
}
export default NoExistPage;
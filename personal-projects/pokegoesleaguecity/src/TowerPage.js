import './CityAndTower.css';

import { AppBar, Box, Button, ButtonGroup, Container, Paper, Toolbar, Typography } from '@mui/material';
import React, { useState } from 'react';

import PVPguide from './PVPguide';
import { ThemeProvider } from '@mui/material/styles';
import TowerMain from './TowerMain';
import TowerPlayerStats from './TowerPlayerStats';
import TowerRules from './TowerRules';
import { mainTheme } from './CityThemes';

export const TowerPage = (props) => {
    const [tab, setTab] = useState('mainTab');

    let currentDate = new Date();
    const [year] = useState(currentDate.getFullYear());
    
    let scrollText = 'BATTLE CHATEAU TOWERIN OVET AVATAAN LÄHITULEVAISUUDESSA!';
    
    const styles = {
        paperContainer: {
            backgroundImage: `url(${require('./resources/backgrounds/tower.png')})`,
        }
    };

    return(
        <div style={{ backgroundColor: props.theme.palette.primary.main, color: props.theme.palette.primary.contrastText, }}>
            <Paper style={styles.paperContainer} square>
                <ThemeProvider theme={{...props.theme}}>
                    <AppBar>
                        <Toolbar className="AppBar">
                            <ButtonGroup color="inherit" variant="text">
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('mainTab')}}>
                                    <font face="Segoe UI Semibold">
                                        ETUSIVU
                                    </font>
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('rules')}}>
                                    <font face="Segoe UI Semibold">
                                        SÄÄNNÖT
                                    </font>
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('PVPguide')}}>
                                    <font face="Segoe UI Semibold">
                                        PVP-OPAS
                                    </font>
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); setTab('playerStats')}}>
                                    <font face="Segoe UI Semibold">
                                        PELAAJAT
                                    </font>
                                </Button>
                                <Button color="inherit" variant="text" onClick={() => {window.scrollTo(0, 0); props.setCity('main'); props.setFixedCity('Main'); props.setPrevCity('main'); props.setTheme(mainTheme)}}>
                                    <font face="Segoe UI Semibold">
                                        PALAA LEAGUE CITYYN
                                    </font>
                                </Button>
                            </ButtonGroup>
                        </Toolbar>
                    </AppBar>
                </ThemeProvider>
                <Container>
                    <header className="Site-header whiteHeaderContainer">
                        <br className="LogoUpBr" />
                        <img src={require('./resources/logos/' + props.city + '.png')} className="Site-logo" alt="Site-logo" />
                    </header>
                </Container>
                {year > 2020 &&
                    <Container>
                        <Typography className="whiteMidContainer" style={{ backgroundColor: '#FFFFFF' }}>
                            <br />
                            <Box className="ScrollFrame">
                                <Box className="ScrollContainer">
                                    <Box className="ScrollText">
                                        <font className="Site-body-mid-header" face="Segoe UI Semibold" color={props.theme.palette.primary.contrastText}>
                                            {scrollText}
                                        </font>
                                    </Box>
                                </Box>
                            </Box>
                        </Typography>
                    </Container>
                }
                {tab === 'mainTab' && <TowerMain login={props.login} username={props.username} city={props.city} tab={tab} setTab={setTab} theme={props.theme} />}
                {tab === 'rules' && <TowerRules city={props.city} theme={props.theme} />}
                {tab === 'PVPguide' && <PVPguide city={props.city} theme={props.theme} />}
                {tab === 'playerStats' && <TowerPlayerStats city={props.city} yourUsername={props.username} theme={props.theme} />}
            </Paper>
        </div>
    );
}
export default TowerPage;
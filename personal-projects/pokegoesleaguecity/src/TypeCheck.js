import { Button } from '@mui/material';
import React from 'react';
import SpecialTypeData from './data/SpecialTypeData.json';
import TypeData from './data/TypeData.json';

export const TypeStrengthCheck = (props) => {

    let rowsOfFive = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        if (TypeData.types[i].engName === props.type) {
            for (var j = 0; j < TypeData.types[i].strengths.length; j++) {
                row.push(TypeData.types[i].strengths[j]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (j < (TypeData.types[i].strengths.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        if (iconTableRow.length === 9) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFive(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 7) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFour(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 5) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconThree(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 3) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconTwo(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 1) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconOne(icon))}
                </tr>
            );
        }
        return (<font><br /></font>);
    }

    // Viiden kuvakkeen riveille
    const renderIconFive = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Neljän kuvakkeen riveille
    const renderIconFour = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kolmen kuvakkeen riveille
    const renderIconThree = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kahden kuvakkeen riveille
    const renderIconTwo = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Yhden kuvakkeen riveille
    const renderIconOne = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFive.length === 0) return(<font><br /></font>);

    return(
        <table align="center">
            <tbody>
                {rowsOfFive.map((iconTableRow) => renderIconTableRow(iconTableRow))}
            </tbody>
        </table>
    );
}

export const TypeWeaknessCheck = (props) => {
    
    let rowsOfFive = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        if (TypeData.types[i].engName === props.type) {
            for (var j = 0; j < TypeData.types[i].weaknesses.length; j++) {
                row.push(TypeData.types[i].weaknesses[j]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (j < (TypeData.types[i].weaknesses.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        if (iconTableRow.length === 9) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFive(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 7) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFour(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 5) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconThree(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 3) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconTwo(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 1) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconOne(icon))}
                </tr>
            );
        }
        return (<br />);
    }

    // Viiden kuvakkeen riveille
    const renderIconFive = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Neljän kuvakkeen riveille
    const renderIconFour = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kolmen kuvakkeen riveille
    const renderIconThree = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kahden kuvakkeen riveille
    const renderIconTwo = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Yhden kuvakkeen riveille
    const renderIconOne = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFive.length === 0) return(<font><br /></font>);

    return(
        <table align="center">
            <tbody>
                {rowsOfFive.map((iconTableRow) => renderIconTableRow(iconTableRow))}
            </tbody>
        </table>
    );
}

export const TypeResistanceCheck = (props) => {
    
    let rowsOfFive = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        if (TypeData.types[i].engName === props.type) {
            for (var j = 0; j < TypeData.types[i].resistances.length; j++) {
                row.push(TypeData.types[i].resistances[j]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (j < (TypeData.types[i].resistances.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        if (iconTableRow.length === 9) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFive(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 7) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconFour(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 5) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconThree(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 3) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconTwo(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 1) {
            return(
                <tr height="90px">
                    {iconTableRow.map((icon) => renderIconOne(icon))}
                </tr>
            );
        }
        return (<br />);
    }

    // Viiden kuvakkeen riveille
    const renderIconFive = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Neljän kuvakkeen riveille
    const renderIconFour = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kolmen kuvakkeen riveille
    const renderIconThree = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kahden kuvakkeen riveille
    const renderIconTwo = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Yhden kuvakkeen riveille
    const renderIconOne = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFive.length === 0) return(<font><br /></font>);

    return(
        <table align="center">
            <tbody>
                {rowsOfFive.map((iconTableRow) => renderIconTableRow(iconTableRow))}
            </tbody>
        </table>
    );
}

export const TypeImmunityCheck = (props) => {
    
    let rowsOfFive = [];
    let row = [];

    for (var i = 0; i < TypeData.types.length; i++) {
        if (TypeData.types[i].engName === props.type) {
            for (var j = 0; j < TypeData.types[i].immunities.length; j++) {
                row.push(TypeData.types[i].immunities[j]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (j < (TypeData.types[i].immunities.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        if (iconTableRow.length === 3) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconTwo(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 1) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconOne(icon))}
                </tr>
            );
        }
        return (<br />);
    }

    // Kahden kuvakkeen riveille
    const renderIconTwo = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Yhden kuvakkeen riveille
    const renderIconOne = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFive.length === 0) return(<font><br /></font>);

    return(
        <table align="center">
            <tbody>
                {rowsOfFive.map((iconTableRow) => renderIconTableRow(iconTableRow))}
            </tbody>
        </table>
    );
}

export const CertainTypesCheck = (props) => {

    let rowsOfFive = [];
    let row = [];

    for (var i = 0; i < SpecialTypeData.types.length; i++) {
        if (SpecialTypeData.types[i].engName === props.type && props.allow === true) {
            for (var j = 0; j < SpecialTypeData.types[i].allowedTypes.length; j++) {
                row.push(SpecialTypeData.types[i].allowedTypes[j]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (j < (SpecialTypeData.types[i].allowedTypes.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
        else if (SpecialTypeData.types[i].engName === props.type && props.allow === false) {
            for (var k = 0; k < SpecialTypeData.types[i].forbiddenTypes.length; k++) {
                row.push(SpecialTypeData.types[i].forbiddenTypes[k]);
                
                if (row.length === 9) {
                    rowsOfFive.push(row);
                    row = [];
                }
                else {
                    if (k < (SpecialTypeData.types[i].forbiddenTypes.length - 1)) row.push('space');
                    else {
                        rowsOfFive.push(row);
                        row = [];
                    }
                }
            }
        }
    }

    const renderIconTableRow = (iconTableRow) => {

        if (iconTableRow.length === 9) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconFive(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 7) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconFour(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 5) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconThree(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 3) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconTwo(icon))}
                </tr>
            );
        }
        else if (iconTableRow.length === 1) {
            return(
                <tr height="75px">
                    {iconTableRow.map((icon) => renderIconOne(icon))}
                </tr>
            );
        }
        return (<font><br /></font>);
    }

    // Viiden kuvakkeen riveille
    const renderIconFive = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Neljän kuvakkeen riveille
    const renderIconFour = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kolmen kuvakkeen riveille
    const renderIconThree = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Kahden kuvakkeen riveille
    const renderIconTwo = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    // Yhden kuvakkeen riveille
    const renderIconOne = (icon) => {

        if (icon === 'space') {
            return(
                <td align="center" width="20px" min-width="20px" max-width="20px" />
            );
        }

        let iconEngName = null;
        let textColor = null;

        for (var i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types[i].name === icon) {
                iconEngName = TypeData.types[i].engName;
                textColor = TypeData.types[i].color;
            }
        }

        return(
            <td align="center" height="80px" width="100px" min-height="80px" max-height="80px" min-width="100px" max-width="100px">
                <Button onClick={() => {window.scrollTo(0, 0); props.setSubtab(iconEngName); props.setSubtabType('type')}}>
                    <img src={require('./resources/types/' + iconEngName + '.png')} className="TypeIcon" alt="icon" />
                </Button>
                <br />
                <font face="Segoe UI Semibold" color={textColor}>
                    {icon.toUpperCase()}<br />{iconEngName.toUpperCase()}
                </font>
            </td>
        );
    }

    if (rowsOfFive.length === 0) return(<font><br /></font>);

    return(
        <table align="center">
            <tbody>
                {rowsOfFive.map((iconTableRow) => renderIconTableRow(iconTableRow))}
            </tbody>
        </table>
    );
}

export const CertainDualTypesCheck = (props) => {

    let type1 = '';
    let type2 = '';
    let icon1 = '';
    let icon2 = '';
    let themeColor = '';
    let text = '';
    let engText = '';

    for (var i = 0; i < SpecialTypeData.types.length; i++) {
        if (SpecialTypeData.types[i].engName === props.type) {
            type1 = SpecialTypeData.types[i].forbiddenDualTypes[0].type1;
            type2 = SpecialTypeData.types[i].forbiddenDualTypes[0].type2;
        }
    }

    if ((type1 === 'ötökkä' && type2 === 'teräs') || (type1 === 'teräs' && type2 === 'ötökkä')) {
        icon1 = 'bug'; icon2 = 'steel'; themeColor = '#59B04D'; text = 'TERÄSÖTÖKÄT / STEEL BUGS';
    }
    else if ((type1 === 'maa' && type2 === 'vesi') || (type1 === 'vesi' && type2 === 'maa')) {
        icon1 = 'ground'; icon2 = 'water'; themeColor = '#8781A0'; text = 'MUTAPOKÉMONIT / MUD POKÉMON';
    }
    else {
        for (var k = 0; k < TypeData.types.length; k++) {
            if (type1 === TypeData.types[k].name) {
                icon1 = TypeData.types[k].engName;
            }
            else if (type2 === TypeData.types[k].name) {
                icon2 = TypeData.types[k].engName;
            }
        }
        themeColor = props.color;
        text = type1.toUpperCase() + '-' + type2.toUpperCase() + ' -HYBRIDIT';

        for (i = 0; i < TypeData.types.length; i++) {
            if (TypeData.types.name === type1) {
                engText = TypeData.types.engName + '-' + engText;
            }
            else if (TypeData.types.name === type2) {
                engText = engText + TypeData.types.engName + ' HYBRIDS';
            }
        }
        text = text + ' / ' + engText
    }

    return(
        <table align="center">
            <tbody>
                <font className="Site-body-text" face="Segoe UI Semibold" color={themeColor}>
                    <center>
                        <img src={require('./resources/types/' + icon1 + '.png')} className="TypeIcon" alt="icon1" />+<img src={require('./resources/types/' + icon2 + '.png')} className="TypeIcon" alt="icon2" />
                        <br />
                        {text}
                        <br />
                    </center>
                </font>          
            </tbody>
        </table>
    );
}
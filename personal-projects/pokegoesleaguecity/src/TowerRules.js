import './CityAndTower.css';

import { Container, Typography } from '@mui/material';
import { baronTheme, commonerTheme, dukeTheme, earlTheme, grandDukeTheme, marquisTheme, viscountTheme } from './TowerRankThemes';

import React from 'react';
import baronReversalUniform from './resources/tower-ranks/1_BaronReversal.png';
import battleKey from './resources/others/battle-key.png';
import commonerUniform from './resources/tower-ranks/0_Commoner.png';
import dukeUniform from './resources/tower-ranks/5_Duke.png';
import earlUniform from './resources/tower-ranks/3_Earl.png';
import grandDukeUniform from './resources/tower-ranks/6_GrandDuke.png';
import marquisUniform from './resources/tower-ranks/4_Marquis.png';
import viscountUniform from './resources/tower-ranks/2_Viscount.png';

export const TowerRules = (props) => {
    // TowerRules sisältää koodin, joka määrittää Battle Chateau Tower -sääntösivun.

    return(
        <Container>
            <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                <font className="Site-body-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    SÄÄNNÖT
                </font>
                <br /><br />
                <font className="Site-body-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                    Säännöt ovat seuraavat:
                    <ul>
                        <li>Käy otteluita muita pelaajia vastaan joko Great League- (joissa CP enintään 1500) tai Ultra League -säännöillä (joissa CP enintään 2500). Legendaariset, myyttiset ja megakehitetyt pokémonit ovat kuitenkin kiellettyjä.</li>
                        <li>Voitte käydä ottelunne jollakin näistä kolmesta tavasta:</li>
                        <ul>
                            <li>Yhden erän ottelu 6 pokémonin tiimillä.</li>
                            <li>Kolmen erän ottelusetti 6 pokémonin tiimillä.</li>
                            <li>Viiden erän ottelusetti 6 pokémonin tiimillä.</li>
                        </ul>
                        <li>Voit käydä samaa pelaajaa vastaan yhden päivän aikana enintään 8 erää.</li>
                        <li>Jokaisesta ottelusta saa ottelupisteitä (BP eli Battle Points) ja jokaisella pelaajalla on tietty arvo (eng. Rank).</li>
                        <li>Jos pelaaja voittaa erässä korkeammalla arvolla olevan pelaajan tai häviää matalammalla arvolla olevalle pelaajalle, hän saa tai menettää lisäottelupisteitä.</li>
                        <li>Voidaksesi otella Battle Chateau Tower -ottelun, sinun ja vastustajasi on ensin ilmoitettava haluavanne otella. Ja jotta tulos voidaan rekisteröidä ja ottelupisteet laskea, ilmoittakaa tiimienne pokémonit ja ottelun lopputulos.</li>
                        <li>Mitkä tahansa ottelut hyväksytään Battle Chateau Toweriin, paitsi League Cityn The Champion Cupin ottelut.</li>
                        <br />
                        <center>
                            <br className="RuleBr" />
                                <img src={battleKey} className="Key-Large" alt="keyLarge" />
                            <br />
                        </center>
                        <li>Jos saavutat jonkin seuraavista tavoitteista, saat yhden <font face="Segoe UI Semibold" color={'#D5A500'}>otteluavaimen (eng. Battle Key)</font>:</li>
                        <ul>
                            <li>Voita 7 peräkkäisessä kolmen erän Great League -ottelusetissä jokaisesta väh. kaksi erää.</li>
                            <li>Voita 5 peräkkäisessä kolmen erän Great League -ottelusetissä kaikki erät.</li>
                            <li>Voita 5 peräkkäisessä viiden erän Great League -ottelusetissä jokaisesta väh. kolme erää.</li>
                            <li>Voita 3 peräkkäisessä viiden erän Great League -ottelusetissä kaikki erät.</li>
                        </ul>
                        <li>Otteluavaimilla voi päästä yrittämään Great League -säännöillä erikoishaasteita, joista voi voittaa tai menettää enemmän ottelupisteitä kuin normaaliotteluista. Kolmen erän erikoishaaste maksaa 1 otteluavaimen ja viiden erän erikoishaaste 2 otteluavainta. Mutta otteluavaimen käytöstä on ilmoitettava ennen ottelun aloitusta.</li>
                        <br />
                        <li>Arvosi riippuu seuraavista pisteistä, saavutat jonkin näistä arvoista:</li>
                        <ul>
                            <li><font face="Segoe UI Semibold" color={commonerTheme.palette.primary.main}>Torniaateliton (eng. Tower Commoner)</font>: ei yhtään voitettua ottelua.</li>
                            <li><font face="Segoe UI Semibold" color={baronTheme.palette.secondary.main}>Torniparoni (eng. Tower Baron) / Torniparonitar (eng. Tower Baroness)</font>: vähintään yksi voitettu erä.</li>
                            <li><font face="Segoe UI Semibold" color={viscountTheme.palette.primary.main}>Tornivarakreivi (eng. Tower Viscount) / Tornivarakreivitär (eng. Tower Viscountess)</font>: vähintään 15 ottelupistettä ja vähintään 10 erää oteltu.</li>
                            <li><font face="Segoe UI Semibold" color={earlTheme.palette.primary.main}>Tornijaarli (eng. Tower Earl) / Tornikreivitär (eng. Tower Countess)</font>: vähintään 50 ottelupistettä ja vähintään 30 erää oteltu.</li>
                            <li><font face="Segoe UI Semibold" color={marquisTheme.palette.primary.main}>Tornimarkiisi (eng. Tower Marquis) / Tornimarkiisitar (eng. Tower Marchioness)</font>: vähintään 120 ottelupistettä ja vähintään 60 erää oteltu.</li>
                            <li><font face="Segoe UI Semibold" color={dukeTheme.palette.primary.main}>Torniherttua (eng. Tower Duke) / Torniherttuatar (eng. Tower Duchess)</font>: vähintään 200 ottelupistettä ja vähintään 100 erää oteltu.</li>
                            <li><font face="Segoe UI Semibold" color={grandDukeTheme.palette.primary.main}>Tornisuurherttua (eng. Tower Grand Duke) / Tornisuurherttuatar (eng. Tower Grand Duchess)</font>: tätä arvoa voi kantaa vain pelaaja, jonka pisteytys on korkeampi kuin muilla torniherttuoilla/torniherttuattarilla.</li>
                        </ul>
                        <br />
                        <table align="center">
                            <tbody>
                                <tr height="140px">
                                    <td align="center" width="19.75%" min-width="19.75%" max-width="19.75%">
                                        <img src={commonerUniform} className="TowerUniform" alt="towerCommoner" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={commonerTheme.palette.primary.main}>
                                            TORNIAATELITON<br />TOWER COMMONER
                                        </font>
                                    </td>
                                    <td align="center" width="7%" min-width="7%" max-width="7%" />
                                    <td align="center" width="19.75%" min-width="19.75%" max-width="19.75%">
                                        <img src={baronReversalUniform} className="TowerUniform" alt="towerBaron" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={baronTheme.palette.secondary.main}>
                                            TORNIPARONI<br />TOWER BARON
                                        </font>
                                    </td>
                                    <td align="center" width="7%" min-width="7%" max-width="7%" />
                                    <td align="center" width="19.75%" min-width="19.75%" max-width="19.75%">
                                        <img src={viscountUniform} className="TowerUniform" alt="towerViscount" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={viscountTheme.palette.primary.main}>
                                            TORNIVARAKREIVI<br />TOWER VISCOUNT
                                        </font>
                                    </td>
                                    <td align="center" width="7%" min-width="7%" max-width="7%" />
                                    <td align="center" width="19.75%" min-width="19.75%" max-width="19.75%">
                                        <img src={earlUniform} className="TowerUniform" alt="towerCount" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={earlTheme.palette.primary.main}>
                                            TORNIJAARLI<br />TOWER EARL
                                        </font>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <table align="center">
                            <tbody>
                                <tr height="140px">
                                    <td align="center" width="10%" min-width="10%" max-width="10%" />
                                    <td align="center" width="20%" min-width="20%" max-width="20%">
                                        <img src={marquisUniform} className="TowerUniform" alt="towerMarquis" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={marquisTheme.palette.primary.main}>
                                            TORNIMARKIISI<br />TOWER MARQUIS
                                        </font>
                                    </td>
                                    <td align="center" width="10%" min-width="10%" max-width="10%" />
                                    <td align="center" width="20%" min-width="20%" max-width="20%">
                                        <img src={dukeUniform} className="TowerUniform" alt="towerDuke" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={dukeTheme.palette.primary.main}>
                                            TORNIHERTTUA<br />TOWER DUKE
                                        </font>
                                    </td>
                                    <td align="center" width="10%" min-width="10%" max-width="10%" />
                                    <td align="center" width="20%" min-width="20%" max-width="20%">
                                        <img src={grandDukeUniform} className="TowerUniform" alt="towerGrandDuke" />
                                        <br />
                                        <font face="Segoe UI Semibold" color={grandDukeTheme.palette.primary.main}>
                                            TORNISUURHERTTUA<br />TOWER GRAND DUKE
                                        </font>
                                    </td>
                                    <td align="center" width="10%" min-width="10%" max-width="10%" />
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <li>Pelaaja voi saada vain yhden arvoylennyksen tai arvoalennuksen ottelusettiä kohden ja ylennys tai alennus tapahtuu aina vasta setin päätyttyä. Seuraava ylennys tai alennus tapahtuu vasta, kun seuraava ottelusetti on käyty, vaikka ehdot muuten täyttyisivät.</li>
                        <li>Pelaajien ottelupisteet, arvot ja käyttämättömät otteluavaimet Battle Chateau Towerissa nollautuvat aina, kun League Cityn yksi kausi päättyy.</li>
                        <li>Pelaajalla, joka oli viimeisintä nollausta tornisuurherttua tai tornisuurherttuatar, saa kutsun League Cityn seuraavan kauden The Champion Cupiin ilman, että osallistuu The Badge Challengeen.</li>
                        <li>Jos taas tornisuurherttua tai tornisuurherttuatar on jo League Cityn viime kauden mestari tai vähintään yksi eliittikolmosista, hän voi itse valita yhden pelaajan, jolle antaa suoraan kutsu League Cityn seuraavan kauden The Champion Cupiin.</li>
                    </ul>
                </font>      
            </Typography>
        </Container>
    );
}
export default TowerRules;
import { createTheme } from '@mui/material/styles';

export const mainTheme = createTheme({ // Kaupunkiseutuvalikon väriteema
  palette: {
    primary: {
      light: '#000000',
      main: '#005AB5',
      dark: '#000000',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#000000',
      main: '#337BC3',
      dark: '#000000',
      contrastText: '#337BC3',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const hesaTheme = createTheme({ // Helsingin väriteema
  palette: {
    primary: {
      light: '#3390EE',
      main: '#0075EA',
      dark: '#0051A3',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#335F8E',
      main: '#003872',
      dark: '#00274F',
      contrastText: '#FFF',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const joensuuTheme = createTheme({ // Joensuun väriteema
  palette: {
    primary: {
      light: '#8E9CB7',
      main: '#7284A5',
      dark: '#4F5C73',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#8F3D40',
      main: '#8E9CB7',
      dark: '#A4AFC5',
      contrastText: '#000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const jyvasTheme = createTheme({ // Jyväskylän väriteema
  palette: {
    primary: {
      light: '#3399CC',
      main: '#0080C0',
      dark: '#005986',
      contrastText: '#fff',
    },
    secondary: {
      light: '#FFFFFF',
      main: '#FFFFFF',
      dark: '#B2B2B2',
      contrastText: '#000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const lahtiTheme = createTheme({ // Lahden väriteema
  palette: {
    primary: {
      light: '#989898',
      main: '#7F7F7F',
      dark: '#585858',
      contrastText: '#000',
    },
    secondary: {
      light: '#444444',
      main: '#222222',
      dark: '#000000',
      contrastText: '#FFF',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const kuopioTowerTheme = createTheme({ // Kuopion väriteema
  palette: {
    primary: {
      light: '#666666',
      main: '#333333',
      dark: '#000000',
      contrastText: '#F1D301',
    },
    secondary: {
      light: '#E3E3E3',
      main: '#DCDCDC',
      dark: '#9A9A9A',
      contrastText: '#FFF',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const poriTheme = createTheme({ // Porin väriteema
  palette: {
    primary: {
      light: '#DBC733',
      main: '#D3B901',
      dark: '#938100',
      contrastText: '#000',
    },
    secondary: {
      light: '#E7DB7B',
      main: '#E2D25B',
      dark: '#9E933F',
      contrastText: '#000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const treTheme = createTheme({ // Tampereen väriteema
  palette: {
    primary: {
      light: '#EF494F',
      main: '#EC1C24',
      dark: '#A51319',
      contrastText: '#FFF200',
    },
    secondary: {
      light: '#8F3D40',
      main: '#730D11',
      dark: '#50090B',
      contrastText: '#000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const turkuTheme = createTheme({ // Turun väriteema
  palette: {
    primary: {
      light: '#5A63C7',
      main: '#313CB9',
      dark: '#222A81',
      contrastText: '#F1D301',
    },
    secondary: {
      light: '#FFFFFF',
      main: '#FFFFFF',
      dark: '#B2B2B2',
      contrastText: '#000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const kingdomTheme = createTheme({ // League Kingdomin väriteema
  palette: {
    primary: {
      light: '#335B9A',
      main: '#003281',
      dark: '#00235A',
      contrastText: '#D4D4D4',
    },
    secondary: {
      light: '#E3E3E3',
      main: '#DCDCDC',
      dark: '#9A9A9A',
      contrastText: '#FFF',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});

export const doubleStarTheme = createTheme({ // Double Star Tournamentin väriteema
  palette: {
    primary: {
      light: '#33DDDD',
      main: '#00BEBE',
      dark: '#009595',
      contrastText: '#000000',
    },
    secondary: {
      light: '#33DDDD',
      main: '#00BEBE',
      dark: '#009595',
      contrastText: '#000000',
    },
  },
  typography: {
    "fontFamily": "Segoe UI Semibold"
  }
});
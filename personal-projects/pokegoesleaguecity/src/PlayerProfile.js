import './CityAndTower.css';

import { baronTheme, commonerTheme, dukeTheme, earlTheme, grandDukeTheme, marquisTheme, viscountTheme } from './TowerRankThemes';

import { Box } from '@mui/material';
import { FullPokemonList } from './FullPokemonList';
import React from 'react';
import SeasonsData from './data/SeasonsData.json';
import SpecialSpecialTypeData from './data/SpecialSpecialTypeData.json';
import SpecialTypeData from './data/SpecialTypeData.json';
import TypeData from './data/TypeData.json';
import baronUniform from './resources/tower-ranks/1_Baron.png';
import battleKey from './resources/others/battle-key-light.png';
import commonerUniform from './resources/tower-ranks/0_Commoner.png';
import dukeUniform from './resources/tower-ranks/5_Duke.png';
import earlUniform from './resources/tower-ranks/3_Earl.png';
import grandDukeUniform from './resources/tower-ranks/6_GrandDuke.png';
import instinctBlackEmblem from './resources/teams/instinct-black.png';
import instinctEmblem from './resources/teams/instinct.png';
import marquisUniform from './resources/tower-ranks/4_Marquis.png';
import mysticEmblem from './resources/teams/mystic.png';
import mysticWhiteEmblem from './resources/teams/mystic-white.png';
import pokeballCup from './resources/others/no-trophy-yes-cup.png';
import pokeballNoCup from './resources/others/no-trophy-no-cup.png';
import trophyFifth from './resources/others/trophy-fifth.png';
import trophyFourth from './resources/others/trophy-fourth.png';
import trophyTampereBronze from './resources/others/tampere/trophy-bronze.png';
import trophyTampereGold from './resources/others/tampere/trophy-gold.png';
import trophyTampereSilver from './resources/others/tampere/trophy-silver.png';
import valorEmblem from './resources/teams/valor.png';
import valorWhiteEmblem from './resources/teams/valor-white.png';
import viscountUniform from './resources/tower-ranks/2_Viscount.png';

export const PlayerProfile = (props) => {
    
    let currentDate = new Date();

    var bgColor = '#A9A9A9';
    var screenColor = '#A9A9A9';
    var textColor = '#000000';
    var emblem = null;
    
    if (props.city !== 'kuopio' && props.city !== 'tower') bgColor = props.theme.palette.primary.dark;
    else bgColor = '#9F9F9F';

    if (props.city !== 'tower') {
        if (props.team === 'Instinct') {
            screenColor = '#FFFF00';
            emblem = instinctBlackEmblem;
            textColor = '#000000';
        }
        else if (props.team === 'Mystic') {
            screenColor = '#4040FF';
            emblem = mysticWhiteEmblem;
            textColor = '#FFFFFF';
        }
        else if (props.team === 'Valor') {
            screenColor = '#FF0000';
            emblem = valorWhiteEmblem;
            textColor = '#FFFFFF';
        }
    }
    else {
        if (props.team === 'Instinct') {
            screenColor = '#000000';
            emblem = instinctEmblem;
            textColor = '#FFFF00';
        }
        else if (props.team === 'Mystic') {
            screenColor = '#000000';
            emblem = mysticEmblem;
            textColor = '#4040FF';
        }
        else if (props.team === 'Valor') {
            screenColor = '#000000';
            emblem = valorEmblem;
            textColor = '#FF0000';
        }
    }

    const renderPokemon = (pokemon) => {

        let cp = pokemon.cp;
        let id = false;
        let region = '';
        let pokemonName = '';

        for (var i = 0; i < FullPokemonList.length; i++) {
            if (FullPokemonList[i].id === pokemon.id) {
                id = FullPokemonList[i].id;
                region = FullPokemonList[i].region;
                pokemonName = FullPokemonList[i].name;
                for (var j = 0; j < FullPokemonList[i].categories.length; j++) {
                    if (FullPokemonList[i].categories[j] === 'muoto') pokemonName = FullPokemonList[i].engName;
                }
            }
        }

        if (id === false) {
            return(
                <Box borderRadius="50%" style={{ width: '70px', height: '70px', backgroundColor: props.theme.palette.primary.light, display: 'inline-block' }}>
                    <center>
                        <font className="PokemonInfo" face="Segoe UI Semibold" color={props.theme.palette.primary.contrastText}>
                            CP {cp}
                        </font>
                        <br />
                        ???
                    </center>
                </Box>
            );
        }

        return(
            <abbr title={pokemonName}>
                <Box borderRadius="50%" style={{ width: '70px', height: '70px', backgroundColor: props.theme.palette.primary.light, display: 'inline-block' }}>
                    <center>
                        <font className="PokemonInfo" face="Segoe UI Semibold" color={props.theme.palette.primary.contrastText}>
                            CP {cp}
                        </font>
                        <br />
                        <img src={require('./data/pokemon/' + region + '/' + id + '.png')} alt={'./data/pokemon/' + region + '/' + id + '.png'} style={{ width: '40px', height: '40px' }} />
                    </center>
                </Box>
            </abbr>
        );
    }

    const renderSeason = (season) => {

        let seasonEnd;

        if (season.city === 'Tampere') {
            for (var i = 0; i < SeasonsData.seasons.tampere.length; i++) {
                if (SeasonsData.seasons.tampere[i].season === currentDate.getFullYear()) {
                    seasonEnd = new Date(SeasonsData.seasons.tampere[i].cupEndDate);
                }
            }
        }

        if (props.city === 'tower') {
            return(
                <font face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                </font>
            );
        }

        const renderBadge = (badge) => {
            
            let srcAdd = '';
            let fixedBagde = '';
            if (badge.includes(' (hopea)') === true) {
                srcAdd = srcAdd + '-silver';
                let str = badge.split(' ');
                fixedBagde = str[0].toLowerCase();
                fixedBagde = fixedBagde.replace('merkki', '');
            }
            else {
                fixedBagde = badge.toLowerCase();
                fixedBagde = fixedBagde.replace('merkki', '');
            }

            for (var i = 0; i < TypeData.types.length; i++) {
                if (fixedBagde === TypeData.types[i].name) {
                    srcAdd = TypeData.types[i].engName + srcAdd;
                }
            }
            
            if (srcAdd === '' || srcAdd === '-silver') {
                for (var j = 0; j < SpecialTypeData.types.length; j++) {
                    if (fixedBagde === SpecialTypeData.types[j].name) {
                        srcAdd = SpecialTypeData.types[j].engName + srcAdd;
                    }
                }
            }
            
            if (srcAdd === '' || srcAdd === '-silver') {
                for (var k = 0; k < SpecialSpecialTypeData.types.length; k++) {
                    if (fixedBagde === SpecialSpecialTypeData.types[k].name) {
                        srcAdd = SpecialSpecialTypeData.types[k].engName + srcAdd;
                    }
                }
            }

            if (season.year >= 2020) {
                if (srcAdd === '' || srcAdd === '-silver') return (<font />)
                else if (srcAdd.includes('-silver') === true) {
                    let str = badge.split(' ');
                    
                    return(<abbr title={'Vanha ' + str[0] + ' on vielä voimassa'}><img src={require('./resources/badges/' + srcAdd + '.png')} className="Badge" alt="badge" /></abbr>);
                }
                else return(<abbr title={badge}><img src={require('./resources/badges/' + srcAdd + '.png')} className="Badge" alt="badge" /></abbr>);
            }
            
            if (srcAdd === '' || srcAdd === '-silver') return(<font />);
            else if (srcAdd.includes('-silver') === true) {
                let str = badge.split(' ');
                
                return(<abbr title={'Vanha ' + str[0] + ' on vielä voimassa'}><img src={require('./resources/badges/' + srcAdd + '.png')} className="Badge" alt="badge" /></abbr>);
            }
            return(<abbr title={badge}><img src={require('./resources/badges/' + srcAdd + '.png')} className="Badge" alt="badge" /></abbr>);
        }
        
        return(
            <div>
                <Box borderRadius={4} p={0.5} style={{ backgroundColor: bgColor, color: props.theme.palette.primary.contrastText }}>
                    <Box borderRadius={3.25} p={1} style={{ backgroundColor: '#FFFFFF', color: props.theme.palette.primary.main }}>
                        <font className="SeasonDescriptionTitle" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            <center>
                                Kausi {season.year}
                            </center>
                        </font>
                        <font className="SeasonDescriptionText" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            <center>
                                Seutukunta, jossa osallistui: {season.city}
                                <br />
                            </center>
                        </font>
                        {currentDate.getFullYear() === season.year && currentDate > seasonEnd && <center>
                            {season.finalPlace === 'LIIGAMESTARI' && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#DCAB01'>
                                <br />
                                <img src={trophyTampereGold} className="Trophy1" alt="trophy-gold" />
                                <br />
                                Lopullinen sijoitus: VOITTAJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #1' || season.finalPlace === 'ELIITTINELONEN #1') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#A1A1A1'>
                                <br />
                                <img src={trophyTampereSilver} className="Trophy1" alt="trophy-silver" />
                                <br />
                                Lopullinen sijoitus: 2. SIJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #2' || season.finalPlace === 'ELIITTINELONEN #2') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#8D573A'>
                                <br />
                                <img src={trophyTampereBronze} className="Trophy1" alt="trophy-bronze" />
                                <br />
                                Lopullinen sijoitus: 3. SIJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #3' || season.finalPlace === 'ELIITTINELONEN #3') && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#00AEF9'>
                                <br />
                                <img src={trophyFourth} className="Trophy4" alt="trophy-forth" />
                                <br />
                                Lopullinen sijoitus: 4. SIJA ({season.finalPlace})
                            </font>}
                            {season.finalPlace === 'ELIITTINELONEN #4' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#00AEF9'>
                                <br />
                                <img src={trophyFifth} className="Trophy4" alt="trophy-fifth" />
                                <br />
                                Lopullinen sijoitus: 5. SIJA ({season.finalPlace})
                            </font>}
                            {season.finalPlace === 'EI SIJOITUSTA' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#353535'>
                                <br />
                                <img src={pokeballNoCup} className="Trophy6" alt="no-trophy-no-cup" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                            {season.finalPlace !== 'LIIGAMESTARI' && season.finalPlace !== 'ELIITTIKOLMONEN #1' && season.finalPlace !== 'ELIITTINELONEN #1' && season.finalPlace !== 'ELIITTIKOLMONEN #2' && season.finalPlace !== 'ELIITTINELONEN #2' && season.finalPlace !== 'ELIITTIKOLMONEN #3' && season.finalPlace !== 'ELIITTINELONEN #3' && season.finalPlace !== 'ELIITTINELONEN #4' && season.finalPlace !== 'EI SIJOITUSTA' && season.finalPlace !== 'TURNAUS KESKEN' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#ED1C24'>
                                <br />
                                <img src={pokeballCup} className="Trophy6" alt="no-trophy-yes-cup" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                        </center>}
                        {currentDate.getFullYear() === season.year && currentDate <= seasonEnd && <center>
                            {(season.finalPlace === 'ELIITTIKOLMONEN #2' || season.finalPlace === 'ELIITTINELONEN #2') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#8D573A'>
                                <br />
                                <img src={trophyTampereBronze} className="Trophy1" alt="trophy-bronze" />
                                <br />
                                Lopullinen sijoitus: 3. SIJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #3' || season.finalPlace === 'ELIITTINELONEN #3') && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#00AEF9'>
                                <br />
                                <img src={trophyFourth} className="Trophy4" alt="trophy-forth" />
                                <br />
                                Lopullinen sijoitus: 4. SIJA ({season.finalPlace})
                            </font>}
                            {season.finalPlace !== 'TURNAUS KESKEN' && season.finalPlace !== 'EI SIJOITUSTA' && season.finalPlace !== 'HYLÄTTY' && season.finalPlace !== 'ELIITTIKOLMONEN #2' && season.finalPlace !== 'ELIITTINELONEN #2' && season.finalPlace !== 'ELIITTIKOLMONEN #3' && season.finalPlace !== 'ELIITTINELONEN #3' && season.finalPlace !== 'ELIITTINELONEN #4' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#ED1C24'>
                                <br />
                                <img src={pokeballCup} className="Trophy6" alt="no-trophy-yes-cup" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                            {(season.finalPlace === 'EI SIJOITUSTA' ||season.finalPlace === 'HYLÄTTY') && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#353535'>
                                <br />
                                <img src={pokeballNoCup} className="Trophy6" alt="no-trophy-no-cup" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                            {season.finalPlace === 'TURNAUS KESKEN' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                                <br />
                                {season.finalPlace}
                            </font>}
                        </center>}
                        {currentDate.getFullYear() > season.year && <center>
                            {season.finalPlace === 'LIIGAMESTARI' && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#DCAB01'>
                                <br />
                                <img src={trophyTampereGold} className="Trophy1" alt="trophy-gold" />
                                <br />
                                Lopullinen sijoitus: VOITTAJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #1' || season.finalPlace === 'ELIITTINELONEN #1') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#A1A1A1'>
                                <br />
                                <img src={trophyTampereSilver} className="Trophy1" alt="trophy-silver" />
                                <br />
                                Lopullinen sijoitus: 2. SIJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #2' || season.finalPlace === 'ELIITTINELONEN #2') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#8D573A'>
                                <br />
                                <img src={trophyTampereBronze} className="Trophy1" alt="trophy-bronze" />
                                <br />
                                Lopullinen sijoitus: 3. SIJA ({season.finalPlace})
                            </font>}
                            {(season.finalPlace === 'ELIITTIKOLMONEN #3' || season.finalPlace === 'ELIITTINELONEN #3') && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#00AEF9'>
                                <br />
                                <img src={trophyFourth} className="Trophy4" alt="trophy-forth" />
                                <br />
                                Lopullinen sijoitus: 4. SIJA ({season.finalPlace})
                            </font>}
                            {season.finalPlace === 'ELIITTINELONEN #4' && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#00AEF9'>
                                <br />
                                <img src={trophyFifth} className="Trophy4" alt="trophy-fifth" />
                                <br />
                                Lopullinen sijoitus: 5. SIJA ({season.finalPlace})
                            </font>}
                            {season.finalPlace === 'EI SIJOITUSTA' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#353535'>
                                <br />
                                <img src={pokeballNoCup} className="Trophy6" alt="no-trophy-no-cup" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                            {season.finalPlace !== 'LIIGAMESTARI' && season.finalPlace !== 'ELIITTIKOLMONEN #1' && season.finalPlace !== 'ELIITTINELONEN #1' && season.finalPlace !== 'ELIITTIKOLMONEN #2' && season.finalPlace !== 'ELIITTINELONEN #2' && season.finalPlace !== 'ELIITTIKOLMONEN #3' && season.finalPlace !== 'ELIITTINELONEN #3' && season.finalPlace !== 'ELIITTINELONEN #4' && season.finalPlace !== 'EI SIJOITUSTA' && season.city === 'Tampere' && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color='#ED1C24'>
                                <br />
                                <img src={pokeballCup} className="Trophy6" alt="trophy-none" />
                                <br />
                                Lopullinen sijoitus: {season.finalPlace}
                            </font>}
                        </center>}
                        {season.cupStarted === true && season.cupTeam.length > 0 && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            <center>
                                <br />
                                The Champion Cup -tiimi:
                                <br />
                                {season.cupTeam.map((pokemon) => renderPokemon(pokemon))}
                            </center>
                        </font>
                        }
                        {season.badges.length > 0 && <font className="SeasonDescriptionText" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                            <center>
                                <br />
                                Kauden merkit:
                                <br />
                                {season.badges.map((badge) => renderBadge(badge))}
                            </center>
                        </font>
                        }
                    </Box>
                </Box>
                <br className="SeasonStatBr" />
            </div>
        );
    }

    const printBP = (points) => {
        let thousands = 'null';
        let hundreds = 'null';
        let dozens = 'null';
        let units = 'null';

        let pointString = points.toString();
        let pointChars = pointString.split("");

        if (points >= 1000) {
            thousands = pointChars[0];
            hundreds = pointChars[1];
            dozens = pointChars[2];
            units = pointChars[3];
        }
        else if (points >= 100) {
            hundreds = pointChars[0];
            dozens = pointChars[1];
            units = pointChars[2];
        }
        else if (points >= 10) {
            dozens = pointChars[0];
            units = pointChars[1];
        }
        else {
            units = pointChars[0];
        }

        return(
            <font className="PlayerProfileText" face="Segoe UI Semibold" color='#B5E61D'>
                <br />
                <img src={require('./resources/digits/' + thousands + '.png')} alt="1000-digit"/>
                <img src={require('./resources/digits/' + hundreds + '.png')} alt="100-digit"/>
                <img src={require('./resources/digits/' + dozens + '.png')} alt="10-digit"/>
                <img src={require('./resources/digits/' + units + '.png')} alt="1-digit"/>
                <img src={require('./resources/digits/BP.png')} alt="battle-points"/>
                <br />
                {props.tower.current.wins} / {props.tower.current.battles} voittoa
            </font>
        );
    }

    if (props.city === 'tower') {
        return(
            <Box borderRadius={7} p={0.5} style={{ backgroundColor: bgColor, color: props.theme.palette.primary.contrastText }}>
                <Box borderRadius={6} p={1} style={{ backgroundColor: screenColor, color: textColor }}>
                    <font className="PlayerProfileTitle" face="Segoe UI Semibold" color={textColor}>
                        <center>
                            {props.player}
                        </center>
                    </font>
                    <center>
                        <img src={emblem} className="ProfileTeamEmblem" alt="profile-team-emblem"/>
                    </center>
                    <font className="PlayerProfileDescription" face="Segoe UI Semibold" color={textColor}>
                        <center>
                            LVL: {props.lvl}, Tiimi: {props.team}
                            <br />
                            {props.description}
                        </center>
                    </font>
                    <br />
                    <center>
                    <table align="center">
                        <tbody>
                            <tr height="130px">
                                <td align="center" width="20%" min-width="20%" max-width="20%">
                                    {props.tower.current.rank === 0 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={commonerTheme.palette.primary.main}>
                                        <center>
                                            <img src={commonerUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Torniaateliton
                                        </center>
                                    </font>}
                                    {props.tower.current.rank === 1 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={baronTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={baronUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Torniparoni
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={baronUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Torniparonitar
                                        </center>}
                                    </font>}
                                    {props.tower.current.rank === 2 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={viscountTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={viscountUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornivarakreivi
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={viscountUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornivarakreivitär
                                        </center>}
                                    </font>}
                                    {props.tower.current.rank === 3 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={earlTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={earlUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornijaarli
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={earlUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornikreivitär
                                        </center>}
                                    </font>}
                                    {props.tower.current.rank === 4 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={marquisTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={marquisUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornimarkiisi
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={marquisUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornimarkiisitar
                                        </center>}
                                    </font>}
                                    {props.tower.current.rank === 5 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={dukeTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={dukeUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Torniherttua
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={dukeUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Torniherttuatar
                                        </center>}
                                    </font>}
                                    {props.tower.current.rank === 6 && <font className="PlayerProfileText" face="Segoe UI Semibold" color={grandDukeTheme.palette.primary.main}>
                                        {props.gender === 'Mies' && <center>
                                            <img src={grandDukeUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornisuurherttua
                                        </center>}
                                        {props.gender === 'Nainen' && <center>
                                            <img src={grandDukeUniform} className="TowerRankProfile" alt="tower-rank" />
                                            <br />
                                            Tornisuurherttuatar
                                        </center>}
                                    </font>}
                                    </td>
                                    <td align="center" width="15%" min-width="15%" max-width="15%" />
                                    <td align="center" width="30%" min-width="30%" max-width="30%">
                                        <center>
                                            {printBP(props.tower.current.points)}
                                        </center>
                                    </td>
                                    <td align="center" width="15%" min-width="15%" max-width="15%" />
                                    <td align="center" width="20%" min-width="20%" max-width="20%">
                                        <font className="PlayerProfileText" face="Segoe UI Semibold" color={'#FFC90E'}>
                                            <center>
                                                <img src={battleKey} className="KeyLargerIcon" alt="battle-key" />
                                                <br />
                                                {props.tower.current.battleKeys} otteluavainta
                                            </center>
                                        </font>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </Box>
            </Box>
        );
    }

    return(
        <Box borderRadius={7} p={0.5} style={{ backgroundColor: bgColor, color: props.theme.palette.primary.contrastText }}>
            <Box borderRadius={6} p={1} style={{ backgroundColor: screenColor, color: textColor }}>
                <font className="PlayerProfileTitle" face="Segoe UI Semibold" color={textColor}>
                    <center>
                        {props.player}
                    </center>
                </font>
                <center>
                    <img src={emblem} className="ProfileTeamEmblem" alt="profile-team-emblem"/>
                </center>
                <font className="PlayerProfileDescription" face="Segoe UI Semibold" color={textColor}>
                    <center>
                        LVL: {props.lvl}, Tiimi: {props.team}
                        <br />
                        {props.description}
                    </center>
                </font>
                <br />
                <font className="PlayerProfileDescription" face="Segoe UI Semibold" color={textColor}>
                    <center>
                        TURNAUSHISTORIA:
                    </center>
                </font>
                {props.seasons.map((season) => renderSeason(season))}
            </Box>
        </Box>
    );
}
export default PlayerProfile;
import { createTheme } from '@mui/material/styles';

export const grandDukeTheme = createTheme({
  palette: {
    primary: {
      light: '#D566FF',
      main: '#CB40FF',
      dark: '#8E2CB2',
      contrastText: '#000',
    },
    secondary: {
      light: '#DD84FF',
      main: '#D566FF',
      dark: '#9547B2',
      contrastText: '#000',
    },
  },
});

export const dukeTheme = createTheme({
  palette: {
    primary: {
      light: '#ED5252',
      main: '#E92727',
      dark: '#A31B1B',
      contrastText: '#000',
    },
    secondary: {
      light: '#F07474',
      main: '#989898',
      dark: '#A53939',
      contrastText: '#000',
    },
  },
});

export const marquisTheme = createTheme({
  palette: {
    primary: {
      light: '#FFD33E',
      main: '#FFC90E',
      dark: '#B28C09',
      contrastText: '#000',
    },
    secondary: {
      light: '#FFDB64',
      main: '#FFD33E',
      dark: '#B2932B',
      contrastText: '#000',
    },
  },
});

export const earlTheme = createTheme({
  palette: {
    primary: {
      light: '#33BB33',
      main: '#00AA00',
      dark: '#007600',
      contrastText: '#000',
    },
    secondary: {
      light: '#5BC85B',
      main: '#33BB33',
      dark: '#238223',
      contrastText: '#000',
    },
  },
});

export const viscountTheme = createTheme({
  palette: {
    primary: {
      light: '#6666FF',
      main: '#4040FF',
      dark: '#2C2CB2',
      contrastText: '#000',
    },
    secondary: {
      light: '#8484FF',
      main: '#6666FF',
      dark: '#4747B2',
      contrastText: '#000',
    },
  },
});

export const baronTheme = createTheme({
  palette: {
    primary: {
      light: '#FFFFFF',
      main: '#FFFFFF',
      dark: '#B2B2B2',
      contrastText: '#000',
    },
    secondary: {
      light: '#111111',
      main: '#000000',
      dark: '#000000',
      contrastText: '#FFF',
    },
  },
});

export const commonerTheme = createTheme({
  palette: {
    primary: {
      light: '#989898',
      main: '#7F7F7F',
      dark: '#585858',
      contrastText: '#000',
    },
    secondary: {
      light: '#ACACAC',
      main: '#989898',
      dark: '#6A6A6A',
      contrastText: '#000',
    },
  },
});
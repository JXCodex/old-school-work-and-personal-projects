import './Main.css';

import { Button, Typography } from '@mui/material';
import React, { useState } from 'react';
import { doubleStarTheme, hesaTheme, joensuuTheme, jyvasTheme, kingdomTheme, kuopioTowerTheme, lahtiTheme, mainTheme, poriTheme, treTheme, turkuTheme } from './CityThemes';

import { CityPage } from './CityPage';
import { NoExistPage } from './NoExistPage';
import { TowerPage } from './TowerPage';
import battleChateauTowerString from './resources/strings/tower.png';
import doubleStarString from './resources/strings/doublestar.png';
import helsinkiString from './resources/strings/helsinki.png';
import joensuuString from './resources/strings/joensuu.png';
import jyvaskylaString from './resources/strings/jyvaskyla.png';
import kuopioString from './resources/strings/kuopio.png';
import lahtiString from './resources/strings/lahti.png';
import leagueKingdomString from './resources/strings/kingdom.png';
import logo from './resources/logos/main.png';
import poriString from './resources/strings/pori.png';
import tampereString from './resources/strings/tampere.png';
import turkuString from './resources/strings/turku.png';

export const Main = () => {
    let currentDate = new Date();
    window.scrollTo(0, 0);
    
    const [login, setLogin] = useState(false); // Jos et ole kirjautunut sisään.
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [gender, setGender] = useState('');
    const [playerTeam, setPlayerTeam] = useState('');
    const [playerLVL, setPlayerLVL] = useState('');
    const [playerDescription, setPlayerDescription] = useState('');
    const [playerSeasons, setPlayerSeasons] = useState('');
    const [playerTower, setPlayerTower] = useState('');

    const [city, setCity] = useState('main'); // Jos tila on 'main', kaupunkiseutuvalikko näkyy, muussa tapauksessa joko kaupunkiseudun oman League Cityn sivu tai koko Suomen League Kingdomin sivu.
    const [fixedCity, setFixedCity] = useState('Main'); // Tämä sisältää ison alkukirjaimen version sekä ä- ja ö-kirjaimilla varustetun version.
    const [prevCity, setPrevCity] = useState('main'); // Avuksi kirjautumisvalikkoon.
    const [theme, setTheme] = useState(mainTheme); // Tämän tila määrittää, että mitkä värit ja teema välittyvät tekstiin kun siirrytään jonkin kaupunkiseudun oman League Cityn sivuille.

    return (
        <div>
            {city === 'main' &&
                <div>
                    <header className="Main-header">
                        <img src={logo} className="Main-logo" alt="logo" />
                    </header>
                    <br className="MainMenuBr" />
                    <div className="Main-body-header">
                        {login === false && <font color={theme.palette.primary.main} className="Intro-caption">Tervetuloa tutustumaan League Cityyn!</font>}
                        {login === true && <font color={theme.palette.primary.main} className="City-caption">Tervetuloa takaisin, {username}!</font>}
                        <br className="MainMenuBr" />
                        <font color={theme.palette.primary.main} className="City-caption">Seutukunnat:</font>
                        <br />
                        <table align="center">{/* Tämä taulukko määrittelee oikein jokaisen seutukuntapainikkeen asetelman. */}
                            <tbody>
                                <tr height="30px">
                                    <td width="45%" min-width="45%" max-width="45%">
                                        <Button>
                                            <img src={helsinkiString} onClick={() => {window.scrollTo(0, 0); setCity('helsinki'); setFixedCity('Helsinki'); setPrevCity('helsinki'); setTheme(hesaTheme)}} className="Helsinki" alt="city-string-helsinki" />
                                        </Button>
                                    </td>
                                    <td width="10%" min-width="10%" max-width="10%" />
                                    <td width="45%" min-width="45%" max-width="45%">
                                        <Button>
                                            <img src={joensuuString} onClick={() => {window.scrollTo(0, 0); setCity('joensuu'); setFixedCity('Joensuu'); setPrevCity('joensuu'); setTheme(joensuuTheme)}} className="Joensuu" alt="city-string-joensuu" />
                                        </Button>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td>
                                        <Button>
                                            <img src={jyvaskylaString} onClick={() => {window.scrollTo(0, 0); setCity('jyvaskyla'); setFixedCity('Jyväskylä'); setPrevCity('jyvaskyla'); setTheme(jyvasTheme)}} className="Jyvaskyla" alt="city-string-jyvaskyla" />
                                        </Button>
                                    </td>
                                    <td />
                                    <td>
                                        <Button>
                                            <img src={kuopioString} onClick={() => {window.scrollTo(0, 0); setCity('kuopio'); setFixedCity('Kuopio'); setPrevCity('kuopio'); setTheme(kuopioTowerTheme)}} className="Kuopio" alt="city-string-kuopio" />
                                        </Button>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td>
                                        <Button>
                                            <img src={lahtiString} onClick={() => {window.scrollTo(0, 0); setCity('lahti'); setFixedCity('Lahti'); setPrevCity('lahti'); setTheme(lahtiTheme)}} className="Lahti" alt="city-string-lahti" />
                                        </Button>
                                    </td>
                                    <td />
                                    <td>
                                        <Button>
                                            <img src={poriString} onClick={() => {window.scrollTo(0, 0); setCity('pori'); setFixedCity('Pori'); setPrevCity('pori'); setTheme(poriTheme)}} className="Pori" alt="city-string-pori" />
                                        </Button>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td>
                                        <Button>
                                            <img src={tampereString} onClick={() => {window.scrollTo(0, 0); setCity('tampere'); setFixedCity('Tampere'); setPrevCity('tampere'); setTheme(treTheme)}} className="Tampere" alt="city-string-tampere" /> 
                                        </Button>
                                    </td>
                                    <td />
                                    <td>
                                        <Button>
                                            <img src={turkuString} onClick={() => {window.scrollTo(0, 0); setCity('turku'); setFixedCity('Turku'); setPrevCity('turku'); setTheme(turkuTheme)}} className="Turku" alt="city-string-turku" />
                                        </Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <font face="Segoe UI Semibold" color={theme.palette.primary.main} className="Kingdom-caption">Suomen mestaruusottelu:</font>
                        <br />
                        <Button>
                            <img src={leagueKingdomString} onClick={() => {window.scrollTo(0, 0); setCity('kingdom'); setFixedCity('Kingdom'); setPrevCity('kingdom'); setTheme(kingdomTheme)}} className="Kingdom-caption" alt="city-string-league-kingdom" />
                        </Button>
                        <br />
                        <font face="Segoe UI Semibold" color={theme.palette.primary.main} className="Tower-caption">Toisenlaisia haasteita etsiville:</font>
                        <br />
                        <Button>
                            <img src={battleChateauTowerString} onClick={() => {window.scrollTo(0, 0); setCity('tower'); setFixedCity('Tower'); setPrevCity('tower'); setTheme(kuopioTowerTheme)}} className="Tower-caption" alt="city-string-battle-tower" />
                        </Button>
                        <br />
                        <font face="Segoe UI Semibold" color={theme.palette.primary.main} className="DoubleStar-caption">Huvin vuoksi tai hyväntekeväisyyttä varten:</font>
                        <br />
                        <Button>
                            <img src={doubleStarString} onClick={() => {window.scrollTo(0, 0); setCity('doublestar'); setFixedCity('DoubleStar'); setPrevCity('doublestar'); setTheme(doubleStarTheme)}} className="DoubleStar-caption" alt="city-string-double-star" />
                        </Button>
                        <br className="MainMenuBr" />
                    </div>
                </div>
            }
            {(city === 'helsinki' || city === 'jyvaskyla' || city === 'kuopio' || city === 'tampere' || city === 'turku') && <CityPage login={login} setLogin={setLogin} username={username} setUsername={setUsername} password={password} setPassword={setPassword} theme={theme} setTheme={setTheme} city={city} setCity={setCity} fixedCity={fixedCity} setFixedCity={setFixedCity} prevCity={prevCity} setPrevCity={setPrevCity} playerSeasons={playerSeasons} setPlayerSeasons={setPlayerSeasons} gender={gender} setGender={setGender} playerTeam={playerTeam} setPlayerTeam={setPlayerTeam} playerLVL={playerLVL} setPlayerLVL={setPlayerLVL} playerDescription={playerDescription} setPlayerDescription={setPlayerDescription} />}
            {city === 'tower' && <TowerPage login={login} setLogin={setLogin} username={username} setUsername={setUsername} password={password} setPassword={setPassword} theme={theme} setTheme={setTheme} city={city} setCity={setCity} fixedCity={fixedCity} setFixedCity={setFixedCity} prevCity={prevCity} setPrevCity={setPrevCity} playerTower={playerTower} setPlayerTower={setPlayerTower} gender={gender} setGender={setGender} playerTeam={playerTeam} setPlayerTeam={setPlayerTeam} playerLVL={playerLVL} setPlayerLVL={setPlayerLVL} playerDescription={playerDescription} setPlayerDescription={setPlayerDescription} />}
            {(city === 'joensuu' || city === 'lahti' || city === 'pori' || city === 'kingdom' || city === 'doublestar') && <NoExistPage city={city} setCity={setCity} fixedCity={fixedCity} setFixedCity={setFixedCity} prevCity={prevCity} setPrevCity={setPrevCity} theme={theme} setTheme={setTheme} />}
            <Typography style={{ backgroundColor: theme.palette.primary.main, color: theme.palette.primary.contrastText }}>
                <center className="Footer-text">
                        <br />
                        ©{currentDate.getFullYear()} Juuso Hauvala | Kaikki oikeudet pidätetään
                        <br />
                        Pokémon ja kaikki vastaavat nimet ovat Nintendon tavaramerkkejä © 1996-{currentDate.getFullYear()}
                        <br />
                        Pokémon GO on Niantic Inc.:n tavaramerkki ©
                        <br />
                        PokéGOES League City, Greater Helsinki PokéGOES League Metropolis, Finland PokéGOES League Kingdom,
                        <br />
                        Battle Chateau Tower sponsored by League City ja Double Star Tournament sponsored by League City
                        <br />eivät ole sidoksissa Niantic Inc.:iin, The Pokemon Companyyn tai Nintendoon.
                        <br /><br />
                </center>
            </Typography>
        </div>
    );
}
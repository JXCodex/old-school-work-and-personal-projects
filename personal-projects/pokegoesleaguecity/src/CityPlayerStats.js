import './CityAndTower.css';

import { Box, Container, Link, Typography } from '@mui/material';
import React, { useState } from 'react';

import PlayerProfile from './PlayerProfile';
import PlayersData from './data/PlayersData.json';
import instinctEmblem from './resources/teams/instinct-black.png';
import mysticEmblem from './resources/teams/mystic-white.png';
import valorEmblem from './resources/teams/valor-white.png';

export const CityPlayerStats = (props) => {
    // CityPlayerStats sisältää koodin, joka määrittää pelaajaluettelon näkymän.
    const [subtab, setSubtab] = useState('mainSubtab');
    const [player, setPlayer] = useState(null);
    const [playerTeam, setPlayerTeam] = useState(null);
    const [playerLVL, setPlayerLVL] = useState(null);
    const [playerDescription, setPlayerDescription] = useState(null);
    const [seasons, setSeasons] = useState(null);

    const renderPlayer = (player) => {

        let emblem;
        let textColor;
        let bgColor;
        let participated = false;

        // Tämä varmistaa, että pelaajan tiedot ovat katsottavissa vain, jos hän on kilpaillut kyseisen kaupungin League Cityssä.
        for (var i = 0; i < player.seasons.length; i++) {
            if (player.seasons[i].city === props.fixedCity) {
                participated = true;
            }
        }

        if (player.team === 'Instinct') {
            emblem = instinctEmblem;
            textColor = '#000000';
            bgColor = '#FFFF00';
        }
        else if (player.team === 'Mystic') {
                emblem = mysticEmblem;
                textColor = '#FFFFFF';
                bgColor = '#4040FF';
        }
        else if (player.team === 'Valor') {
                emblem = valorEmblem;
                textColor = '#FFFFFF';
                bgColor = '#FF0000';
        }
        else {
            textColor = '#A9A9A9';
            emblem = null;
        }

        if (player.seasons.length > 0 && participated === true) {
            // Pelaajan tiedot tulostetaan vain, jos hän on osallistunut yhdellekään kaudelle.
            return(
                <div>
                    <Box borderRadius={4} p={0.5} style={{ backgroundColor: props.theme.palette.primary.dark, color: props.theme.palette.primary.contrastText }}>
                        <Box borderRadius={3.25} p={1} style={{ backgroundColor: bgColor, color: textColor }}>
                            <font className="PlayerInfo" face="Segoe UI Semibold">
                                {player.username}: <img src={emblem} className="TeamEmblem" alt="team-emblem"/> ({player.team}), LVL {player.lvl}, {player.description}
                            </font>
                            <br />
                            <Link component="button" onClick={() => {window.scrollTo(0, 0); setPlayer(player.username); setPlayerTeam(player.team); setPlayerLVL(player.lvl); setPlayerDescription(player.description); setSeasons(player.seasons); setSubtab('player')}}>
                                <font color={textColor} className="PlayerProfileLink" face="Segoe UI Semibold">{'≫'} näytä profiili {'≫'}</font>
                            </Link>
                        </Box>
                    </Box>
                    <br className="PlayerStatBr" />
                </div>
            );
        }

        return(<font />);
    }

    return(
        <Container>
            {subtab === 'mainSubtab' && props.city === 'tampere' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <font className="Site-body-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        KILPAILIJAT
                    </font>
                    <br /><br />
                    {PlayersData.users.map((player) => renderPlayer(player))}
                </Typography>
            }
            {subtab === 'mainSubtab' && props.city !== 'tampere' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF', height: '650px' }}>
                    <font className="Site-body-header" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>
                        KILPAILIJAT
                    </font>
                    <br /><br />
                    <font className="Site-body-mid-text" face="Segoe UI Semibold" color={props.theme.palette.primary.main}>Tämän seutukunnan turnauksella ei ole vielä kilpailijoita.</font>
                </Typography>
            }
            {subtab !== 'mainSubtab' && 
                <Typography className="whiteContainer" style={{ backgroundColor: '#FFFFFF' }}>
                    <Link className="LinkLeft" component="button" onClick={() => {window.scrollTo(0, 0); setPlayer(null); setPlayerTeam(null); setPlayerLVL(null); setPlayerDescription(null); setSubtab('mainSubtab')}}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                                {'≪'} Palaa pelaajaluetteloon
                            </center>
                        </font>
                    </Link>
                    {props.yourUsername === player && <Link className="LinkRight" component="button" onClick={() => {window.scrollTo(0, 0)}} style={{ textAlign: 'right' }}>
                        <font className="Site-body-link" face="Segoe UI Semibold" color={props.theme.palette.primary.dark}>
                            <center>
                            Muokkaa profiilia {'≫'}
                            </center>
                        </font>
                    </Link>}
                    <br /><br />
                    <PlayerProfile player={player} yourUsername={props.username} seasons={seasons} city={props.city} team={playerTeam} theme={props.theme} lvl={playerLVL} description={playerDescription} />
                </Typography>
            }
        </Container>
    );
}
export default CityPlayerStats;